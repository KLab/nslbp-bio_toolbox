% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : June 2020
% -------------------------------------------------------------------------
% Description  : Find correlation between biomarkers and PROMs
% -------------------------------------------------------------------------
% Dependencies : None
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

clearvars;
close all;
warning ('off','all');
clc;

cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\data\');
load('Biomarker_patient.mat');
load('PROMs.mat');

BMo5  = [];
BMo9  = [];
BMo12 = [];
BMo15 = [];
BMo18 = [];
BMo49 = [];
BMo57 = [];
BMo59 = [];
for isubject = 1:30
    BMo5  = [BMo5; mean(Biomarker_patient.BMo5(isubject,1).value)];
    BMo9  = [BMo9; mean(Biomarker_patient.BMo9(isubject,1).value)];
    BMo12 = [BMo12; mean(Biomarker_patient.BMo12(isubject,1).value)];
    BMo15 = [BMo15; mean(Biomarker_patient.BMo15(isubject,1).value)];
    BMo18 = [BMo18; mean(Biomarker_patient.BMo18(isubject,1).value)];
    BMo49 = [BMo49; mean(Biomarker_patient.BMo49(isubject,1).value)];
    BMo57 = [BMo57; mean(Biomarker_patient.BMo57(isubject,1).value)];
    BMo59 = [BMo59; mean(Biomarker_patient.BMo59(isubject,1).value)];
end

corrcoef([BMo5 BMo9 BMo12 BMo15 BMo18 BMo49 BMo57 BMo59 PROMs])