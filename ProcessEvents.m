% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : June 2020
% -------------------------------------------------------------------------
% Description  : Autodetect events related to each motor task
% Inputs       : Trial (structure)
% Outputs      : Trial (structure)
% -------------------------------------------------------------------------
% Dependencies : - f_zeni_event from Visscher et al.: https://doi.org/10.1016/j.gaitpost.2021.02.031
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function Trial = ProcessEvents(Participant,Session,Trial)

% -------------------------------------------------------------------------
% GAIT TRIALS
% -------------------------------------------------------------------------
if contains(Trial.type,'Gait')
    % Compute gait events using the Zeni et al. 2008 algorithm
    [RTO,RHS] = f_zeni_event(Trial.fmarker,...
                             permute(Trial.Marker(17).Trajectory.smooth(1,:,:),[3,1,2]),... % HEE marker
                             permute(Trial.Marker(20).Trajectory.smooth(1,:,:),[3,1,2]),... % SMH marker 
                             (permute(Trial.Marker(3).Trajectory.smooth(1,:,:),[3,1,2])+permute(Trial.Marker(4).Trajectory.smooth(1,:,:),[3,1,2]))/2,... % Sacrum marker
                             [],[]);
    [LTO,LHS] = f_zeni_event(Trial.fmarker,...
                             permute(Trial.Marker(32).Trajectory.smooth(1,:,:),[3,1,2]),... % HEE marker
                             permute(Trial.Marker(35).Trajectory.smooth(1,:,:),[3,1,2]),... % SMH marker 
                             (permute(Trial.Marker(3).Trajectory.smooth(1,:,:),[3,1,2])+permute(Trial.Marker(4).Trajectory.smooth(1,:,:),[3,1,2]))/2,... % Sacrum marker
                             [],[]);

    % Clean false events (if two consecutive events not spaced more than a temporal threshold)                      
    threshold = 20; % frames
    if isempty(Trial.Event(1).value) % Avoid to delete manually defined events
        Trial.Event(1).value = [];
        for ievent = 1:length(RHS)-1
            if ievent == length(RHS)-1
                if abs(RHS(ievent)-RHS(ievent+1)) > threshold
                    Trial.Event(1).value = [Trial.Event(1).value RHS(ievent) RHS(ievent+1)];
                end

            else
                if abs(RHS(ievent)-RHS(ievent+1)) > threshold
                    Trial.Event(1).value = [Trial.Event(1).value RHS(ievent)];
                end
            end
        end
        Trial.Event(1).Processing.detection = 'Zeni';
    else
        Trial.Event(1).Processing.detection = 'Manual';
    end
    if isempty(Trial.Event(2).value) % Avoid to delete manually defined events
        Trial.Event(2).value = [];
        for ievent = 1:length(RTO)-1
            if ievent == length(RTO)-1
                if abs(RTO(ievent)-RTO(ievent+1)) > threshold
                    Trial.Event(2).value = [Trial.Event(2).value RTO(ievent) RTO(ievent+1)];
                end

            else
                if abs(RTO(ievent)-RTO(ievent+1)) > threshold
                    Trial.Event(2).value = [Trial.Event(2).value RTO(ievent)];
                end
            end
        end
        Trial.Event(2).Processing.detection = 'Zeni';
    else
        Trial.Event(2).Processing.detection = 'Manual';
    end
    if isempty(Trial.Event(3).value) % Avoid to delete manually defined events
        Trial.Event(3).value = [];
        for ievent = 1:length(LHS)-1
            if ievent == length(LHS)-1
                if abs(LHS(ievent)-LHS(ievent+1)) > threshold
                    Trial.Event(3).value = [Trial.Event(3).value LHS(ievent) LHS(ievent+1)];
                end

            else
                if abs(LHS(ievent)-LHS(ievent+1)) > threshold
                    Trial.Event(3).value = [Trial.Event(3).value LHS(ievent)];
                end
            end
        end
        Trial.Event(3).Processing.detection = 'Zeni';
    else
        Trial.Event(3).Processing.detection = 'Manual';
    end
    if isempty(Trial.Event(4).value) % Avoid to delete manually defined events
        Trial.Event(4).value = [];
        for ievent = 1:length(LTO)-1
            if ievent == length(LTO)-1
                if abs(LTO(ievent)-LTO(ievent+1)) > threshold
                    Trial.Event(4).value = [Trial.Event(4).value LTO(ievent) LTO(ievent+1)];
                end

            else
                if abs(LTO(ievent)-LTO(ievent+1)) > threshold
                    Trial.Event(4).value = [Trial.Event(4).value LTO(ievent)];
                end
            end
        end
        Trial.Event(4).Processing.detection = 'Zeni';
    else
        Trial.Event(4).Processing.detection = 'Manual';
    end
end

% -------------------------------------------------------------------------
% OTHER TRIALS
% -------------------------------------------------------------------------
% Crop raw files if needed to keep only wanted cycles
if contains(Trial.type,'Perturbation_R_Shoulder')
    type      = 1;
    threshold = 125; % deg
    vec1      = permute(Trial.Marker(56).Trajectory.smooth,[3,1,2])-permute(Trial.Marker(54).Trajectory.smooth,[3,1,2]); % Vector RWRA-RSHO
    vec2      = permute(Trial.Marker(8).Trajectory.smooth,[3,1,2])-permute(Trial.Marker(54).Trajectory.smooth,[3,1,2]); % Vector RGTR-RSHO
    Trial     = DetectEvents(Trial,vec1,vec2,type,threshold);
    clear type threshold vec1 vec2;

elseif contains(Trial.type,'Perturbation_L_Shoulder')   
    type      = 1;
    threshold = 125; % deg
    vec1      = permute(Trial.Marker(61).Trajectory.smooth,[3,1,2])-permute(Trial.Marker(59).Trajectory.smooth,[3,1,2]); % Vector LWRA-lSHO
    vec2      = permute(Trial.Marker(23).Trajectory.smooth,[3,1,2])-permute(Trial.Marker(59).Trajectory.smooth,[3,1,2]); % Vector LGTR-LSHO
    Trial     = DetectEvents(Trial,vec1,vec2,type,threshold);
    clear type threshold vec1 vec2;

elseif contains(Trial.type,'Trunk_Forward') || contains(Trial.type,'XDMNN') % Kevin FRP data
    type      = 2;
    threshold = 45; % deg
    vec1      = permute(Trial.Marker(41).Trajectory.smooth,[3,1,2])-...
                (permute(Trial.Marker(3).Trajectory.smooth,[3,1,2])+permute(Trial.Marker(4).Trajectory.smooth,[3,1,2]))/2; % Vector C7-mean(RPSI,LPSI)
    vec2      = repmat([0 1 0],[size(vec1,1),1]); % Vector ICS_Y
    Trial     = DetectEvents(Trial,vec1,vec2,type,threshold);
    clear type threshold vec1 vec2;

elseif contains(Trial.type,'Trunk_Lateral')
    type      = 2;
    threshold = 20; % deg
    vec1      = permute(Trial.Marker(41).Trajectory.smooth,[3,1,2])-...
                (permute(Trial.Marker(3).Trajectory.smooth,[3,1,2])+permute(Trial.Marker(4).Trajectory.smooth,[3,1,2]))/2; % Vector C7-mean(RPSI,LPSI)
    vec2      = repmat([0 1 0],[size(vec1,1),1]); % Vector ICS_Y
    Trial     = DetectEvents(Trial,vec1,vec2,type,threshold);
    clear type threshold vec1 vec2;

elseif contains(Trial.type,'Trunk_Rotation')
    type      = 2;
    threshold = 22.5; % deg
    vec1      = permute(Trial.Marker(54).Trajectory.smooth,[3,1,2])-permute(Trial.Marker(59).Trajectory.smooth,[3,1,2]); % Vector RSHO-lSHO
    vec2      = permute(Trial.Marker(15).Trajectory.smooth,[3,1,2])-permute(Trial.Marker(30).Trajectory.smooth,[3,1,2]); % Vector RANK-LANK
    Trial     = DetectEvents(Trial,vec1,vec2,type,threshold);
    clear type threshold vec1 vec2;

elseif contains(Trial.type,'Weight_Constrained')
    type      = 2;
    threshold = 40; % deg
    vec1      = permute(Trial.Marker(8).Trajectory.smooth,[3,1,2])-permute(Trial.Marker(10).Trajectory.smooth,[3,1,2]); % Vector RGTR-RKNE
    vec2      = permute(Trial.Marker(8).Trajectory.smooth,[3,1,2])-permute(Trial.Marker(15).Trajectory.smooth,[3,1,2]); % Vector RANK-RKNE
    Trial     = DetectEvents(Trial,vec1,vec2,type,threshold);
    clear type threshold vec1 vec2;

elseif contains(Trial.type,'Weight_Unconstrained')
    type      = 2;
    threshold = 40; % deg
    vec1      = permute(Trial.Marker(41).Trajectory.smooth,[3,1,2])-...
                (permute(Trial.Marker(3).Trajectory.smooth,[3,1,2])+permute(Trial.Marker(4).Trajectory.smooth,[3,1,2]))/2; % Vector C7-mean(RPSI,LPSI)
    vec2      = repmat([0 1 0],[size(vec1,1),1]); % Vector ICS_Y
    Trial     = DetectEvents(Trial,vec1,vec2,type,threshold);
    % Event misdetection manual correction NSLBP-BIO-039 | REL session
    if strcmp(Participant.id,'NSLBP-BIO-039') && strcmp(Session.type,'REL') 
        Trial.Event(9).value = [Trial.Event(9).value(1:end-1) 2100 2727 Trial.Event(9).value(end)];
        Trial.Event(10).value = [Trial.Event(10).value 2451 2948];
    end
    % Event misdetection manual correction NSLBP-BIO-049 | REL session
    if strcmp(Participant.id,'NSLBP-BIO-049') && strcmp(Session.type,'REL') 
        Trial.Event(9).value = [Trial.Event(9).value(1:end-1) 583 1105 1741 2128 2570 Trial.Event(9).value(end)];
        Trial.Event(10).value = [Trial.Event(10).value 896 1424 1934 2404 2878];
    end
    % Event misdetection manual correction NSLBP-BIO-039 | INI session
    if strcmp(Participant.id,'NSLBP-BIO-039') && strcmp(Session.type,'INI') 
        Trial.Event(9).value = [Trial.Event(9).value(1) 758 1275 1775 2278 2763 Trial.Event(9).value(end)];
        Trial.Event(10).value = [433 944 1458 1957 2457 3010];
    end
    % Event misdetection manual correction NSLBP-BIO-060 | REL session
    if strcmp(Participant.id,'NSLBP-BIO-060') && strcmp(Session.type,'REL') 
        Trial.Event(9).value = [Trial.Event(9).value(1) 690 1237 1801 2328 2729 Trial.Event(9).value(end)];
        Trial.Event(10).value = [413 924 1469 2028 2523 3044];
    end
    % Event misdetection manual correction NSLBP-BIO-036 | REL session
    if strcmp(Participant.id,'NSLBP-BIO-036') && strcmp(Session.type,'REL') 
        Trial.Event(9).value = [Trial.Event(9).value(1) 933 1601 2236 2870 3550 Trial.Event(9).value(end)];
        Trial.Event(10).value = [491 1245 1894 2553 3194 3883];
    end
    % Event misdetection manual correction NSLBP-BIO-063 | REL session
    if strcmp(Participant.id,'NSLBP-BIO-063') && strcmp(Session.type,'REL') 
        Trial.Event(9).value = [Trial.Event(9).value(1) 569 1219 1781 2393 2951 Trial.Event(9).value(end)];
        Trial.Event(10).value = [277 831 1457 2104 2622 3255];
    end
    clear type threshold vec1 vec2;
elseif contains(Trial.type,'S2S')
    type      = 3;
    threshold = 25; % deg
    vec1      = permute(Trial.Marker(41).Trajectory.smooth,[3,1,2])-...
                (permute(Trial.Marker(3).Trajectory.smooth,[3,1,2])+permute(Trial.Marker(4).Trajectory.smooth,[3,1,2]))/2; % Vector C7-mean(RPSI,LPSI)
    vec2      = repmat([0 1 0],[size(vec1,1),1]); % Vector ICS_Y
    Trial     = DetectEvents(Trial,vec1,vec2,type,threshold);
    % Event misdetection manual correction NSLBP-BIO-028 | REL session    
    if strcmp(Participant.id,'NSLBP-BIO-028') && strcmp(Session.type,'REL') 
        if contains(Trial.type,'S2S_Unconstrained')
            Trial.Event(9).value = [Trial.Event(9).value(1) 900 1812 Trial.Event(9).value(end)];
            Trial.Event(10).value = [556 1301 2157];    
        end
    end
    clear type threshold vec1 vec2;
end