% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : June 2020
% -------------------------------------------------------------------------
% Description  : Define parameters of each body segment
% Inputs       : Participant (structure), Static (structure),
%                Trial (structure)
% Outputs      : Trial (structure)
% -------------------------------------------------------------------------
% Dependencies : - 3D Kinematics and Inverse Dynamics toolbox by Rapha�l Dumas: https://fr.mathworks.com/matlabcentral/fileexchange/58021-3d-kinematics-and-inverse-dynamics?s_tid=prof_contriblnk
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function Trial = DefineSegments(Participant,Static,Trial)

% -------------------------------------------------------------------------
% Pelvis parameters
% -------------------------------------------------------------------------
% Extract marker trajectories
RASI = Trial.Marker(1).Trajectory.smooth;
RILC = Trial.Marker(2).Trajectory.smooth;
RPSI = Trial.Marker(3).Trajectory.smooth;
LPSI = Trial.Marker(4).Trajectory.smooth;
LILC = Trial.Marker(5).Trajectory.smooth;
LASI = Trial.Marker(6).Trajectory.smooth;
S1   = Trial.Marker(7).Trajectory.smooth;
% Pelvis axes (Dumas and Wojtusch 2018)
Z5 = Vnorm_array3(RASI-LASI);
Y5 = Vnorm_array3(cross(RASI-(RPSI+LPSI)/2, ...
                        LASI-(RPSI+LPSI)/2));
X5 = Vnorm_array3(cross(Y5,Z5));
% Determination of the lumbar joint centre by regression (Dumas and Wojtusch 2018)
if ~isempty(Static)
    % Determination of the lumbar joint centre by singular value decomposition
    % based on the static record
    RASIs = Static.Marker(1).Trajectory.smooth;
    RILCs = Static.Marker(2).Trajectory.smooth;
    RPSIs = Static.Marker(3).Trajectory.smooth;
    LPSIs = Static.Marker(4).Trajectory.smooth;
    LILCs = Static.Marker(5).Trajectory.smooth;
    LASIs = Static.Marker(6).Trajectory.smooth;
    S1s   = Static.Marker(7).Trajectory.smooth;
    LJCs  = Static.Vmarker(9).Trajectory.smooth;
    for t = 1:Trial.n1
        [R5,d5,rms5] = soder([RASIs';RPSIs';LPSIs';LASIs';S1s'],...
                             [RASI(:,:,t)';RPSI(:,:,t)';LPSI(:,:,t)';LASI(:,:,t)';S1(:,:,t)']);  
        LJC(:,:,t)   = R5*LJCs+d5;
        clear R5 d5 rms5;
    end
else
    % Pelvis width
    W5 = mean(sqrt(sum((RASI-LASI).^2)));
    % Determination of the lumbar joint centre by regression (Dumas and Wojtusch 2018)
    if strcmp(Participant.gender,'Female')
        LJC(1) = -34.0/100;
        LJC(2) = 4.9/100;
        LJC(3) = 0.0/100;
    elseif strcmp(Participant.gender,'Male')
        LJC(1) = -33.5/100;
        LJC(2) = -3.2/100;
        LJC(3) = 0.0/100;
    end
    LJC = (RASI+LASI)/2 + ...
          LJC(1)*W5*X5 + LJC(2)*W5*Y5 + LJC(3)*W5*Z5;
end
% Store virtual marker
Trial.Vmarker(9).label             = 'LJC';
Trial.Vmarker(9).Trajectory.smooth = LJC;  
% Determination of the hip joint centre by regression (Dumas and Wojtusch 2018)
if ~isempty(Static)
    % Determination of the hip joint centre by singular value decomposition
    % based on the static record
    RASIs = Static.Marker(1).Trajectory.smooth;
    RILCs = Static.Marker(2).Trajectory.smooth;
    RPSIs = Static.Marker(3).Trajectory.smooth;
    LPSIs = Static.Marker(4).Trajectory.smooth;
    LILCs = Static.Marker(5).Trajectory.smooth;
    LASIs = Static.Marker(6).Trajectory.smooth;
    S1s   = Static.Marker(7).Trajectory.smooth;
    RHJCs = Static.Vmarker(4).Trajectory.smooth;
    LHJCs = Static.Vmarker(8).Trajectory.smooth;
    for t = 1:Trial.n1
        [R5,d5,rms5] = soder([RASIs';RPSIs';LPSIs';LASIs';S1s'],...
                             [RASI(:,:,t)';RPSI(:,:,t)';LPSI(:,:,t)';LASI(:,:,t)';S1(:,:,t)']);  
        RHJC(:,:,t)  = R5*RHJCs+d5;  
        LHJC(:,:,t)  = R5*LHJCs+d5;
        clear R5 d5 rms5;
    end
else
    % Determination of the hip joint centre by regression (Dumas and Wojtusch 2018)
    if strcmp(Participant.gender,'Female')
        R_HJC(1) = -13.9/100;
        R_HJC(2) = -33.6/100;
        R_HJC(3) = 37.2/100;
        L_HJC(1) = -13.9/100;
        L_HJC(2) = -33.6/100;
        L_HJC(3) = -37.2/100;
    elseif strcmp(Participant.gender,'Male')
        R_HJC(1) = -9.5/100;
        R_HJC(2) = -37.0/100;
        R_HJC(3) = 36.1/100;
        L_HJC(1) = -9.5/100;
        L_HJC(2) = -37.0/100;
        L_HJC(3) = -36.1/100;
    end
    RHJC = (RASI+LASI)/2 + ...
           R_HJC(1)*W5*X5 + R_HJC(2)*W5*Y5 + R_HJC(3)*W5*Z5;
    LHJC = (RASI+LASI)/2 + ...
           L_HJC(1)*W5*X5 + L_HJC(2)*W5*Y5 + L_HJC(3)*W5*Z5;
end
% Store virtual markers
Trial.Vmarker(4).label             = 'RHJC';
Trial.Vmarker(4).Trajectory.smooth = RHJC;  
Trial.Vmarker(8).label             = 'LHJC';
Trial.Vmarker(8).Trajectory.smooth = LHJC;  
% Pelvis parameters (Dumas and Ch�ze 2007) = Pelvis duplicated to manage
% different kinematic chains
rP5                         = LJC;
rD5                         = (RHJC+LHJC)/2;
w5                          = Z5;
u5                          = X5;
Trial.Segment(5).Q.smooth   = [u5;rP5;rD5;w5];
Trial.Segment(5).rM.smooth  = [RASI,LASI,RPSI,LPSI,RILC,LILC,S1];
Trial.Segment(10).Q.smooth  = [u5;rP5;rD5;w5];
Trial.Segment(10).rM.smooth = [RASI,LASI,RPSI,LPSI,RILC,LILC,S1];
Trial.Segment(14).Q.smooth  = [u5;rP5;rD5;w5];
Trial.Segment(14).rM.smooth = [RASI,LASI,RPSI,LPSI,RILC,LILC,S1];

% -------------------------------------------------------------------------
% Right femur parameters
% -------------------------------------------------------------------------
% Extract marker trajectories
RGTR = Trial.Marker(8).Trajectory.smooth;
RTHI = Trial.Marker(9).Trajectory.smooth;
RKNE = Trial.Marker(10).Trajectory.smooth;
RKNM = Trial.Marker(11).Trajectory.smooth;
% Knee joint centre
RKJC = (RKNE+RKNM)/2;
% Store virtual marker
Trial.Vmarker(3).label             = 'RKJC';
Trial.Vmarker(3).Trajectory.smooth = RKJC;  
% Femur axes (Dumas and Wojtusch 2018)
Y4 = Vnorm_array3(RHJC-RKJC);
X4 = Vnorm_array3(cross(RKNE-RHJC,RKJC-RHJC));
Z4 = Vnorm_array3(cross(X4,Y4));
% Femur parameters (Dumas and Ch�ze 2007)
rP4                        = RHJC;
rD4                        = RKJC;
w4                         = Z4;
u4                         = X4;
Trial.Segment(4).Q.smooth  = [u4;rP4;rD4;w4];
Trial.Segment(4).rM.smooth = [RGTR,RHJC,RKNE,RKNM]; % Not sure about constant orientation of RTHI

% -------------------------------------------------------------------------
% Right Tibia/fibula parameters
% -------------------------------------------------------------------------
% Extract marker trajectories
RFAX = Trial.Marker(12).Trajectory.smooth;
RTTA = Trial.Marker(13).Trajectory.smooth;
RTIB = Trial.Marker(14).Trajectory.smooth;
RANK = Trial.Marker(15).Trajectory.smooth;
RMED = Trial.Marker(16).Trajectory.smooth;
% Ankle joint centre
RAJC = (RANK+RMED)/2;
% Store virtual marker
Trial.Vmarker(2).label             = 'RAJC';
Trial.Vmarker(2).Trajectory.smooth = RAJC;  
% Tibia/fibula axes (Dumas and Wojtusch 2018)
Y3 = Vnorm_array3(RKJC-RAJC);
X3 = Vnorm_array3(cross(RAJC-RFAX,RKJC-RFAX));
Z3 = Vnorm_array3(cross(X3,Y3));
% Tibia/fibula parameters (Dumas and Ch�ze 2007)
rP3                        = RKJC;
rD3                        = RAJC;
w3                         = Z3;
u3                         = X3;
Trial.Segment(3).Q.smooth  = [u3;rP3;rD3;w3];
Trial.Segment(3).rM.smooth = [RFAX,RTTA,RANK,RMED]; % Not sure about constant orientation of RTIB

% -------------------------------------------------------------------------
% Right foot parameters
% -------------------------------------------------------------------------
% Extract marker trajectories
RHEE = Trial.Marker(17).Trajectory.smooth;
RFMH = Trial.Marker(19).Trajectory.smooth;
RVMH = Trial.Marker(21).Trajectory.smooth;
% Metatarsal joint centre (Dumas and Wojtusch 2018)
RMJC = (RFMH+RVMH)/2;
% Store virtual marker
Trial.Vmarker(1).label             = 'RMJC';
Trial.Vmarker(1).Trajectory.smooth = RMJC;  
% Foot axes (Dumas and Wojtusch 2018)
X2 = Vnorm_array3(RMJC-RHEE);
Y2 = Vnorm_array3(cross(RVMH-RHEE,RFMH-RHEE));
Z2 = Vnorm_array3(cross(X2,Y2));
% Foot parameters (Dumas and Ch�ze 2007)
rP2                        = RAJC;
rD2                        = RMJC;
w2                         = Z2;
u2                         = X2;
Trial.Segment(2).Q.smooth  = [u2;rP2;rD2;w2];
Trial.Segment(2).rM.smooth = [RHEE,RFMH,RVMH];

% -------------------------------------------------------------------------
% Left femur parameters
% -------------------------------------------------------------------------
% Extract marker trajectories
LGTR = Trial.Marker(23).Trajectory.smooth;
LTHI = Trial.Marker(24).Trajectory.smooth;
LKNE = Trial.Marker(25).Trajectory.smooth;
LKNM = Trial.Marker(26).Trajectory.smooth;
% Knee joint centre
LKJC = (LKNE+LKNM)/2;
% Store virtual marker
Trial.Vmarker(7).label             = 'LKJC';
Trial.Vmarker(7).Trajectory.smooth = LKJC;  
% Femur axes (Dumas and Wojtusch 2018)
Y9 = Vnorm_array3(LHJC-LKJC);
X9 = -Vnorm_array3(cross(LKNE-LHJC,LKJC-LHJC));
Z9 = Vnorm_array3(cross(X9,Y9));
% Femur parameters (Dumas and Ch�ze 2007)
rP9                        = LHJC;
rD9                        = LKJC;
w9                         = Z9;
u9                         = X9;
Trial.Segment(9).Q.smooth  = [u9;rP9;rD9;w9];
Trial.Segment(9).rM.smooth = [LGTR,LHJC,LKNE,LKNM]; % Not sure about constant orientation of LTHI

% -------------------------------------------------------------------------
% Left Tibia/fibula parameters
% -------------------------------------------------------------------------
% Extract marker trajectories
LFAX = Trial.Marker(27).Trajectory.smooth;
LTTA = Trial.Marker(28).Trajectory.smooth;
LTIB = Trial.Marker(29).Trajectory.smooth;
LANK = Trial.Marker(30).Trajectory.smooth;
LMED = Trial.Marker(31).Trajectory.smooth;
% Ankle joint centre
LAJC = (LANK+LMED)/2;
% Store virtual marker
Trial.Vmarker(6).label             = 'LAJC';
Trial.Vmarker(6).Trajectory.smooth = LAJC;  
% Tibia/fibula axes (Dumas and Wojtusch 2018)
Y8 = Vnorm_array3(LKJC-LAJC);
X8 = -Vnorm_array3(cross(LAJC-LFAX,LKJC-LFAX));
Z8 = Vnorm_array3(cross(X8,Y8));
% Tibia/fibula parameters (Dumas and Ch�ze 2007)
rP8                        = LKJC;
rD8                        = LAJC;
w8                         = Z8;
u8                         = X8;
Trial.Segment(8).Q.smooth  = [u8;rP8;rD8;w8];
Trial.Segment(8).rM.smooth = [LFAX,LTTA,LANK,LMED]; % Not sure about constant orientation of LTIB

% -------------------------------------------------------------------------
% Left foot parameters
% -------------------------------------------------------------------------
% Extract marker trajectories
LHEE = Trial.Marker(32).Trajectory.smooth;
LFMH = Trial.Marker(34).Trajectory.smooth;
LVMH = Trial.Marker(36).Trajectory.smooth;
% Metatarsal joint centre (Dumas and Wojtusch 2018)
LMJC = (LFMH+LVMH)/2;
% Store virtual marker
Trial.Vmarker(5).label             = 'LMJC';
Trial.Vmarker(5).Trajectory.smooth = LMJC;  
% Foot axes (Dumas and Wojtusch 2018)
X7 = Vnorm_array3(LMJC-LHEE);
Y7 = -Vnorm_array3(cross(LVMH-LHEE,LFMH-LHEE));
Z7 = Vnorm_array3(cross(X7,Y7));
% Foot parameters (Dumas and Ch�ze 2007)
rP7                        = LAJC;
rD7                        = LMJC;
w7                         = Z7;
u7                         = X7;
Trial.Segment(7).Q.smooth  = [u7;rP7;rD7;w7];
Trial.Segment(7).rM.smooth = [LHEE,LFMH,LVMH];

% -------------------------------------------------------------------------
% Lumbar parameters
% -------------------------------------------------------------------------
% Extract marker trajectories
CLAV = Trial.Marker(48).Trajectory.smooth;
STRN = Trial.Marker(49).Trajectory.smooth;
C7   = Trial.Marker(41).Trajectory.smooth;
T8   = Trial.Marker(45).Trajectory.smooth;
T12  = Trial.Marker(47).Trajectory.smooth;
L1   = Trial.Marker(38).Trajectory.smooth;
% Determination of thoracic joint centre by regression (Dumas and Wojtusch 2018)
if ~isempty(Static)
    % Determination of the joint centre by singular value decomposition
    % based on the static record
    CLAVs = Static.Marker(48).Trajectory.smooth;
    STRNs = Static.Marker(49).Trajectory.smooth;
    C7s   = Static.Marker(41).Trajectory.smooth;
    T8s   = Static.Marker(45).Trajectory.smooth;
    T12s  = Static.Marker(46).Trajectory.smooth; % T10 instead (T12 not recorded)
    L1s   = Static.Marker(38).Trajectory.smooth;
    TJCs  = Static.Vmarker(10).Trajectory.smooth;
    for t = 1:Trial.n1
        [R11,d11,rms11] = soder([CLAVs';STRNs';C7s';T8s';T12s';L1s'],...
                                [CLAV(:,:,t)';STRN(:,:,t)';C7(:,:,t)';T8(:,:,t)';T12(:,:,t)';L1(:,:,t)']);  
        TJC(:,:,t)      = R11*TJCs+d11;  
        clear R11 d11 rms11;
    end 
else
    % Thorax width (Dumas and Wojtusch 2018)
    W11 = mean(sqrt(sum((CLAV-C7).^2)));
    % Determination of thoracic joint centre by regression (Dumas and Wojtusch 2018)
    tX11 = Vnorm_array3(STRN-T8);
    tY11 = Vnorm_array3(T8-T12);
    tZ11 = cross(tX11,tY11);
    tX11 = cross(tY11,tZ11);
    if strcmp(Participant.gender,'Female')
        angle = 92;
        coeff = 0.50;
    elseif strcmp(Participant.gender,'Male')
        angle = 94;
        coeff = 0.52;
    end
    R11 = [cosd(angle) sind(angle) 0 0; ...
           -sind(angle) cosd(angle) 0 0;
           0 0 1 0; ...
           0 0 0 1];
    TJC = Mprod_array3(Mprod_array3([tX11 tY11 tZ11 T12; ...
                       repmat([0 0 0 1],[1,1,size(T12,3)])], ...
                       repmat(R11,[1,1,size(T12,3)])), ...
                       repmat([0; coeff*W11; 0; 1],[1,1,size(T12,3)]));
    TJC = TJC(1:3,:,:);
end
% Store virtual marker
Trial.Vmarker(10).label             = 'TJC';
Trial.Vmarker(10).Trajectory.smooth = TJC;  
% Lumbar axes (Dumas and Wojtusch 2018)
Y11 = Vnorm_array3(TJC-LJC);
Z11 = Z5; % no axial rotation at lumbar joint centre assumed
X11 = Vnorm_array3(cross(Y11,Z11));
% Lumbar parameters
rP11                        = TJC;
rD11                        = LJC;
w11                         = Z11;
u11                         = X11;
Trial.Segment(11).Q.smooth  = [u11;rP11;rD11;w11];
Trial.Segment(11).rM.smooth = [TJC,RPSI,LPSI]; % no axial rotation at lumbar joint centre assumed

% -------------------------------------------------------------------------
% Thorax parameters
% -------------------------------------------------------------------------
% Thorax width (Dumas and Wojtusch 2018)
if ~isempty(Static)
    % Determination of the joint centre by singular value decomposition
    % based on the static record
    CLAVs = Static.Marker(48).Trajectory.smooth;
    STRNs = Static.Marker(49).Trajectory.smooth;
    C7s   = Static.Marker(41).Trajectory.smooth;
    T8s   = Static.Marker(45).Trajectory.smooth;
    T12s  = Static.Marker(47).Trajectory.smooth; % T10 instead (T12 not recorded)
    L1s   = Static.Marker(38).Trajectory.smooth;
    CJCs  = Static.Vmarker(11).Trajectory.smooth;
    for t = 1:Trial.n1
        [R12,d12,rms12] = soder([CLAVs';STRNs';C7s';T8s';T12s';L1s'],...
                                [CLAV(:,:,t)';STRN(:,:,t)';C7(:,:,t)';T8(:,:,t)';T12(:,:,t)';L1(:,:,t)']);  
        CJC(:,:,t)      = R12*CJCs+d12;  
        clear R12 d12 rms12;
    end
else
    % Thorax width (Dumas and Wojtusch 2018)
    W12 = mean(sqrt(sum((CLAV-C7).^2)));
    % Determination of the cervical joint centre by regression (Dumas and Wojtusch 2018)
    tX12 = Vnorm_array3(CLAV-C7);
    tZ12 = Vnorm_array3(cross(STRN-C7,CLAV-C7));
    tY12 = Vnorm_array3(cross(tZ12,tX12));
    if strcmp(Participant.gender,'Female')
        angle = -14;
        coeff = 0.53;
    elseif strcmp(Participant.gender,'Male')
        angle = -8;
        coeff = 0.55;
    end
    R12 = [cosd(angle) sind(angle) 0 0; ...
           -sind(angle) cosd(angle) 0 0;
           0 0 1 0; ...
           0 0 0 1];
    CJC = Mprod_array3(Mprod_array3([tX12 tY12 tZ12 C7; ...
                       repmat([0 0 0 1],[1,1,size(C7,3)])], ...
                       repmat(R12,[1,1,size(C7,3)])), ...
                       repmat([coeff*W12; 0; 0; 1],[1,1,size(C7,3)]));
    CJC = CJC(1:3,:,:);
end
% Store virtual marker
Trial.Vmarker(11).label             = 'CJC';
Trial.Vmarker(11).Trajectory.smooth = CJC;  
% Thorax axes (Dumas and Ch�ze 2007)
Y12 = Vnorm_array3(CJC-TJC);
Z12 = Vnorm_array3(cross(CLAV-TJC,CJC-TJC));
X12 = Vnorm_array3(cross(Y12,Z12));
% Thorax parameters
rP12                        = CJC;
rD12                        = TJC;
w12                         = Z12;
u12                         = X12;
Trial.Segment(12).Q.smooth  = [u12;rP12;rD12;w12];
Trial.Segment(12).rM.smooth = [CLAV,C7,STRN,TJC];

% -------------------------------------------------------------------------
% Head with neck parameters
% -------------------------------------------------------------------------
% Extract marker trajectories
RFHD = Trial.Marker(50).Trajectory.smooth;
RBHD = Trial.Marker(51).Trajectory.smooth;
LFHD = Trial.Marker(52).Trajectory.smooth;
LBHD = Trial.Marker(53).Trajectory.smooth;
% Head vertex (Dumas and Wojtusch 2018)
VER = (RFHD+RBHD+LFHD+LBHD)/4; % assimilated to the head vertex described in Dumas and Wojtusch 2018
% Store virtual marker
Trial.Vmarker(12).label             = 'VER';
Trial.Vmarker(12).Trajectory.smooth = VER;  
% Head axes
Y13 = Vnorm_array3(VER-CJC);
Z13 = Vnorm_array3(cross((RFHD+LFHD)/2-CJC,(RBHD+LBHD)/2-CJC));
X13 = Vnorm_array3(cross(Y13,Z13));
% Head parameters
rP13                        = VER;
rD13                        = CJC;
w13                         = Z13;
u13                         = X13;
Trial.Segment(13).Q.smooth  = [u13;rP13;rD13;w13];
Trial.Segment(13).rM.smooth = [RFHD,RBHD,LFHD,LBHD];
Trial.Segment(19).Q.smooth  = [u13;rP13;rD13;w13];
Trial.Segment(19).rM.smooth = [RFHD,RBHD,LFHD,LBHD];

% -------------------------------------------------------------------------
% Lower lumbar parameters (Hidalgo et al. 2012)
% -------------------------------------------------------------------------
% Extract marker trajectories
S1 = Trial.Marker(7).Trajectory.smooth;
L3 = Trial.Marker(39).Trajectory.smooth;
% Lower lumbar axes
Y15 = Vnorm_array3(L3-S1);
Z15 = Z11;
X15 = cross(Y15,Z15);
% Lower lumbar parameters
rP15                        = L3;
rD15                        = S1;
w15                         = Z15;
u15                         = X15;
Trial.Segment(15).Q.smooth  = [u15;rP15;rD15;w15];
Trial.Segment(15).rM.smooth = [S1,L3];

% -------------------------------------------------------------------------
% Upper lumbar parameters (Hidalgo et al. 2012)
% -------------------------------------------------------------------------
% Upper lumbar axes
Y16 = Vnorm_array3(T12-L3);
Z16 = Z11;
X16 = cross(Y16,Z16);
% Upper lumbar parameters
rP16                        = T12;
rD16                        = L3;
w16                         = Z16;
u16                         = X16;
Trial.Segment(16).Q.smooth  = [u16;rP16;rD16;w16];
Trial.Segment(16).rM.smooth = [T12,L3];

% -------------------------------------------------------------------------
% Lower thorax parameters (Hidalgo et al. 2012)
% -------------------------------------------------------------------------
% Lower thorax axes
Y17 = Vnorm_array3(T8-T12);
Z17 = Z12;
X17 = cross(Y17,Z17);
% Lower lumbar parameters
rP17                        = T8;
rD17                        = T12;
w17                         = Z17;
u17                         = X17;
Trial.Segment(17).Q.smooth  = [u17;rP17;rD17;w17];
Trial.Segment(17).rM.smooth = [T8,T12];

% -------------------------------------------------------------------------
% Upper thorax parameters (Hidalgo et al. 2012)
% -------------------------------------------------------------------------
% Lower thorax axes
Y18 = Vnorm_array3(C7-T8);
Z18 = Z12;
X18 = cross(Y18,Z18);
% Lower lumbar parameters
rP18                        = C7;
rD18                        = T8;
w18                         = Z18;
u18                         = X18;
Trial.Segment(18).Q.smooth  = [u18;rP18;rD18;w18];
Trial.Segment(18).rM.smooth = [C7,T8];