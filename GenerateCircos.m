% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : June 2020
% -------------------------------------------------------------------------
% Description  : Generate table files for CIRCOS plots
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% INIT THE WORKSPACE
% -------------------------------------------------------------------------
clearvars;
close all;
warning ('off','all');
clc;

% -------------------------------------------------------------------------
% SET FOLDERS
% -------------------------------------------------------------------------
Folder.toolbox      = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\';
Folder.data         = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO\Data\';
Folder.biomarkers   = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\data\';
Folder.R            = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\R\Outputs\';
Folder.systematic   = 'C:\Users\moissene\Documents\Professionnel\publications\articles\1- en cours\Moissenet - Biomarker movement\1- Draft\circos\systematicReviewData\';
Folder.export       = 'C:\Users\moissene\Documents\Professionnel\publications\articles\1- en cours\Moissenet - Biomarker movement\1- Draft\circos\';
Folder.dependencies = [Folder.toolbox,'dependencies\'];
addpath(Folder.toolbox);
addpath(genpath(Folder.dependencies));

% -------------------------------------------------------------------------
% TABLE: biomarkers_movement.data.txt
% -------------------------------------------------------------------------
% Get biomarkers IDs
cd(Folder.biomarkers);
load('Biomarker_control.mat');
temp = fieldnames(Biomarker_control);
for itemp = 2:size(temp,1)
    BMlist{itemp-1} = temp{itemp};
end
clear temp itemp;
% Write table
fileName  = 'biomarkers_movement.data.txt';
for iBM = 1:size(BMlist,2)
    Col1{iBM,1} = 'chr';
    Col2{iBM,1} = '-';
    Col3{iBM,1} = BMlist{iBM};
    Col4{iBM,1} = BMlist{iBM};
    Col5{iBM,1} = 0;
    Col6{iBM,1} = 100;
    Col7{iBM,1} = 'white';
end
dataTable = table(Col1,Col2,Col3,Col4,Col5,Col6,Col7);
cd(Folder.export);
writetable(dataTable,fileName,'Delimiter','\t','WriteVariableNames',0);
clear Col1 Col2 Col3 Col4 Col5 Col6 Col7 dataTable iBM fileName;

% -------------------------------------------------------------------------
% TABLES RELATED TO BIOMARKER DEFINITION
% Information based on the systematic review
% -------------------------------------------------------------------------
defList = {'ICF1','ICF2','ICF3','ICF4','ICF5',...
           'variable_type1','variable_type2','variable_type3','variable_type4','variable_type5',...
           'region1','region2','region3','region4','region5','region6','region7','region8','region9',...
           'region10','region11','region12','region13','region14','region15'};
for idef = 1:size(defList,2)
    % Get previously defined value for each biomarker
    cd(Folder.systematic);
    oldData = readtable(['biomarkers_movement.',defList{idef},'.heat.txt']);
    for iBM = 1:size(BMlist,2)
        for idata = 1:size(oldData,1)
            if strcmp(BMlist{iBM},cell2mat(table2array(oldData(idata,1)))) == 1
                newData(iBM) = table2array(oldData(idata,4));
            end
        end
    end
    clear oldData iBM idata;
    % Write table
    fileName  = ['biomarkers_movement.',defList{idef},'.heat.txt'];
    for iBM = 1:size(BMlist,2)
        Col1{iBM,1} = BMlist{iBM};
        Col2{iBM,1} = 0;
        Col3{iBM,1} = 100;
        Col4{iBM,1} = newData(iBM);
    end
    dataTable = table(Col1,Col2,Col3,Col4);
    cd(Folder.export);
    writetable(dataTable,fileName,'Delimiter','\t','WriteVariableNames',0);
    clear Col1 Col2 Col3 Col4 Col5 Col6 Col7 dataTable fileName newData iBM;
end
clear idef defList;

% -------------------------------------------------------------------------
% TABLES RELATED TO BIOMARKER PSYCHOMETRIC PROPERTIES
% New information
% -------------------------------------------------------------------------
% Reliability parameters
cd(Folder.R);
paramList   = {'reliability1','reliability2',... % ICCintra, SEM%intra
               'reliability3','reliability4'};   % ICCtest, SEM%test
paramID     = [4,8,...
               5,9];     
reliability = readtable('Reliability.csv');
for iparam = 1:size(paramList,2)
    for iBM = 1:size(BMlist,2)
        for idata = 1:size(reliability,1)
            temp = cell2mat(table2array(reliability(idata,2)));
            if strcmp(temp(1:7),'Control') && contains(temp,[BMlist{iBM},'.csv'])
                Col11{iBM,1} = BMlist{iBM};
                Col12{iBM,1} = 0;
                Col13{iBM,1} = 100;
                Col14{iBM,1} = 1;
                if iparam == 1 || iparam == 3 % ICC values
                    if str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) < 0.5
                        Col15{iBM,1} = 'color=white';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) >= 0.5 && str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) < 0.75
                        Col15{iBM,1} = 'color=yellow';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) >= 0.75 && str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) < 0.90
                        Col15{iBM,1} = 'color=blue';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) >= 0.9
                        Col15{iBM,1} = 'color=green';
                    end
                elseif iparam == 2 || iparam == 4 % SEM% values
                    if str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) > 50
                        Col15{iBM,1} = 'color=white';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) <= 50 && str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) > 33
                        Col15{iBM,1} = 'color=yellow';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) <= 33 && str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) > 16.5
                        Col15{iBM,1} = 'color=blue';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) <= 16.5
                        Col15{iBM,1} = 'color=green';
                    end
                end
            end
            if strcmp(temp(1:7),'Patient') && contains(temp,[BMlist{iBM},'.csv'])
                Col21{iBM,1} = BMlist{iBM};
                Col22{iBM,1} = 0;
                Col23{iBM,1} = 100;
                Col24{iBM,1} = 1;
                if iparam == 1 || iparam == 3 % ICC values
                    if str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) < 0.5
                        Col25{iBM,1} = 'color=white';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) >= 0.5 && str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) < 0.75
                        Col25{iBM,1} = 'color=yellow';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) >= 0.75 && str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) < 0.90
                        Col25{iBM,1} = 'color=blue';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) >= 0.9
                        Col25{iBM,1} = 'color=green';
                    end
                elseif iparam == 2 || iparam == 4 % SEM% values
                    if str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) > 50
                        Col25{iBM,1} = 'color=white';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) <= 50 && str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) > 33
                        Col25{iBM,1} = 'color=yellow';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) <= 33 && str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) > 16.5
                        Col25{iBM,1} = 'color=blue';
                    elseif str2double(cell2mat(table2array(reliability(idata,paramID(iparam))))) <= 16.5
                        Col25{iBM,1} = 'color=green';
                    end
                end
            end
            clear temp;
        end
        clear idata;
    end
    % Write table
    fileName  = ['biomarkers_movement.',paramList{iparam},'_control','.heat.txt'];
    dataTable = table(Col11,Col12,Col13,Col14,Col15);
    cd(Folder.export);
    writetable(dataTable,fileName,'Delimiter','\t','WriteVariableNames',0);
    clear fileName dataTable;
    fileName  = ['biomarkers_movement.',paramList{iparam},'_patient','.heat.txt'];
    dataTable = table(Col21,Col22,Col23,Col24,Col25);
    cd(Folder.export);
    writetable(dataTable,fileName,'Delimiter','\t','WriteVariableNames',0);
    clear fileName dataTable;
    clear Col11 Col12 Col13 Col14 Col15 Col21 Col22 Col23 Col24 Col25 iBM;
end
clear paramList paramID reliability iparam;

% Validity parameters
cd(Folder.R);
paramList = {'validity1','validity2'}; % p-value, AUC
paramID   = [5,6]; 
validity  = readtable('Validity.csv');
for iparam = 1:size(paramList,2)
    for iBM = 1:size(BMlist,2)   
        for idata = 1:size(validity,1)   
            temp = cell2mat(table2array(validity(idata,2)));   
            if contains(temp,[BMlist{iBM},'.csv'])
                Col11{iBM,1} = BMlist{iBM};
                Col12{iBM,1} = 0;
                Col13{iBM,1} = 100;
                Col14{iBM,1} = 1;
                if iparam == 1 % p-values
                    if str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) > 0.05
                        Col15{iBM,1} = 'color=white';
                    elseif str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) <= 0.05 && str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) > 0.01
                        Col15{iBM,1} = 'color=yellow';
                    elseif str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) <= 0.01 && str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) > 0.001
                        Col15{iBM,1} = 'color=blue';
                    elseif str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) <= 0.001
                        Col15{iBM,1} = 'color=green';
                    end
                elseif iparam == 2 % AUC
                    if str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) < 60
                        Col15{iBM,1} = 'color=white';
                    elseif str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) >= 60 && str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) < 70
                        Col15{iBM,1} = 'color=yellow';
                    elseif str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) >= 70 && str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) < 80
                        Col15{iBM,1} = 'color=blue';
                    elseif str2double(cell2mat(table2array(validity(idata,paramID(iparam))))) >= 80
                        Col15{iBM,1} = 'color=green';
                    end
                end
            end
            clear temp;    
        end
        clear idata;
    end
    % Write table
    fileName  = ['biomarkers_movement.',paramList{iparam},'.heat.txt'];
    dataTable = table(Col11,Col12,Col13,Col14,Col15);
    cd(Folder.export);
    writetable(dataTable,fileName,'Delimiter','\t','WriteVariableNames',0);
    clear fileName dataTable;
    clear Col11 Col12 Col13 Col14 Col15 Col21 Col22 Col23 Col24 Col25 iBM;
end 
clear paramList paramID validity iparam;  

% Interpretability parameters
cd(Folder.R);
paramList        = {'interpretability'};   % MDC% test patient
paramID          = [7];     
interpretability = readtable('Interpretability.csv');
for iparam = 1:size(paramList,2)
    for iBM = 1:size(BMlist,2)
        for idata = 1:size(interpretability,1)
            temp = cell2mat(table2array(interpretability(idata,2)));
            if strcmp(temp(1:7),'Patient') && contains(temp,[BMlist{iBM},'.csv'])
                Col11{iBM,1} = BMlist{iBM};
                Col12{iBM,1} = 0;
                Col13{iBM,1} = 100;
                Col14{iBM,1} = 1;
                if iparam == 1
                    if str2double(cell2mat(table2array(interpretability(idata,paramID(iparam))))) > 50
                        Col15{iBM,1} = 'color=white';
                    elseif str2double(cell2mat(table2array(interpretability(idata,paramID(iparam))))) <= 50 && str2double(cell2mat(table2array(interpretability(idata,paramID(iparam))))) > 33
                        Col15{iBM,1} = 'color=yellow';
                    elseif str2double(cell2mat(table2array(interpretability(idata,paramID(iparam))))) <= 33 && str2double(cell2mat(table2array(interpretability(idata,paramID(iparam))))) > 16.5
                        Col15{iBM,1} = 'color=blue';
                    elseif str2double(cell2mat(table2array(interpretability(idata,paramID(iparam))))) <= 16.5
                        Col15{iBM,1} = 'color=green';
                    end
                end
            end
            clear temp;
        end
        clear idata;
    end
    % Write table
    fileName  = ['biomarkers_movement.',paramList{iparam},'_patient','.heat.txt'];
    dataTable = table(Col11,Col12,Col13,Col14,Col15);
    cd(Folder.export);
    writetable(dataTable,fileName,'Delimiter','\t','WriteVariableNames',0);
    clear fileName dataTable;
    clear Col11 Col12 Col13 Col14 Col15 Col21 Col22 Col23 Col24 Col25 iBM;
end
clear paramList paramID interpretability iparam;

% -------------------------------------------------------------------------
% GENERATE CIRCOS PLOT
% -------------------------------------------------------------------------
system('cd C:\Users\moissene\Documents\Professionnel\publications\articles\1- en cours\Moissenet - Biomarker movement\1- Draft\circos');
system('perl C:\circos\circos-0.69-9\bin\circos -conf circos.conf');
system('inkscape circos.svg');