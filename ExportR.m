% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : June 2020
% -------------------------------------------------------------------------
% Description  : Export data for R analysis
% -------------------------------------------------------------------------
% Dependencies : None
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% STORE AND EXPORT DATA FOR STATISTICAL ANALYSIS
% -------------------------------------------------------------------------
% Reliability controls
cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\data\');
load('Biomarker_control.mat');
nTrial = 3*ones(length(fieldnames(Biomarker_control))-1,1);
BMc = [];
nBM = fieldnames(Biomarker_control);
for iBM = 2:length(nBM)
    % Clear workspace
    clear Biomarker Group Participant Session Value Unit Operator Measure T;
    istat = 1;
    % Prepare data
    Biomarker = Biomarker_control.(nBM{iBM});
    eval([nBM{iBM},'=[];']);
    label = nBM{iBM}
    if nTrial(iBM-1) == 3
        for i = 1:size(Biomarker,1) % Participants      
            for j = 1:2 % Only INI and REL sessions 
                if ~isempty(Biomarker(i,1,1).value) && ~isempty(Biomarker(i,2,1).value) % Store only data within REL_session data
                    for k = 1%:size(Biomarker,3) % Only look at right side
                        % Store data
                        Group{istat,:}         = 'CTR';
                        Group{istat+1,:}       = 'CTR';
                        Group{istat+2,:}       = 'CTR';
                        Participant{istat,:}   = regexprep(Biomarker_control.participant{i},'NSLBP-BIO-0','P');
                        Participant{istat+1,:} = regexprep(Biomarker_control.participant{i},'NSLBP-BIO-0','P');
                        Participant{istat+2,:} = regexprep(Biomarker_control.participant{i},'NSLBP-BIO-0','P');
                        if j == 1
                            Session{istat,:}   = 'INI';
                            Session{istat+1,:} = 'INI';
                            Session{istat+2,:} = 'INI';
                        else
                            Session{istat,:}   = 'REL';
                            Session{istat+1,:} = 'REL';
                            Session{istat+2,:} = 'REL';
                        end           
                        Operator{istat,:}      = 'FM';
                        Operator{istat+1,:}    = 'FM';
                        Operator{istat+2,:}    = 'FM';  
                        Measure{istat,:}       = 'M1';
                        Measure{istat+1,:}     = 'M2';
                        Measure{istat+2,:}     = 'M3';
                        if ~isnan(Biomarker(i,j,k).value(1)) && Biomarker(i,j,k).value(1) < nanmean([Biomarker.value])+nanstd([Biomarker.value])*3
                            Value{istat,:}     = Biomarker(i,j,k).value(1);
                        else
                            Value{istat,:}     = '';
                        end
                        if ~isnan(Biomarker(i,j,k).value(2)) && Biomarker(i,j,k).value(2) < nanmean([Biomarker.value])+nanstd([Biomarker.value])*3
                            Value{istat+1,:}   = Biomarker(i,j,k).value(2);
                        else
                            Value{istat+1,:}   = '';
                        end
                        if size(Biomarker(i,j,k).value,2)>=3
                            if ~isnan(Biomarker(i,j,k).value(3)) && Biomarker(i,j,k).value(3) < nanmean([Biomarker.value])+nanstd([Biomarker.value])*3
                                Value{istat+2,:} = Biomarker(i,j,k).value(3);
                            else
                                Value{istat+2,:} = '';
                            end
                        else
                            Value{istat+2,:}   = '';
                        end
                        Unit{istat,:}          = Biomarker(i,j,k).units;  
                        Unit{istat+1,:}        = Biomarker(i,j,k).units;   
                        Unit{istat+2,:}        = Biomarker(i,j,k).units;             
                        istat                  = istat+3;
                    end
                end
            end
        end
        % Store values
        T = table(Group,Participant,Session,Operator,Measure,Value,Unit,...
                  'VariableNames',{'Group','Participant','Session','Operator','Measure','Value','Unit'});
        cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\R\Inputs_reliability_interpretability\');
        writetable(T,['Control_',label,'.csv'],'Delimiter',',');
    end
end

% Reliability patients
cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\data\');
load('Biomarker_patient.mat');
nTrial = 3*ones(length(fieldnames(Biomarker_control))-1,1);
BMc = [];
nBM = fieldnames(Biomarker_patient);
for iBM = 2:length(nBM)
    % Clear workspace
    clear Biomarker Group Participant Session Value Unit Operator Measure T;
    istat = 1;
    % Prepare data
    Biomarker = Biomarker_patient.(nBM{iBM});
    eval([nBM{iBM},'=[];']);
    if nTrial(iBM-1) == 3
        for i = 1:size(Biomarker,1)        
            for j = 1:2 % Only INI and REL sessions 
                if ~isempty(Biomarker(i,1,1).value) && ~isempty(Biomarker(i,2,1).value) % Store only data within REL_session data
                    label = nBM{iBM};
                    for k = 1%:size(Biomarker,3) % Only look at right side
                        % Store data
                        Group{istat,:}         = 'PAT';
                        Group{istat+1,:}       = 'PAT';
                        Group{istat+2,:}       = 'PAT';
                        Participant{istat,:}   = regexprep(Biomarker_patient.participant{i},'NSLBP-BIO-0','P');
                        Participant{istat+1,:} = regexprep(Biomarker_patient.participant{i},'NSLBP-BIO-0','P');
                        Participant{istat+2,:} = regexprep(Biomarker_patient.participant{i},'NSLBP-BIO-0','P');
                        if j == 1
                            Session{istat,:}   = 'INI';
                            Session{istat+1,:} = 'INI';
                            Session{istat+2,:} = 'INI';
                        else
                            Session{istat,:}   = 'REL';
                            Session{istat+1,:} = 'REL';
                            Session{istat+2,:} = 'REL';
                        end           
                        Operator{istat,:}      = 'FM';
                        Operator{istat+1,:}    = 'FM';
                        Operator{istat+2,:}    = 'FM';  
                        Measure{istat,:}       = 'M1';
                        Measure{istat+1,:}     = 'M2';
                        Measure{istat+2,:}     = 'M3';
                        if ~isnan(Biomarker(i,j,k).value(1)) && Biomarker(i,j,k).value(1) < nanmean([Biomarker.value])+nanstd([Biomarker.value])*3
                            Value{istat,:}     = Biomarker(i,j,k).value(1);
                        else
                            Value{istat,:}     = '';
                        end
                        if ~isnan(Biomarker(i,j,k).value(2)) && Biomarker(i,j,k).value(2) < nanmean([Biomarker.value])+nanstd([Biomarker.value])*3
                            Value{istat+1,:}   = Biomarker(i,j,k).value(2);
                        else
                            Value{istat+1,:}   = '';
                        end
                        if size(Biomarker(i,j,k).value,2)>=3
                            if ~isnan(Biomarker(i,j,k).value(3)) && Biomarker(i,j,k).value(3) < nanmean([Biomarker.value])+nanstd([Biomarker.value])*3
                                Value{istat+2,:} = Biomarker(i,j,k).value(3);
                            else
                                Value{istat+2,:} = '';
                            end
                        else
                            Value{istat+2,:}   = '';
                        end
                        Unit{istat,:}          = Biomarker(i,j,k).units;  
                        Unit{istat+1,:}        = Biomarker(i,j,k).units;   
                        Unit{istat+2,:}        = Biomarker(i,j,k).units;             
                        istat                  = istat+3;
                    end
                end
            end
        end 
        % Store values
        T = table(Group,Participant,Session,Operator,Measure,Value,Unit,...
                  'VariableNames',{'Group','Participant','Session','Operator','Measure','Value','Unit'});
        cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\R\Inputs_reliability_interpretability\');
        writetable(T,['Patient_',label,'.csv'],'Delimiter',',');
    end
end

% Validity
BMc = [];
nBM = fieldnames(Biomarker_control);
for iBM = 2:length(nBM)
    % Clear workspace
    clear Group Participant Session Value Unit Operator Measure T;
    istat = 1;
    % Prepare data
    BiomarkerC = Biomarker_control.(nBM{iBM});
    BiomarkerP = Biomarker_patient.(nBM{iBM});
    eval([nBM{iBM},'=[];']);
    for i = 1:size(BiomarkerC,1)        
        for j = 1 % Only INI sessions 
            if ~isempty(BiomarkerC(i,1,1).value)
                label = nBM{iBM};
                if strcmp(label,'BMo47')
                    test = 2;
                end
                for k = 1%:size(Biomarker,3) % Only look at right side
                    % Store control data
                    Group{istat,:}       = 'CTR';
                    Participant{istat,:} = regexprep(Biomarker_control.participant{i},'NSLBP-BIO-0','P');
                    Session{istat,:}     = 'INI';
                    Operator{istat,:}    = 'FM';
                    Measure{istat,:}     = 'MEAN';
                    temp                 = []; 
                    for ivalue = 1:length(BiomarkerC(i,j,k).value)
                        if ~isnan(BiomarkerC(i,j,k).value(ivalue)) && BiomarkerC(i,j,k).value(ivalue) < nanmean([BiomarkerC.value])+nanstd([BiomarkerC.value])*3
                            temp = [temp BiomarkerC(i,j,k).value(ivalue)];
                        else
                            temp = [temp NaN];
                        end
                    end
                    if ~isnan(nanmean(temp))
                        Value{istat,:}   = nanmean(temp); 
                    else
                        Value{istat,:}   = ''; 
                    end
                    Unit{istat,:}        = BiomarkerC.units;
                    istat                = istat+1;
                    clear temp;
                end
            end
        end
    end 
    for i = 1:size(BiomarkerP,1)        
        for j = 1 % Only INI sessions 
            if ~isempty(BiomarkerP(i,1,1).value)
                label = nBM{iBM};
                for k = 1%:size(Biomarker,3) % Only look at right side
                    % Store patient data
                    Group{istat,:}       = 'PAT';
                    Participant{istat,:} = regexprep(Biomarker_patient.participant{i},'NSLBP-BIO-0','P');
                    Session{istat,:}     = 'INI';
                    Operator{istat,:}    = 'FM';
                    Measure{istat,:}     = 'MEAN';
                    temp                 = []; 
                    for ivalue = 1:length(BiomarkerP(i,j,k).value)
                        if ~isnan(BiomarkerP(i,j,k).value(ivalue)) && BiomarkerP(i,j,k).value(ivalue) < nanmean([BiomarkerP.value])+nanstd([BiomarkerP.value])*3
                            temp = [temp BiomarkerP(i,j,k).value(ivalue)];
                        else
                            temp = [temp NaN];
                        end
                    end
                    if ~isnan(nanmean(temp))
                        Value{istat,:}   = nanmean(temp); 
                    else
                        Value{istat,:}   = ''; 
                    end
                    Unit{istat,:}        = BiomarkerP.units;         
                    istat                = istat+1;
                    clear temp;
                end
            end
        end
    end 
    % Store values
    T = table(Group,Participant,Session,Operator,Measure,Value,Unit,...
              'VariableNames',{'Group','Participant','Session','Operator','Measure','Value','Unit'});
    cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\R\Inputs_validity\');
    writetable(T,[label,'.csv'],'Delimiter',',');
end