Executable Commands
=====================

This section describes the different executable you can find within the *script*
folder of your virtual environement.  

.. contents::
    :depth: 3

.. _initCommand:

Init Command
------------------------

.. automodule:: pyCGM2f.commands.initiatingCommand
    :members:


.. _editCommand:

Edit Command
------------------------

.. automodule:: pyCGM2f.commands.editingCommand
    :members:


.. _processCommand:

Process command
------------------------

.. automodule:: pyCGM2f.commands.processingCommand
    :members:
