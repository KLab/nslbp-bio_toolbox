.. pyCGM2-flow documentation master file, created by
   sphinx-quickstart on Wed Oct 14 10:03:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation of pyCGM2-flow
=======================================


.. toctree::
   :maxdepth: 1
   :glob:

   fr/mainFr
   commands


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
