import os

# CONSTANTS
MAIN_APPS_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)) + "\\"

#  [Optional] setting folder
SETTINGS_FOLDER = MAIN_APPS_PATH+"pyCGM2f\\Settings\\"


DB_MEDIA_PATH = "C:\\Users\\fleboeuf\\Documents\\Programmation\\pyCGM2\\pyCGM2-extensions\\pyCGM2_flow\\data\\manDB_media\\"

MONGODB_ADRESS = "localhost" #'127.0.0.1'
MONGODB_PORT =  27017
