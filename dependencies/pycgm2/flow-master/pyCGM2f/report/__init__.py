import os

# CONSTANTS
MAIN_APPS_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)) + "\\"

#  [Optional] setting folder
SETTINGS_FOLDER = MAIN_APPS_PATH+"pyCGM2f\\Settings\\"
