Version 0.7.1
=============

- Open Matlab

Set Paths
---------
- Open the 'set Paths' dialog box
- Add the path 'lcwt'
- Add the path 'moveck'

Execute functions
-----------------
- Open the folder 'lcwt'
- Choose the the file demo_cgm11
- Set the required values
- Execute
- Plot the parameters of interests using the content of the variable 'output'
