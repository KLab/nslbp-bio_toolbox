% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : June 2020
% -------------------------------------------------------------------------
% Description  : Compute biomarkers
% -------------------------------------------------------------------------
% Dependencies : None
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function Biomarker = ComputeBiomarkers(Session,Trial,Biomarker,id)

% -------------------------------------------------------------------------
% INIT THE WORKSPACE
% -------------------------------------------------------------------------

% Store current participant and session in the list
if ~isfield(Biomarker,'participant')
    Biomarker.participant = {id};
    iparticipant          = 1;
else
    founder = 0;
    for i = 1:size(Biomarker.participant,2)
        if iscell(Biomarker.participant) % More than one participant listed in the structure
            if contains(Biomarker.participant{i},id)
                iparticipant = i;
                founder = 1;
            end
        else % Only one participant listed in the structure
            if contains(Biomarker.participant,id)
                iparticipant = i;
                founder = 1;
            end
        end
    end
    if founder == 0
        if iscell(Biomarker.participant) % More than one participant listed in the structure
            iparticipant = size(Biomarker.participant,2)+1;
        else % Only one participant listed in the structure
            iparticipant = 2;
        end
        Biomarker.participant{iparticipant} = id;
    end
    clear founder i;
end

% Set biomarker indices            
if contains(Session.type,'INI')
    isession = 1;
elseif contains(Session.type,'REL')
    isession = 2;
elseif contains(Session.type,'FWP')
    isession = 3;
end

% BMo1 /CHECKED
% d410 "Changing basic body position"	Sit to stand	Head	Variability	Head anterior-posterior displacement (std)
%disp('  - BMo1');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp = [];
        if ~isempty(Trial(itrial).Segment(5).T.rcycle)
            for icycle = 1:size(Trial(itrial).Segment(5).T.rcycle,4)
                value1 = permute(Trial(itrial).Segment(5).T.rcycle(1,4,:,icycle),[3,1,2]); % Position /X_ICS of the head centre
                value2 = permute(Trial(itrial).Segment(5).T.rcycle(2,4,:,icycle),[3,1,2]); % Position /Y_ICS of the head centre
                value3 = find(gradient(value1)==max(gradient(value1))); % Start of phase 2 in original article
                test1  = gradient(value2);
                [~,test2] = max(test1);
                value4 = test2+find(test1(test2:end)<max(test1)*0.1); % value4(1) corresponds to the end of phase 3 in original article
                temp   = [temp std(Trial(itrial).Segment(13).T.rcycle(1,4,value3:value4(1),icycle))];
                clear value1 value2 value3 value4 test;
            end
        end
        Biomarker.BMo1(iparticipant,isession,1).label = "Sit to stand | Head anterior-posterior displacement (std)";
        Biomarker.BMo1(iparticipant,isession,1).value = temp;
        Biomarker.BMo1(iparticipant,isession,1).mean  = mean(temp);
        Biomarker.BMo1(iparticipant,isession,1).std   = std(temp);
        Biomarker.BMo1(iparticipant,isession,1).units = 'm';
        clear temp;
    end
end
clear itrial;

% BMo3 /CHECKED
% d410 "Changing basic body position" Sit to stand	Pelvis/leg	Spatial/intensity	Hip sagittal angle (rom)
%disp('  - BMo3');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        % Right side
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(4).Euler.rcycle,4)
            value = permute(Trial(itrial).Joint(4).Euler.rcycle(1,1,:,icycle),[3,1,2]);
            temp  = [temp max(value)-min(value)];
            clear value;
        end
        Biomarker.BMo3(iparticipant,isession,1).label = "Sit to stand | Hip sagittal angle (rom)";
        Biomarker.BMo3(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo3(iparticipant,isession,1).mean  = rad2deg(mean(temp));
        Biomarker.BMo3(iparticipant,isession,1).std   = rad2deg(std(temp));
        Biomarker.BMo3(iparticipant,isession,1).units = '�deg';
        clear temp;
        % Left side
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(9).Euler.rcycle,4)
            value = permute(Trial(itrial).Joint(9).Euler.rcycle(1,1,:,icycle),[3,1,2]);
            temp  = [temp max(value)-min(value)];
            clear value;
        end
        Biomarker.BMo3(iparticipant,isession,2).label = "Sit to stand | Hip sagittal angle (rom)";
        Biomarker.BMo3(iparticipant,isession,2).value = rad2deg(temp);
        Biomarker.BMo3(iparticipant,isession,2).mean  = rad2deg(mean(temp));
        Biomarker.BMo3(iparticipant,isession,2).std   = rad2deg(std(temp));
        Biomarker.BMo3(iparticipant,isession,2).units = '�deg';
        clear temp;
    end
end
clear itrial;

% BMo4 /CHECKED
% d410 "Changing basic body position" Stand to sit	Pelvis/leg	Spatial/intensity	Hip sagittal angle (rom)
%disp('  - BMo4');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        % Right side
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(4).Euler.lcycle,4)
            value = permute(Trial(itrial).Joint(4).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            temp  = [temp max(value)-min(value)];
            clear value;
        end
        Biomarker.BMo4(iparticipant,isession,1).label = "Stand to sit | Hip sagittal angle (rom)";
        Biomarker.BMo4(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo4(iparticipant,isession,1).mean  = rad2deg(mean(temp));
        Biomarker.BMo4(iparticipant,isession,1).std   = rad2deg(std(temp));
        Biomarker.BMo4(iparticipant,isession,1).units = '�deg';
        clear temp;
        % Left side
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(9).Euler.lcycle,4)
            value = permute(Trial(itrial).Joint(9).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            temp  = [temp max(value)-min(value)];
            clear value;
        end
        Biomarker.BMo4(iparticipant,isession,2).label = "Stand to sit | Hip sagittal angle (rom)";
        Biomarker.BMo4(iparticipant,isession,2).value = rad2deg(temp);
        Biomarker.BMo4(iparticipant,isession,2).mean  = rad2deg(mean(temp));
        Biomarker.BMo4(iparticipant,isession,2).std   = rad2deg(std(temp));
        Biomarker.BMo4(iparticipant,isession,2).units = '�deg';
        clear temp;
    end
end   
clear itrial;  

% BMo5 /CHECKED
% d410 "Changing basic body position" Trunk sagittal bending	Lumbar	Spatial/intensity	Lower lumbar sagittal angle (max)
%disp('  - BMo5');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(15).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(permute(Trial(itrial).Segment(15).Euler.rcycle(1,2,:,icycle),[3,1,2])); % Segment kinematics (and not joint kinematics) in the original article of Hidalgo et al. 2012
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(permute(Trial(itrial).Segment(15).Euler.rcycle(1,3,:,icycle),[3,1,2])); % Segment kinematics (and not joint kinematics) in the original article of Hidalgo et al. 2012
                temp  = [temp max(value)];
                clear value;
            end
        end
        Biomarker.BMo5(iparticipant,isession,1).label = "Trunk sagittal bending | Lower lumbar sagittal angle (max)";
        Biomarker.BMo5(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo5(iparticipant,isession,1).mean  = rad2deg(mean(temp));
        Biomarker.BMo5(iparticipant,isession,1).std   = rad2deg(std(temp));
        Biomarker.BMo5(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end       
clear itrial;     

% BMo6 /MODIFIED
% d410 "Changing basic body position" Trunk sagittal bending	Lumbar	Spatial/intensity	Lower lumbar sagittal angular velocity (max)
%disp('  - BMo6');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(15).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(gradient(permute(Trial(itrial).Segment(15).Euler.rcycle(1,2,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(gradient(permute(Trial(itrial).Segment(15).Euler.rcycle(1,3,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp max(value)];
                clear value;
            end
        end
        Biomarker.BMo6(iparticipant,isession,1).label = "Trunk sagittal bending | Lower lumbar sagittal angular velocity (max)";
        Biomarker.BMo6(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo6(iparticipant,isession,1).mean  = rad2deg(mean(temp));
        Biomarker.BMo6(iparticipant,isession,1).std   = rad2deg(std(temp));
        Biomarker.BMo6(iparticipant,isession,1).units = '�deg.s-1';
        clear temp;
    end
end   
clear itrial;  

% BMo7 /CHECKED
% d410 "Changing basic body position"	Sit to stand	Thorax	Spatial/intensity	Lower thorax curvature (max)
%disp('  - BMo7');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp = [];
        for icycle = 1:size(Trial(itrial).Marker(7).Trajectory.rcycle,4)
            vec1  = [];
            vec2  = [];
            vec3  = [];
            % T12-T10 vector
            vec1 = permute(Trial(itrial).Marker(46).Trajectory.rcycle(:,:,:,icycle) - Trial(itrial).Marker(47).Trajectory.rcycle(:,:,:,icycle),[3,1,2]);
            % T10-T8 vector
            vec2 = permute(Trial(itrial).Marker(45).Trajectory.rcycle(:,:,:,icycle) - Trial(itrial).Marker(46).Trajectory.rcycle(:,:,:,icycle),[3,1,2]);
            % T8-T6 vector
            vec3 = permute(Trial(itrial).Marker(44).Trajectory.rcycle(:,:,:,icycle) - Trial(itrial).Marker(45).Trajectory.rcycle(:,:,:,icycle),[3,1,2]);            
            for t = 1:101
                value(t) = atan2(norm(cross(vec1(t,:),vec2(t,:))),dot(vec1(t,:),vec2(t,:)))+...
                           atan2(norm(cross(vec2(t,:),vec3(t,:))),dot(vec2(t,:),vec3(t,:)));
            end         
            temp = [temp max(abs(value))];
            clear value;
        end
        Biomarker.BMo7(iparticipant,isession,1).label = "Sit to stand | Lower thorax curvature (max)";
        Biomarker.BMo7(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo7(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo7(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo7(iparticipant,isession,1).units = '�deg';
        clear vec1 vec2 vec3 value;
    end
end
clear itrial;

% BMo8 /CHECKED
% d410 "Changing basic body position"	Stand to sit	Thorax	Spatial/intensity	Lower thorax curvature (max)
%disp('  - BMo8');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp = [];
        for icycle = 1:size(Trial(itrial).Marker(7).Trajectory.lcycle,4)
            vec1  = [];
            vec2  = [];
            vec3  = [];
            % T12-T10 vector
            vec1 = permute(Trial(itrial).Marker(46).Trajectory.lcycle(:,:,:,icycle) - Trial(itrial).Marker(47).Trajectory.lcycle(:,:,:,icycle),[3,1,2]);
            % T10-T8 vector
            vec2 = permute(Trial(itrial).Marker(45).Trajectory.lcycle(:,:,:,icycle) - Trial(itrial).Marker(46).Trajectory.lcycle(:,:,:,icycle),[3,1,2]);
            % T8-T6 vector
            vec3 = permute(Trial(itrial).Marker(44).Trajectory.lcycle(:,:,:,icycle) - Trial(itrial).Marker(45).Trajectory.lcycle(:,:,:,icycle),[3,1,2]);            
            for t = 1:101
                value(t) = atan2(norm(cross(vec1(t,:),vec2(t,:))),dot(vec1(t,:),vec2(t,:)))+...
                           atan2(norm(cross(vec2(t,:),vec3(t,:))),dot(vec2(t,:),vec3(t,:)));
            end         
            temp = [temp max(abs(value))];
            clear value;
        end
        Biomarker.BMo8(iparticipant,isession,1).label = "Stand to sit | Lower thorax curvature (max)";
        Biomarker.BMo8(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo8(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo8(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo8(iparticipant,isession,1).units = '�deg';
        clear vec1 vec2 vec3 value;
    end
end
clear itrial;

% BMo9 /CHECKED
% d410 "Changing basic body position" Trunk sagittal bending	Thorax	Spatial/intensity	Lower thorax sagittal angle (max)
%disp('  - BMo9');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(17).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(permute(Trial(itrial).Segment(17).Euler.rcycle(1,2,:,icycle),[3,1,2])); % Segment kinematics (and not joint kinematics) in the original article of Hidalgo et al. 2012
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(permute(Trial(itrial).Segment(17).Euler.rcycle(1,3,:,icycle),[3,1,2])); % Segment kinematics (and not joint kinematics) in the original article of Hidalgo et al. 2012
                temp  = [temp max(value)];
                clear value;
            end
        end
        Biomarker.BMo9(iparticipant,isession,1).label = "Trunk sagittal bending | Lower thorax sagittal angle (max)";
        Biomarker.BMo9(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo9(iparticipant,isession,1).mean  = rad2deg(mean(temp));
        Biomarker.BMo9(iparticipant,isession,1).std   = rad2deg(std(temp));
        Biomarker.BMo9(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end 
clear itrial;   

% BMo10 /MODIFIED
% d410 "Changing basic body position" Trunk sagittal bending	Lumbar	Spatial/intensity	Lumbar contribution to thorax angle (rom)
%disp('  - BMo10');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp1 = [];
        temp2 = [];
        for icycle = 1:size(Trial(itrial).Joint(10).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]); % Lumbar kinematics expressed as the difference between thorax and pelvis kinematics in the original article of Laird et al. 2016
                temp1 = [temp1 max(value)-min(value)];
                clear value;
                value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                temp2 = [temp2 max(value)-min(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]); % Lumbar kinematics expressed as the difference between thorax and pelvis kinematics in the original article of Laird et al. 2016
                temp1 = [temp1 max(value)-min(value)];
                clear value;
                value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                temp2 = [temp2 max(value)-min(value)];
                clear value;
            end
        end
        Biomarker.BMo10(iparticipant,isession,1).label = "Trunk sagittal bending | Lumbar contribution to thorax angle (rom)";
        Biomarker.BMo10(iparticipant,isession,1).value = temp1*100./temp2;
        Biomarker.BMo10(iparticipant,isession,1).mean  = mean(temp1*100./temp2);
        Biomarker.BMo10(iparticipant,isession,1).std   = std(temp1*100./temp2);
        Biomarker.BMo10(iparticipant,isession,1).units = '%';
        clear temp1 temp2;
    end
end   
clear itrial;     

% BMo12 /CHECKED
% d410 "Changing basic body position" Trunk sagittal bending	Lumbar	Spatial/intensity	Lumbar sagittal angle (max)
%disp('  - BMo12');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(11).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(permute(Trial(itrial).Segment(11).Euler.rcycle(1,2,:,icycle),[3,1,2])); % Segment kinematics (and not joint kinematics) in the original article of Hidalgo et al. 2012
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(permute(Trial(itrial).Segment(11).Euler.rcycle(1,3,:,icycle),[3,1,2])); % Segment kinematics (and not joint kinematics) in the original article of Hidalgo et al. 2012
                temp  = [temp max(value)];
                clear value;
            end
        end
        Biomarker.BMo12(iparticipant,isession,1).label = "Trunk sagittal bending | Lumbar sagittal angle (max)";
        Biomarker.BMo12(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo12(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo12(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo12(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end  
clear itrial;    

% BMo14 /DELETED (lumbar rotation not allowed with the markerset)
% d410 "Changing basic body position"	Trunk rotation	Lumbar	Spatial/intensity	Lumbar transversal angle (rom)
%disp('  - BMo14');
% for itrial = 1:size(Trial,2)
%     if contains(Trial(itrial).type,'Trunk_Rotation')
%         temp1 = [];
%         temp2 = [];
%         for icycle = 1:size(Trial(itrial).Marker(54).Trajectory.rcycle,4)
%             if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward            
%                 if (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))>0 % Turning right
%                     value = permute(Trial(itrial).Segment(11).Euler.rcycle(1,2,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
%                     temp1 = [temp1 max(value)-min(value)];
%                     clear value;
%                 elseif (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))<0 % Turning left
%                     value = permute(Trial(itrial).Segment(11).Euler.rcycle(1,2,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
%                     temp2 = [temp2 max(value)-min(value)];
%                     clear value;          
%                 end
%             elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward                
%                 if (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))>0 % Turning right
%                     value = permute(Trial(itrial).Segment(11).Euler.rcycle(1,3,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
%                     temp1 = [temp1 max(value)-min(value)];
%                     clear value;
%                 elseif (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))<0 % Turning left
%                     value = permute(Trial(itrial).Segment(11).Euler.rcycle(1,3,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
%                     temp2 = [temp2 max(value)-min(value)];
%                     clear value;            
%                 end   
%             end
%         end
%         Biomarker.BMo14(iparticipant,isession,1).label = "Trunk rotation | Lumbar transversal angle (rom)";
%         Biomarker.BMo14(iparticipant,isession,1).value = rad2deg(temp1);
%         Biomarker.BMo14(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
%         Biomarker.BMo14(iparticipant,isession,1).std   = std(rad2deg(temp1));
%         Biomarker.BMo14(iparticipant,isession,1).units = '�deg';
%         Biomarker.BMo14(iparticipant,isession,2).label = "Trunk rotation | Lumbar transversal angle (rom)";
%         Biomarker.BMo14(iparticipant,isession,2).value = rad2deg(temp2);
%         Biomarker.BMo14(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
%         Biomarker.BMo14(iparticipant,isession,2).std   = std(rad2deg(temp2));
%         Biomarker.BMo14(iparticipant,isession,2).units = '�deg';
%         clear temp1 temp2 vec1 vec2;
%     end
% end 
% clear itrial;

% BMo15 /MODIFIED
% d410 "Changing basic body position"	Trunk sagittal bending	Lumbar	Spatial/intensity	Lumbar sagittal angle (rom)
%disp('  - BMo15');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(11).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = permute(Trial(itrial).Segment(11).Euler.rcycle(1,2,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                temp  = [temp max(value)-min(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = permute(Trial(itrial).Segment(11).Euler.rcycle(1,3,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                temp  = [temp max(value)-min(value)];
                clear value;
            end
        end
        Biomarker.BMo15(iparticipant,isession,1).label = "Trunk sagittal bending | Lumbar sagittal angle (rom)";
        Biomarker.BMo15(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo15(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo15(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo15(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end  
clear itrial;   

% BMo17 /MODIFIED
% d410 "Changing basic body position" Trunk sagittal bending	Lumbar	Spatial/intensity	Lumbar sagittal angular velocity (max)
%disp('  - BMo17');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(11).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(gradient(permute(Trial(itrial).Segment(11).Euler.rcycle(1,2,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(gradient(permute(Trial(itrial).Segment(11).Euler.rcycle(1,3,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp max(value)];
                clear value;
            end
        end
        Biomarker.BMo17(iparticipant,isession,1).label = "Trunk sagittal bending | Lumbar sagittal angular velocity (max)";
        Biomarker.BMo17(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo17(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo17(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo17(iparticipant,isession,1).units = '�deg.s-1';
        clear temp;
    end
end    
clear itrial; 

% BMo18 /MODIFIED
% d410 "Changing basic body position" Trunk sagittal bending	Lumbar	Spatial/intensity	Lumbar sagittal angular velocity (mean)
%disp('  - BMo18');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(11).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(gradient(permute(Trial(itrial).Segment(11).Euler.rcycle(1,2,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp mean(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(gradient(permute(Trial(itrial).Segment(11).Euler.rcycle(1,3,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp mean(value)];
                clear value;
            end
        end
        Biomarker.BMo18(iparticipant,isession,1).label = "Trunk sagittal bending | Lumbar sagittal angular velocity (mean)";
        Biomarker.BMo18(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo18(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo18(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo18(iparticipant,isession,1).units = '�deg.s-1';
        clear temp;
    end
end    
clear itrial; 

% BMo23 /CHECKED
% d410 "Changing basic body position" Sit to stand	Lumbar/leg	Spatial/intensity	Lumbar/hip ratio of sagittal angle (rom)
%disp('  - BMo23');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp1 = [];
        temp2 = [];
        temp3 = [];
        for icycle = 1:size(Trial(itrial).Joint(10).Euler.rcycle,4)
            value = permute(Trial(itrial).Joint(10).Euler.rcycle(1,1,:,icycle),[3,1,2]);
            temp1 = [temp1 max(value)-min(value)];
            clear value;
            value = permute(Trial(itrial).Joint(4).Euler.rcycle(1,1,:,icycle),[3,1,2]);
            temp2 = [temp2 max(value)-min(value)];
            clear value;
            value = permute(Trial(itrial).Joint(9).Euler.rcycle(1,1,:,icycle),[3,1,2]);
            temp3 = [temp3 max(value)-min(value)];
            clear value;
        end
        Biomarker.BMo23(iparticipant,isession,1).label = "Sit to stand | Lumbar/hip ratio of sagittal angle (rom)";
        Biomarker.BMo23(iparticipant,isession,1).value = temp1./temp2;
        Biomarker.BMo23(iparticipant,isession,1).mean  = mean(temp1./temp2);
        Biomarker.BMo23(iparticipant,isession,1).std   = std(temp1./temp2);
        Biomarker.BMo23(iparticipant,isession,1).units = 'ratio';
        Biomarker.BMo23(iparticipant,isession,2).label = "Sit to stand | Lumbar/hip ratio of sagittal angle (rom)";
        Biomarker.BMo23(iparticipant,isession,2).value = temp1./temp3;
        Biomarker.BMo23(iparticipant,isession,2).mean  = mean(temp1./temp3);
        Biomarker.BMo23(iparticipant,isession,2).std   = std(temp1./temp3);
        Biomarker.BMo23(iparticipant,isession,2).units = 'ratio';
        clear temp1 temp2 temp3;
    end
end 
clear itrial;

% BMo24 /CHECKED
% d410 "Changing basic body position" Stand to sit	Lumbar/leg	Spatial/intensity	Lumbar/hip ratio of sagittal angle (rom)
%disp('  - BMo24');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp1 = [];
        temp2 = [];
        temp3 = [];
        for icycle = 1:size(Trial(itrial).Joint(10).Euler.rcycle,4)
            value = permute(Trial(itrial).Joint(10).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            temp1 = [temp1 max(value)-min(value)];
            clear value;
            value = permute(Trial(itrial).Joint(4).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            temp2 = [temp2 max(value)-min(value)];
            clear value;
            value = permute(Trial(itrial).Joint(9).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            temp3 = [temp3 max(value)-min(value)];
            clear value;
        end
        Biomarker.BMo24(iparticipant,isession,1).label = "Stand to sit | Lumbar/hip ratio of sagittal angle (rom)";
        Biomarker.BMo24(iparticipant,isession,1).value = temp1./temp2;
        Biomarker.BMo24(iparticipant,isession,1).mean  = mean(temp1./temp2);
        Biomarker.BMo24(iparticipant,isession,1).std   = std(temp1./temp2);
        Biomarker.BMo24(iparticipant,isession,1).units = 'ratio';
        Biomarker.BMo24(iparticipant,isession,2).label = "Stand to sit | Lumbar/hip ratio of sagittal angle (rom)";
        Biomarker.BMo24(iparticipant,isession,2).value = temp1./temp3;
        Biomarker.BMo24(iparticipant,isession,2).mean  = mean(temp1./temp3);
        Biomarker.BMo24(iparticipant,isession,2).std   = std(temp1./temp3);
        Biomarker.BMo24(iparticipant,isession,2).units = 'ratio';
        clear temp1 temp2 temp3;
    end
end   
clear itrial;         

% BMo25 /MODIFIED
% d410 "Changing basic body position" Sit to stand	Lumbar/leg	Coordination	Lumbar/hip relative phase difference (max)
%disp('  - BMo25');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        phi1 = [];
        phi2 = [];
        phi3 = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            phi1 = [phi1 atan(gradient(permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,1,2]))];
            phi2 = [phi2 atan(gradient(permute(Trial(itrial).Joint(4).Euler.rcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(4).Euler.rcycle(1,1,:,icycle),[3,1,2]))];
            phi3 = [phi3 atan(gradient(permute(Trial(itrial).Joint(9).Euler.rcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(9).Euler.rcycle(1,1,:,icycle),[3,1,2]))];
        end
        relativePhase1 = phi2-phi1;
        relativePhase2 = phi3-phi1;
        Biomarker.BMo25(iparticipant,isession,1).label = "Sit to stand | Lumbar/hip relative phase difference (max)";
        Biomarker.BMo25(iparticipant,isession,1).value = rad2deg(max(relativePhase1));
        Biomarker.BMo25(iparticipant,isession,1).mean  = mean(rad2deg(max(relativePhase1)));
        Biomarker.BMo25(iparticipant,isession,1).std   = std(rad2deg(max(relativePhase1)));
        Biomarker.BMo25(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo25(iparticipant,isession,2).label = "Sit to stand | Lumbar/hip relative phase difference (max)";
        Biomarker.BMo25(iparticipant,isession,2).value = rad2deg(max(relativePhase2));
        Biomarker.BMo25(iparticipant,isession,2).mean  = mean(rad2deg(max(relativePhase2)));
        Biomarker.BMo25(iparticipant,isession,2).std   = std(rad2deg(max(relativePhase2)));
        Biomarker.BMo25(iparticipant,isession,2).units = '�deg';
        clear phi1 phi2 phi3 relativePhase1 relativePhase2;        
    end
end 
clear itrial;    

% BMo26 /MODIFIED
% d410 "Changing basic body position" Sit to stand	Lumbar/leg	Coordination	Lumbar/hip relative phase difference (mean)
%disp('  - BMo26');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        phi1 = [];
        phi2 = [];
        phi3 = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            phi1 = [phi1 atan(gradient(permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,1,2]))];
            phi2 = [phi2 atan(gradient(permute(Trial(itrial).Joint(4).Euler.rcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(4).Euler.rcycle(1,1,:,icycle),[3,1,2]))];
            phi3 = [phi3 atan(gradient(permute(Trial(itrial).Joint(9).Euler.rcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(9).Euler.rcycle(1,1,:,icycle),[3,1,2]))];
        end
        relativePhase1 = phi2-phi1;
        relativePhase2 = phi3-phi1;
        Biomarker.BMo26(iparticipant,isession,1).label = "Sit to stand | Lumbar/hip relative phase difference (mean)";
        Biomarker.BMo26(iparticipant,isession,1).value = rad2deg(mean(relativePhase1));
        Biomarker.BMo26(iparticipant,isession,1).mean  = mean(rad2deg(mean(relativePhase1)));
        Biomarker.BMo26(iparticipant,isession,1).std   = std(rad2deg(mean(relativePhase1)));
        Biomarker.BMo26(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo26(iparticipant,isession,2).label = "Sit to stand | Lumbar/hip relative phase difference (mean)";
        Biomarker.BMo26(iparticipant,isession,2).value = rad2deg(mean(relativePhase2));
        Biomarker.BMo26(iparticipant,isession,2).mean  = mean(rad2deg(mean(relativePhase2)));
        Biomarker.BMo26(iparticipant,isession,2).std   = std(rad2deg(mean(relativePhase2)));
        Biomarker.BMo26(iparticipant,isession,2).units = '�deg';
        clear phi1 phi2 phi3 relativePhase1 relativePhase2;        
    end
end    
clear itrial;    

% BMo27 /MODIFIED
% d410 "Changing basic body position" Stand to sit	Lumbar/leg	Coordination	Lumbar/hip relative phase difference (mean)
%disp('  - BMo27');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        phi1 = [];
        phi2 = [];
        phi3 = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            phi1 = [phi1 atan(gradient(permute(Trial(itrial).Joint(5).Euler.lcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(5).Euler.lcycle(1,1,:,icycle),[3,1,2]))];
            phi2 = [phi2 atan(gradient(permute(Trial(itrial).Joint(4).Euler.lcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(4).Euler.lcycle(1,1,:,icycle),[3,1,2]))];
            phi3 = [phi3 atan(gradient(permute(Trial(itrial).Joint(9).Euler.lcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(9).Euler.lcycle(1,1,:,icycle),[3,1,2]))];
        end
        relativePhase1 = phi2-phi1;
        relativePhase2 = phi3-phi1;
        Biomarker.BMo27(iparticipant,isession,1).label = "Stand to sit | Lumbar/hip relative phase difference (mean)";
        Biomarker.BMo27(iparticipant,isession,1).value = rad2deg(mean(relativePhase1));
        Biomarker.BMo27(iparticipant,isession,1).mean  = mean(rad2deg(mean(relativePhase1)));
        Biomarker.BMo27(iparticipant,isession,1).std   = std(rad2deg(mean(relativePhase1)));
        Biomarker.BMo27(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo27(iparticipant,isession,2).label = "Stand to sit | Lumbar/hip relative phase difference (mean)";
        Biomarker.BMo27(iparticipant,isession,2).value = rad2deg(mean(relativePhase2));
        Biomarker.BMo27(iparticipant,isession,2).mean  = mean(rad2deg(mean(relativePhase2)));
        Biomarker.BMo27(iparticipant,isession,2).std   = std(rad2deg(mean(relativePhase2)));
        Biomarker.BMo27(iparticipant,isession,2).units = '�deg';
        clear phi1 phi2 phi3 relativePhase1 relativePhase2;        
    end
end    
clear itrial;    

% BMo28 /MODIFIED
% d410 "Changing basic body position" Sit to stand	Lumbar/leg	Coordination	Lumbar/hip relative phase difference (min)
%disp('  - BMo28');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        phi1 = [];
        phi2 = [];
        phi3 = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            phi1 = [phi1 atan(gradient(permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,1,2]))];
            phi2 = [phi2 atan(gradient(permute(Trial(itrial).Joint(4).Euler.rcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(4).Euler.rcycle(1,1,:,icycle),[3,1,2]))];
            phi3 = [phi3 atan(gradient(permute(Trial(itrial).Joint(9).Euler.rcycle(1,1,:,icycle),[3,2,1]))./permute(Trial(itrial).Joint(9).Euler.rcycle(1,1,:,icycle),[3,1,2]))];
        end
        relativePhase1 = phi2-phi1;
        relativePhase2 = phi3-phi1;
        Biomarker.BMo28(iparticipant,isession,1).label = "Sit to stand | Lumbar/hip relative phase difference (min)";
        Biomarker.BMo28(iparticipant,isession,1).value = rad2deg(min(relativePhase1));
        Biomarker.BMo28(iparticipant,isession,1).mean  = mean(rad2deg(min(relativePhase1)));
        Biomarker.BMo28(iparticipant,isession,1).std   = std(rad2deg(min(relativePhase1)));
        Biomarker.BMo28(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo28(iparticipant,isession,2).label = "Sit to stand | Lumbar/hip relative phase difference (min)";
        Biomarker.BMo28(iparticipant,isession,2).value = rad2deg(min(relativePhase2));
        Biomarker.BMo28(iparticipant,isession,2).mean  = mean(rad2deg(min(relativePhase2)));
        Biomarker.BMo28(iparticipant,isession,2).std   = std(rad2deg(min(relativePhase2)));
        Biomarker.BMo28(iparticipant,isession,2).units = '�deg';
        clear phi1 phi2 phi3 relativePhase1 relativePhase2;        
    end
end    
clear itrial;

% BMo29 /CHECKED
% d410 "Changing basic body position" Trunk sagittal bending	Lumbar/pelvis	Coordination	Lumbar/pelvis absolute relative phase (mean)
%disp('  - BMo29');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        phi1 = [];
        phi2 = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                ang1  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,2,1]);
                nang1 = 2*(ang1-min(ang1))-(max(ang1)-min(ang1))-1;
                vel1  = gradient(ang1);
                nvel1 = vel1/max(vel1);
                phi1  = [phi1 atan(nvel1./nang1)];
                ang2  = permute(Trial(itrial).Segment(11).Euler.rcycle(1,2,:,icycle),[3,2,1]);
                nang2 = 2*(ang2-min(ang2))-(max(ang2)-min(ang2))-1;
                vel2  = gradient(ang2);
                nvel2 = vel2/max(vel2);
                phi2  = [phi2 atan(nvel2./nang2)];
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                ang1  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,2,1]);
                nang1 = 2*(ang1-min(ang1))-(max(ang1)-min(ang1))-1;
                vel1  = gradient(ang1);
                nvel1 = vel1/max(vel1);
                phi1  = [phi1 atan(nvel1./nang1)];
                ang2  = permute(Trial(itrial).Segment(11).Euler.rcycle(1,3,:,icycle),[3,2,1]);
                nang2 = 2*(ang2-min(ang2))-(max(ang2)-min(ang2))-1;
                vel2  = gradient(ang2);
                nvel2 = vel2/max(vel2);
                phi2  = [phi2 atan(nvel2./nang2)];
            end
        end
        relativePhase = phi1-phi2; % mean(phi1-phi2,2) Mean across cycles in the original article ('mean ensemble curve')
        MARP          = sum(abs(relativePhase))/101;
        Biomarker.BMo29(iparticipant,isession,1).label = "Trunk sagittal bending | Lumbar/pelvis absolute relative phase (mean)";
        Biomarker.BMo29(iparticipant,isession,1).value = rad2deg(MARP);
        Biomarker.BMo29(iparticipant,isession,1).mean  = mean(rad2deg(MARP));
        Biomarker.BMo29(iparticipant,isession,1).std   = std(rad2deg(MARP));
        Biomarker.BMo29(iparticipant,isession,1).units = '�deg';
        clear ang1 nang1 vel1 nvel1 ang2 nang2 vel2 nvel2 phi1 phi2 relativePhase MARP;  
    end
end   
clear itrial;   

% BMo30 /MODIFIED
% d410 "Changing basic body position" Trunk sagittal bending	Lumbar/pelvis	Coordination	Lumbar/pelvis deviation phase (mean)
%disp('  - BMo30');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        phi1 = [];
        phi2 = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                ang1  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,2,1]);
                nang1 = 2*(ang1-min(ang1))-(max(ang1)-min(ang1))-1;
                vel1  = gradient(ang1);
                nvel1 = vel1/max(vel1);
                phi1  = [phi1 atan(nvel1./nang1)];
                ang2  = permute(Trial(itrial).Segment(11).Euler.rcycle(1,2,:,icycle),[3,2,1]);
                nang2 = 2*(ang2-min(ang2))-(max(ang2)-min(ang2))-1;
                vel2  = gradient(ang2);
                nvel2 = vel2/max(vel2);
                phi2  = [phi2 atan(nvel2./nang2)];
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                ang1  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,2,1]);
                nang1 = 2*(ang1-min(ang1))-(max(ang1)-min(ang1))-1;
                vel1  = gradient(ang1);
                nvel1 = vel1/max(vel1);
                phi1  = [phi1 atan(nvel1./nang1)];
                ang2  = permute(Trial(itrial).Segment(11).Euler.rcycle(1,3,:,icycle),[3,2,1]);
                nang2 = 2*(ang2-min(ang2))-(max(ang2)-min(ang2))-1;
                vel2  = gradient(ang2);
                nvel2 = vel2/max(vel2);
                phi2  = [phi2 atan(nvel2./nang2)];
            end
        end
        for t = 1:size(phi1,1)
            temp(t) = std([phi1(t,1) phi2(t,1)]);
        end
        DP(1) = sum(temp)/size(phi1,1);
        clear temp;
        for t = 1:size(phi1,1)
            temp(t) = std([phi1(t,2) phi2(t,2)]);
        end
        DP(2) = sum(temp)/size(phi1,1);
        clear temp;
        for t = 1:size(phi1,1)
            temp(t) = std([phi1(t,3) phi2(t,3)]);
        end
        DP(3) = sum(temp)/size(phi1,1);
        clear temp;
        Biomarker.BMo30(iparticipant,isession,1).label = "Trunk sagittal bending | Lumbar/pelvis deviation phase (mean)"; 
        Biomarker.BMo30(iparticipant,isession,1).value = rad2deg(DP);
        Biomarker.BMo30(iparticipant,isession,1).mean  = mean(rad2deg(DP));
        Biomarker.BMo30(iparticipant,isession,1).std   = std(rad2deg(DP));
        Biomarker.BMo30(iparticipant,isession,1).units = '�deg';
        clear ang1 nang1 vel1 nvel1 ang2 nang2 vel2 nvel2 phi1 phi2 relativePhase DP;  
    end
end  
clear itrial;       

% BMo31 /CHECKED
% d410 "Changing basic body position"	Sit to stand	Lumbar/pelvis	Spatial/intensity	Lumbopelvic sagittal angle (max)
%disp('  - BMo31');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            value = permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,1,2]);
            temp  = [temp max(value)];
            clear value;
        end
        Biomarker.BMo31(iparticipant,isession,1).label = "Sit to stand | Lumbopelvic sagittal angle (max)"; 
        Biomarker.BMo31(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo31(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo31(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo31(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end 
clear itrial;  

% BMo33 /CHECKED
% d410 "Changing basic body position" Sit to stand	Lumbar/pelvis	Spatial/intensity	Lumbopelvic sagittal angle (rom)
%disp('  - BMo33');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(10).Euler.rcycle,4)
            value = permute(Trial(itrial).Joint(10).Euler.rcycle(1,1,:,icycle),[3,1,2]);
            temp  = [temp max(value)-min(value)];
            clear value;
        end
        Biomarker.BMo33(iparticipant,isession,1).label = "Sit to stand | Lumbopelvic sagittal angle (rom)";
        Biomarker.BMo33(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo33(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo33(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo33(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end 
clear itrial;  

% BMo34 /CHECKED
% d410 "Changing basic body position" Stand to sit	Lumbar/pelvis	Spatial/intensity	Lumbopelvic sagittal angle (rom)
%disp('  - BMo34');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(10).Euler.lcycle,4)
            value = permute(Trial(itrial).Joint(10).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            temp  = [temp max(value)-min(value)];
            clear value;
        end
        Biomarker.BMo34(iparticipant,isession,1).label = "Stand to sit | Lumbopelvic sagittal angle (rom)";
        Biomarker.BMo34(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo34(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo34(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo34(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end    
clear itrial;   

% BMo35 /MODIFIED
% d410 "Changing basic body position"	Sit to stand	Lumbar/pelvis	Spatial/intensity	Lumbopelvic sagittal angular velocity (max)
%disp('  - BMo35');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            value = abs(gradient(permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
            temp  = [temp max(value)];
            clear value;
        end
        Biomarker.BMo35(iparticipant,isession,1).label = "Sit to stand | Lumbopelvic sagittal angular velocity (max)";
        Biomarker.BMo35(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo35(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo35(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo35(iparticipant,isession,1).units = '�deg.s-1';
        clear temp;
    end
end 
clear itrial;  

% BMo37 /CHECKED
% d410 "Changing basic body position" Trunk sagittal bending	Pelvis	Spatial/intensity	Pelvis sagittal angle (rom)
%disp('  - BMo37');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(10).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                temp  = [temp max(value)-min(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                temp  = [temp max(value)-min(value)];
                clear value;
            end
        end
        Biomarker.BMo37(iparticipant,isession,1).label = "Trunk sagittal bending | Pelvis sagittal angle (rom)";
        Biomarker.BMo37(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo37(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo37(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo37(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end  
clear itrial; 

% BMo38 /CHECKED
% d410 "Changing basic body position"	Trunk rotation	Pelvis	Spatial/intensity	Pelvis sagittal angle (rom)
%disp('  - BMo38');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Rotation')
        temp1 = [];
        temp2 = [];
        for icycle = 1:size(Trial(itrial).Marker(54).Trajectory.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward            
                if (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))>0 % Turning right
                    value = permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))<0 % Turning left
                    value = permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;          
                end
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward                
                if (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))>0 % Turning right
                    value = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))<0 % Turning left
                     value = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;            
                end   
            end
        end
        Biomarker.BMo38(iparticipant,isession,1).label = "Trunk rotation | Pelvis sagittal angle (rom)";
        Biomarker.BMo38(iparticipant,isession,1).value = rad2deg(temp1);
        Biomarker.BMo38(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
        Biomarker.BMo38(iparticipant,isession,1).std   = std(rad2deg(temp1));
        Biomarker.BMo38(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo38(iparticipant,isession,2).label = "Trunk rotation | Pelvis sagittal angle (rom)";
        Biomarker.BMo38(iparticipant,isession,2).value = rad2deg(temp2);
        Biomarker.BMo38(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
        Biomarker.BMo38(iparticipant,isession,2).std   = std(rad2deg(temp2));
        Biomarker.BMo38(iparticipant,isession,2).units = '�deg';
        clear temp1 temp2 vec1 vec2;
    end
end 
clear itrial;

% BMo41 /MODIFIED
% d410 "Changing basic body position"	Trunk sagittal bending	Pelvis	Spatial/intensity	Pelvis sagittal angular velocity (mean)
%disp('  - BMo41');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(5).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(gradient(permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp mean(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(gradient(permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp mean(value)];
                clear value;
            end
        end
        Biomarker.BMo41(iparticipant,isession,1).label = "Trunk sagittal bending | Pelvis sagittal angular velocity (mean)";
        Biomarker.BMo41(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo41(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo41(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo41(iparticipant,isession,1).units = '�deg.s-1';
        clear temp;
    end
end    
clear itrial; 

% BMo42 /MODIFIED
% d410 "Changing basic body position" Trunk sagittal bending	Lumbar/pelvis	Coordination	Pelvis/thigh deviation phase (mean)
%disp('  - BMo42');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        phi1 = [];
        phi2 = [];
        phi3 = [];
        for icycle = 1:size(Trial(itrial).Joint(10).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                ang1  = permute(Trial(itrial).Segment(4).Euler.rcycle(1,2,:,icycle),[3,2,1]);
                nang1 = 2*(ang1-min(ang1))-(max(ang1)-min(ang1))-1;
                vel1  = gradient(ang1);
                nvel1 = vel1/max(vel1);
                phi1  = [phi1 atan(nvel1./nang1)];
                ang2  = permute(Trial(itrial).Segment(9).Euler.rcycle(1,2,:,icycle),[3,2,1]);
                nang2 = 2*(ang2-min(ang2))-(max(ang2)-min(ang2))-1;
                vel2  = gradient(ang2);
                nvel2 = vel1/max(vel2);
                phi2  = [phi2 atan(nvel2./nang2)];
                ang3  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,2,1]);
                nang3 = 2*(ang2-min(ang3))-(max(ang3)-min(ang3))-1;
                vel3  = gradient(ang3);
                nvel3 = vel2/max(vel3);
                phi3  = [phi3 atan(nvel3./nang3)];
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                ang1  = permute(Trial(itrial).Segment(4).Euler.rcycle(1,3,:,icycle),[3,2,1]);
                nang1 = 2*(ang1-min(ang1))-(max(ang1)-min(ang1))-1;
                vel1  = gradient(ang1);
                nvel1 = vel1/max(vel1);
                phi1  = [phi1 atan(nvel1./nang1)];
                ang2  = permute(Trial(itrial).Segment(9).Euler.rcycle(1,3,:,icycle),[3,2,1]);
                nang2 = 2*(ang2-min(ang2))-(max(ang2)-min(ang2))-1;
                vel2  = gradient(ang2);
                nvel2 = vel1/max(vel2);
                phi2  = [phi2 atan(nvel2./nang2)];
                ang3  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,2,1]);
                nang3 = 2*(ang2-min(ang3))-(max(ang3)-min(ang3))-1;
                vel3  = gradient(ang3);
                nvel3 = vel2/max(vel3);
                phi3  = [phi3 atan(nvel3./nang3)];
                
            end
        end       
        for t = 1:size(phi1,1)
            temp(t) = std([phi1(t,1) phi3(t,1)]);
        end
        DP1(1) = sum(temp)/size(phi1,1);
        clear temp;
        for t = 1:size(phi1,1)
            temp(t) = std([phi1(t,2) phi3(t,2)]);
        end
        DP1(2) = sum(temp)/size(phi1,1);
        clear temp;
        for t = 1:size(phi1,1)
            temp(t) = std([phi1(t,3) phi3(t,3)]);
        end
        DP1(3) = sum(temp)/size(phi1,1);
        clear temp;
        for t = 1:size(phi1,1)
            temp(t) = std([phi2(t,1) phi3(t,1)]);
        end
        DP2(1) = sum(temp)/size(phi1,1);
        clear temp;
        for t = 1:size(phi1,1)
            temp(t) = std([phi2(t,2) phi3(t,2)]);
        end
        DP2(2) = sum(temp)/size(phi1,1);
        clear temp;
        for t = 1:size(phi1,1)
            temp(t) = std([phi2(t,3) phi3(t,3)]);
        end
        DP2(3) = sum(temp)/size(phi1,1);
        clear temp;       
        Biomarker.BMo42(iparticipant,isession,1).label = "Trunk sagittal bending | Pelvis/thigh deviation phase (mean)";
        Biomarker.BMo42(iparticipant,isession,1).value = rad2deg(DP1);
        Biomarker.BMo42(iparticipant,isession,1).mean  = mean(rad2deg(DP1));
        Biomarker.BMo42(iparticipant,isession,1).std   = std(rad2deg(DP1));
        Biomarker.BMo42(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo42(iparticipant,isession,2).label = "Trunk sagittal bending | Pelvis/thigh deviation phase (mean)";
        Biomarker.BMo42(iparticipant,isession,2).value = rad2deg(DP2);
        Biomarker.BMo42(iparticipant,isession,2).mean  = mean(rad2deg(DP2));
        Biomarker.BMo42(iparticipant,isession,2).std   = std(rad2deg(DP2));
        Biomarker.BMo42(iparticipant,isession,2).units = '�deg';
        clear ang1 nang1 vel1 nvel1 phi1 ang2 nang2 vel2 nvel2 phi2 ang3 nang3 vel3 nvel3 phi3 relativePhase1 relativePhase2 DP1 DP2;  
    end
end      
clear itrial;       

% BMo43 /CHECKED
% d410 "Changing basic body position" Trunk rotation	Thorax	Spatial/intensity	Scapular belt transversal angle (max)
%disp('  - BMo43');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Rotation')
        temp1 = [];
        temp2 = [];
        for icycle = 1:size(Trial(itrial).Marker(54).Trajectory.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward            
                if (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))>0 % Turning right
                    vec1  = [];
                    vec2  = [];
                    vec1  = permute(Trial(itrial).Marker(54).Trajectory.rcycle([1,3],:,:,icycle) - Trial(itrial).Marker(59).Trajectory.rcycle([1,3],:,:,icycle),[3,1,2]);
                    vec1  = [vec1 zeros(101,1)];
                    vec2  = permute(Trial(itrial).Marker(1).Trajectory.rcycle([1,3],:,:,icycle) - Trial(itrial).Marker(6).Trajectory.rcycle([1,3],:,:,icycle),[3,1,2]);
                    vec2  = [vec2 zeros(101,1)];
                    for t = 1:101
                        value(t) = atan2(norm(cross(vec1(t,:),vec2(t,:))),dot(vec1(t,:),vec2(t,:)));
                    end
                    temp1 = [temp1 max(abs(value))];
                    clear value;
                elseif (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))<0 % Turning left
                    vec1  = [];
                    vec2  = [];
                    vec1  = permute(Trial(itrial).Marker(54).Trajectory.rcycle([1,2],:,:,icycle) - Trial(itrial).Marker(59).Trajectory.rcycle([1,2],:,:,icycle),[3,1,2]);
                    vec1  = [vec1 zeros(101,1)];
                    vec2  = permute(Trial(itrial).Marker(1).Trajectory.rcycle([1,2],:,:,icycle) - Trial(itrial).Marker(6).Trajectory.rcycle([1,2],:,:,icycle),[3,1,2]);
                    vec2  = [vec2 zeros(101,1)];
                    for t = 1:101
                        value(t) = atan2(norm(cross(vec1(t,:),vec2(t,:))),dot(vec1(t,:),vec2(t,:)));
                    end
                    temp2 = [temp2 max(abs(value))];  
                    clear value;           
                end
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward                
                if (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))>0 % Turning right
                    vec1  = [];
                    vec2  = [];
                    vec1  = permute(Trial(itrial).Marker(54).Trajectory.rcycle([1,3],:,:,icycle) - Trial(itrial).Marker(59).Trajectory.rcycle([1,3],:,:,icycle),[3,1,2]);
                    vec1  = [vec1 zeros(101,1)];
                    vec2  = permute(Trial(itrial).Marker(1).Trajectory.rcycle([1,3],:,:,icycle) - Trial(itrial).Marker(6).Trajectory.rcycle([1,3],:,:,icycle),[3,1,2]);
                    vec2  = [vec2 zeros(101,1)];
                    for t = 1:101
                        value(t)  = atan2(norm(cross(vec1(t,:),vec2(t,:))),dot(vec1(t,:),vec2(t,:)));
                    end
                    temp1 = [temp1 max(abs(value))];
                    clear value;
                elseif (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))<0 % Turning left
                    vec1  = [];
                    vec2  = [];
                    vec1  = permute(Trial(itrial).Marker(54).Trajectory.rcycle([1,3],:,:,icycle) - Trial(itrial).Marker(59).Trajectory.rcycle([1,3],:,:,icycle),[3,1,2]);
                    vec1  = [vec1 zeros(101,1)];
                    vec2  = permute(Trial(itrial).Marker(1).Trajectory.rcycle([1,3],:,:,icycle) - Trial(itrial).Marker(6).Trajectory.rcycle([1,3],:,:,icycle),[3,1,2]);
                    vec2  = [vec2 zeros(101,1)];
                    for t = 1:101
                        value(t)  = atan2(norm(cross(vec1(t,:),vec2(t,:))),dot(vec1(t,:),vec2(t,:)));
                    end
                    temp2 = [temp2 max(abs(value))];
                    clear value;             
                end   
            end
        end
        Biomarker.BMo43(iparticipant,isession,1).label = "Trunk rotation | Scapular belt transversal angle (max)";
        Biomarker.BMo43(iparticipant,isession,1).value = rad2deg(temp1);
        Biomarker.BMo43(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
        Biomarker.BMo43(iparticipant,isession,1).std   = std(rad2deg(temp1));
        Biomarker.BMo43(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo43(iparticipant,isession,2).label = "Trunk rotation | Scapular belt transversal angle (max)";
        Biomarker.BMo43(iparticipant,isession,2).value = rad2deg(temp2);
        Biomarker.BMo43(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
        Biomarker.BMo43(iparticipant,isession,2).std   = std(rad2deg(temp2));
        Biomarker.BMo43(iparticipant,isession,2).units = '�deg';
        clear temp1 temp2 vec1 vec2;
    end
end 
clear itrial;    

% BMo44 /MODIFIED
% d410 "Changing basic body position" Trunk sagittal bending	Thorax/pelvis	Spatial/intensity	Thoracopelvic sagittal angle (max)
%disp('  - BMo44');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2]) - ...  % Thorax segment
                            permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]));      % Pelvis segment
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward  
                value = abs(permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2]) - ...  % Thorax segment
                            permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]));      % Pelvis segment
                temp  = [temp max(value)];
                clear value;              
            end
        end
        Biomarker.BMo44(iparticipant,isession,1).label = "Trunk sagittal bending | Thoracopelvic sagittal angle (max)";
        Biomarker.BMo44(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo44(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo44(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo44(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end  
clear itrial;    

% BMo45 /MODIFIED
% d410 "Changing basic body position"	Trunk rotation	Thorax/pelvis	Spatial/intensity	Thoracopelvic sagittal angle (rom)
%disp('  - BMo45');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Rotation')
        temp1 = [];
        temp2 = [];
        for icycle = 1:size(Trial(itrial).Marker(54).Trajectory.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward            
                if (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))>0 % Turning right
                    value = (permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,1,2])+... % Lumbo-pelvis joint
                             permute(Trial(itrial).Joint(11).Euler.rcycle(1,1,:,icycle),[3,1,2]));  % Thoraco-lumbar joint
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))<0 % Turning left
                    value = (permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,1,2])+... % Lumbo-pelvis joint
                             permute(Trial(itrial).Joint(11).Euler.rcycle(1,1,:,icycle),[3,1,2]));  % Thoraco-lumbar joint
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;          
                end
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward                
                if (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))>0 % Turning right
                    value = (permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,1,2])+... % Lumbo-pelvis joint
                             permute(Trial(itrial).Joint(11).Euler.rcycle(1,1,:,icycle),[3,1,2]));  % Thoraco-lumbar joint
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))<0 % Turning left
                    value = (permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,1,2])+... % Lumbo-pelvis joint
                             permute(Trial(itrial).Joint(11).Euler.rcycle(1,1,:,icycle),[3,1,2]));  % Thoraco-lumbar joint
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;            
                end   
            end
        end
        Biomarker.BMo45(iparticipant,isession,1).label = "Trunk rotation | Thoracopelvic sagittal angle (rom)";
        Biomarker.BMo45(iparticipant,isession,1).value = rad2deg(temp1);
        Biomarker.BMo45(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
        Biomarker.BMo45(iparticipant,isession,1).std   = std(rad2deg(temp1));
        Biomarker.BMo45(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo45(iparticipant,isession,2).label = "Trunk rotation | Thoracopelvic sagittal angle (rom)";
        Biomarker.BMo45(iparticipant,isession,2).value = rad2deg(temp2);
        Biomarker.BMo45(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
        Biomarker.BMo45(iparticipant,isession,2).std   = std(rad2deg(temp2));
        Biomarker.BMo45(iparticipant,isession,2).units = '�deg';
        clear temp1 temp2 vec1 vec2;
    end
end 
clear itrial;

% BMo46 /MODIFIED
% d410 "Changing basic body position"	Trunk rotation	Thorax	Spatial/intensity	Thorax frontal angle (rom)
%disp('  - BMo46');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Rotation')
        temp1 = [];
        temp2 = [];
        for icycle = 1:size(Trial(itrial).Marker(54).Trajectory.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward            
                if (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))>0 % Turning right
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))<0 % Turning left
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;          
                end
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward                
                if (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))>0 % Turning right
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))<0 % Turning left
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2]) - permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;            
                end   
            end
        end
        Biomarker.BMo46(iparticipant,isession,1).label = "Trunk rotation | Thorax frontal angle (rom)";
        Biomarker.BMo46(iparticipant,isession,1).value = rad2deg(temp1);
        Biomarker.BMo46(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
        Biomarker.BMo46(iparticipant,isession,1).std   = std(rad2deg(temp1));
        Biomarker.BMo46(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo46(iparticipant,isession,2).label = "Trunk rotation | Thorax frontal angle (rom)";
        Biomarker.BMo46(iparticipant,isession,2).value = rad2deg(temp2);
        Biomarker.BMo46(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
        Biomarker.BMo46(iparticipant,isession,2).std   = std(rad2deg(temp2));
        Biomarker.BMo46(iparticipant,isession,2).units = '�deg';
        clear temp1 temp2 vec1 vec2;
    end
end 
clear itrial;

% BMo47 /CHECKED
% d410 "Changing basic body position"	Trunk sagittal bending	Thorax	Spatial/intensity	Thorax frontal angle (rom)
%disp('  - BMo47');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp1 = [];
        temp2 = [];
        for icycle = 1:size(Trial(itrial).Marker(54).Trajectory.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward            
                if (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))>0 % Turning right
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                    if rad2deg(max(value)-min(value)) > 150
                        temp1 = [temp1 180-(max(value)-min(value))];
                    else
                        temp1 = [temp1 max(value)-min(value)];
                    end
                    clear value;
                elseif (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))<0 % Turning left
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                    if rad2deg(max(value)-min(value)) > 150
                        temp1 = [temp1 180-(max(value)-min(value))];
                    else
                        temp1 = [temp1 max(value)-min(value)];
                    end
                    clear value;          
                end
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward                
                if (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))>0 % Turning right
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                    if rad2deg(max(value)-min(value)) > 150
                        temp1 = [temp1 180-(max(value)-min(value))];
                    else
                        temp1 = [temp1 max(value)-min(value)];
                    end
                    clear value;
                elseif (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))<0 % Turning left
                     value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                    if rad2deg(max(value)-min(value)) > 150
                        temp1 = [temp1 180-(max(value)-min(value))];
                    else
                        temp1 = [temp1 max(value)-min(value)];
                    end
                    clear value;            
                end   
            end
        end
        Biomarker.BMo47(iparticipant,isession,1).label = "Trunk sagittal bending | Thorax frontal angle (rom)";
        Biomarker.BMo47(iparticipant,isession,1).value = rad2deg(temp1);
        Biomarker.BMo47(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
        Biomarker.BMo47(iparticipant,isession,1).std   = std(rad2deg(temp1));
        Biomarker.BMo47(iparticipant,isession,1).units = '�deg';
        clear temp1 temp2 vec1 vec2;
    end
end 
clear itrial;

% BMo48 /MODIFIED
% d410 "Changing basic body position"	Trunk lateral bending	Thorax	Spatial/intensity	Thorax frontal angular velocity (max)
%disp('  - BMo48');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Lateral')
        temp1 = [];
        temp2 = [];
        for icycle = 1:size(Trial(itrial).Marker(54).Trajectory.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward            
                if abs((Trial(itrial).Marker(54).Trajectory.rcycle(2,:,1,icycle)-(Trial(itrial).Marker(54).Trajectory.rcycle(2,:,end,icycle))))>0.03 % Bending right (test on RSHO/Y)
                    value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                    temp1 = [temp1 max(value)];
                    clear value;
                elseif abs((Trial(itrial).Marker(59).Trajectory.rcycle(2,:,1,icycle)-(Trial(itrial).Marker(59).Trajectory.rcycle(2,:,end,icycle))))>0.03 % Bending right (test on LSHO/Y)
                    value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                    temp2 = [temp2 max(value)];
                    clear value;          
                end
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward                
                if abs((Trial(itrial).Marker(54).Trajectory.rcycle(2,:,1,icycle)-(Trial(itrial).Marker(54).Trajectory.rcycle(2,:,end,icycle))))>0.03 % Bending right (test on RSHO/Y)
                    value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                    temp1 = [temp1 max(value)];
                    clear value;
                elseif abs((Trial(itrial).Marker(59).Trajectory.rcycle(2,:,1,icycle)-(Trial(itrial).Marker(59).Trajectory.rcycle(2,:,end,icycle))))>0.03 % Bending right (test on LSHO/Y)
                    value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                    temp2 = [temp2 max(value)];
                    clear value;            
                end   
            end
        end
        Biomarker.BMo48(iparticipant,isession,1).label = "Trunk lateral bending | Thorax frontal angular velocity (max)";
        Biomarker.BMo48(iparticipant,isession,1).value = rad2deg(temp1);
        Biomarker.BMo48(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
        Biomarker.BMo48(iparticipant,isession,1).std   = std(rad2deg(temp1));
        Biomarker.BMo48(iparticipant,isession,1).units = '�deg.s-1';
        Biomarker.BMo48(iparticipant,isession,2).label = "Trunk lateral bending | Thorax frontal angular velocity (max)";
        Biomarker.BMo48(iparticipant,isession,2).value = rad2deg(temp2);
        Biomarker.BMo48(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
        Biomarker.BMo48(iparticipant,isession,2).std   = std(rad2deg(temp2));
        Biomarker.BMo48(iparticipant,isession,2).units = '�deg.s-1';
        clear temp1 temp2;
    end
end 
clear itrial;

% BMo49 /CHECKED
% d410 "Changing basic body position" Trunk sagittal bending	Thorax	Spatial/intensity	Thorax sagittal angle (rom)
%disp('  - BMo49');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                temp  = [temp max(value)-min(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                temp  = [temp max(value)-min(value)];
                clear value;
            end
        end
        Biomarker.BMo49(iparticipant,isession,1).label = "Trunk sagittal bending | Thorax sagittal angle (rom)";
        Biomarker.BMo49(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo49(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo49(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo49(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end 
clear itrial; 

% BMo50 /MODIFIED
% d410 "Changing basic body position"	Trunk lateral bending	Thorax	Spatial/intensity	Thorax sagittal angle (rom)
%disp('  - BMo50');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Lateral')
        temp1 = [];
        temp2 = [];
        for icycle = 1:size(Trial(itrial).Marker(54).Trajectory.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward            
                if abs((Trial(itrial).Marker(54).Trajectory.rcycle(2,:,1,icycle)-(Trial(itrial).Marker(54).Trajectory.rcycle(2,:,end,icycle))))>0.03 % Bending right (test on RSHO/Y)
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif abs((Trial(itrial).Marker(59).Trajectory.rcycle(2,:,1,icycle)-(Trial(itrial).Marker(59).Trajectory.rcycle(2,:,end,icycle))))>0.03 % Bending right (test on LSHO/Y)
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;          
                end
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward                
                if abs((Trial(itrial).Marker(54).Trajectory.rcycle(2,:,1,icycle)-(Trial(itrial).Marker(54).Trajectory.rcycle(2,:,end,icycle))))>0.03 % Bending right (test on RSHO/Y)
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif abs((Trial(itrial).Marker(59).Trajectory.rcycle(2,:,1,icycle)-(Trial(itrial).Marker(59).Trajectory.rcycle(2,:,end,icycle))))>0.03 % Bending right (test on LSHO/Y)
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;            
                end   
            end
        end
        Biomarker.BMo50(iparticipant,isession,1).label = "Trunk lateral bending | Thorax sagittal angle (rom)";
        Biomarker.BMo50(iparticipant,isession,1).value = rad2deg(temp1);
        Biomarker.BMo50(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
        Biomarker.BMo50(iparticipant,isession,1).std   = std(rad2deg(temp1));
        Biomarker.BMo50(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo50(iparticipant,isession,2).label = "Trunk lateral bending | Thorax sagittal angle (rom)";
        Biomarker.BMo50(iparticipant,isession,2).value = rad2deg(temp2);
        Biomarker.BMo50(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
        Biomarker.BMo50(iparticipant,isession,2).std   = std(rad2deg(temp2));
        Biomarker.BMo50(iparticipant,isession,2).units = '�deg';
        clear temp1 temp2;
    end
end 
clear itrial;

% BMo51 /MODIFIED
% d410 "Changing basic body position"	Trunk rotation	Thorax	Spatial/intensity	Thorax sagittal angle (rom)
%disp('  - BMo51');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Rotation')
        temp1 = [];
        temp2 = [];
        for icycle = 1:size(Trial(itrial).Marker(54).Trajectory.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward            
                if (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))>0 % Turning right
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))<0 % Turning left
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;          
                end
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward                
                if (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))>0 % Turning right
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))<0 % Turning left
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;            
                end   
            end
        end
        Biomarker.BMo51(iparticipant,isession,1).label = "Trunk rotation | Thorax sagittal angle (rom)";
        Biomarker.BMo51(iparticipant,isession,1).value = rad2deg(temp1);
        Biomarker.BMo51(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
        Biomarker.BMo51(iparticipant,isession,1).std   = std(rad2deg(temp1));
        Biomarker.BMo51(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo51(iparticipant,isession,2).label = "Trunk rotation | Thorax sagittal angle (rom)";
        Biomarker.BMo51(iparticipant,isession,2).value = rad2deg(temp2);
        Biomarker.BMo51(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
        Biomarker.BMo51(iparticipant,isession,2).std   = std(rad2deg(temp2));
        Biomarker.BMo51(iparticipant,isession,2).units = '�deg';
        clear temp1 temp2;
    end
end 
clear itrial;

% BMo53 /MODIFIED
% d410 "Changing basic body position"	Trunk sagittal bending	Thorax	Spatial/intensity	Thorax sagittal angular velocity (max)
%disp('  - BMo53');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.rcycle(1,2,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp max(value)];
                clear value;
            end
        end
        Biomarker.BMo53(iparticipant,isession,1).label = "Trunk sagittal bending | Thorax sagittal angular velocity (max)";
        Biomarker.BMo53(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo53(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo53(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo53(iparticipant,isession,1).units = '�deg.s-1';
        clear temp;
    end
end 
clear itrial; 

% BMo54 /MODIFIED
% d410 "Changing basic body position"	Trunk rotation	Thorax	Spatial/intensity	Thorax transversal angle (rom)
%disp('  - BMo54');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Rotation')
        temp1 = [];
        temp2 = [];
        for icycle = 1:size(Trial(itrial).Marker(54).Trajectory.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward            
                if (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))>0 % Turning right
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,1,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,1,:,icycle),[3,1,2]);
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))<0 % Turning left
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,1,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,1,:,icycle),[3,1,2]);
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;          
                end
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward                
                if (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))>0 % Turning right
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,1,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,1,:,icycle),[3,1,2]);
                    temp1 = [temp1 max(value)-min(value)];
                    clear value;
                elseif (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))<0 % Turning left
                    value = permute(Trial(itrial).Segment(12).Euler.rcycle(1,1,:,icycle),[3,1,2])-permute(Trial(itrial).Segment(5).Euler.rcycle(1,1,:,icycle),[3,1,2]);
                    temp2 = [temp2 max(value)-min(value)];
                    clear value;            
                end   
            end
        end
        Biomarker.BMo54(iparticipant,isession,1).label = "Trunk rotation | Thorax transversal angle (rom)";
        Biomarker.BMo54(iparticipant,isession,1).value = rad2deg(temp1);
        Biomarker.BMo54(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
        Biomarker.BMo54(iparticipant,isession,1).std   = std(rad2deg(temp1));
        Biomarker.BMo54(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo54(iparticipant,isession,2).label = "Trunk rotation | Thorax transversal angle (rom)";
        Biomarker.BMo54(iparticipant,isession,2).value = rad2deg(temp2);
        Biomarker.BMo54(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
        Biomarker.BMo54(iparticipant,isession,2).std   = std(rad2deg(temp2));
        Biomarker.BMo54(iparticipant,isession,2).units = '�deg';
        clear temp1 temp2;
    end
end 
clear itrial;

% BMo55 /MODIFIED
% d410 "Changing basic body position"	Trunk rotation	Thorax	Spatial/intensity	Thorax transversal angular velocity (max)
%disp('  - BMo55');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Rotation')
        temp1 = [];
        temp2 = [];
        for icycle = 1:size(Trial(itrial).Marker(54).Trajectory.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward            
                if (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))>0 % Turning right
                    value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.rcycle(1,1,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                    temp1 = [temp1 max(value)];
                    clear value;
                elseif (-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,1,icycle)+100-(-Trial(itrial).Marker(54).Trajectory.rcycle(3,:,end,icycle)+100))<0 % Turning left
                    value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.rcycle(1,1,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                    temp2 = [temp2 max(value)];
                    clear value;          
                end
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward                
                if (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))>0 % Turning right
                    value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.rcycle(1,1,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                    temp1 = [temp1 max(value)];
                    clear value;
                elseif (Trial(itrial).Marker(54).Trajectory.rcycle(1,:,1,icycle)+100-(Trial(itrial).Marker(54).Trajectory.rcycle(1,:,end,icycle)+100))<0 % Turning left
                    value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.rcycle(1,1,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                    temp2 = [temp2 max(value)];
                    clear value;            
                end   
            end
        end
        Biomarker.BMo55(iparticipant,isession,1).label = "Trunk rotation | Thorax transversal angular velocity (max)";
        Biomarker.BMo55(iparticipant,isession,1).value = rad2deg(temp1);
        Biomarker.BMo55(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
        Biomarker.BMo55(iparticipant,isession,1).std   = std(rad2deg(temp1));
        Biomarker.BMo55(iparticipant,isession,1).units = '�deg.s-1';
        Biomarker.BMo55(iparticipant,isession,2).label = "Trunk rotation | Thorax transversal angular velocity (max)";
        Biomarker.BMo55(iparticipant,isession,2).value = rad2deg(temp2);
        Biomarker.BMo55(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
        Biomarker.BMo55(iparticipant,isession,2).std   = std(rad2deg(temp2));
        Biomarker.BMo55(iparticipant,isession,2).units = '�deg.s-1';
        clear temp1 temp2;
    end
end 
clear itrial;

% BMo57 /CHECKED
% d410 "Changing basic body position" Trunk sagittal bending	Lumbar	Spatial/intensity	Upper lumbar sagittal angle (max)
%disp('  - BMo57');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(16).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(permute(Trial(itrial).Segment(16).Euler.rcycle(1,2,:,icycle),[3,1,2]));
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(permute(Trial(itrial).Segment(16).Euler.rcycle(1,3,:,icycle),[3,1,2]));
                temp  = [temp max(value)];
                clear value;
            end
        end
        Biomarker.BMo57(iparticipant,isession,1).label = "Trunk sagittal bending | Upper lumbar sagittal angle (max)";
        Biomarker.BMo57(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo57(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo57(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo57(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end
clear itrial; 

% BMo58 /CHECKED
% d410 "Changing basic body position" Trunk sagittal bending	Lumbar	Spatial/intensity	Upper lumbar sagittal angular velocity (max)
%disp('  - BMo58');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(16).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(gradient(permute(Trial(itrial).Segment(16).Euler.rcycle(1,2,:,icycle),[3,2,1])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(gradient(permute(Trial(itrial).Segment(16).Euler.rcycle(1,3,:,icycle),[3,2,1])))/(Trial(itrial).RCycle(icycle).n/100)*100;
                temp  = [temp max(value)];
                clear value;
            end
        end
        Biomarker.BMo58(iparticipant,isession,1).label = "Trunk sagittal bending | Upper lumbar sagittal angular velocity (max)";
        Biomarker.BMo58(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo58(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo58(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo58(iparticipant,isession,1).units = '�deg.s-1';
        clear temp;
    end
end
clear itrial; 

% BMo59 /CHECKED
% d410 "Changing basic body position" Trunk sagittal bending	Thorax	Spatial/intensity	Upper thorax sagittal angle (max)
%disp('  - BMo59');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Trunk_Forward')
        temp = [];
        for icycle = 1:size(Trial(itrial).Segment(18).Euler.rcycle,4)
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(permute(Trial(itrial).Segment(18).Euler.rcycle(1,2,:,icycle),[3,2,1]));
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(permute(Trial(itrial).Segment(18).Euler.rcycle(1,3,:,icycle),[3,2,1]));
                temp  = [temp max(value)];
                clear value;
            end
        end
        Biomarker.BMo59(iparticipant,isession,1).label = "Trunk sagittal bending | Upper thorax sagittal angle (max)";
        Biomarker.BMo59(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo59(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo59(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo59(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end
clear itrial; 

% BMo60 /CHECKED
% d410 "Changing basic body position"	Sit to stand	Lumbar	Spatial/intensity	Upper/lower lumbar sagittal angle (max)
%disp('  - BMo60');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            value = abs(permute(Trial(itrial).Joint(14).Euler.rcycle(1,1,:,icycle),[3,1,2]));
            temp  = [temp max(value)];
            clear value;
        end
        Biomarker.BMo60(iparticipant,isession,1).label = "Sit to stand | Upper/lower lumbar sagittal angle (max)";
        Biomarker.BMo60(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo60(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo60(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo60(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end 
clear itrial;  

% BMo61 /MODIFIED
% d410 "Changing basic body position"	Sit to stand	Lumbar	Spatial/intensity	Upper/lower lumbar sagittal angular velocity (max)
%disp('  - BMo61');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            value = abs(gradient(permute(Trial(itrial).Joint(14).Euler.rcycle(1,1,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
            temp  = [temp max(value)];
            clear value;
        end
        Biomarker.BMo61(iparticipant,isession,1).label = "Sit to stand | Upper/lower lumbar sagittal angular velocity (max)";
        Biomarker.BMo61(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo61(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo61(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo61(iparticipant,isession,1).units = '�deg.s-1';
        clear temp;
    end
end 
clear itrial; 

% BMo62 /CHECKED
% d410 "Changing basic body position"	Sit to stand	Thorax	Spatial/intensity	Upper/lower thorax sagittal angle (max)
%disp('  - BMo62');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            value = abs(permute(Trial(itrial).Joint(16).Euler.rcycle(1,1,:,icycle),[3,1,2]));
            temp  = [temp max(value)];
            clear value;
        end
        Biomarker.BMo62(iparticipant,isession,1).label = "Sit to stand | Upper/lower thorax sagittal angle (max)";
        Biomarker.BMo62(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo62(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo62(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo62(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end 
clear itrial; 

% BMo63 /MODIFIED
% d410 "Changing basic body position"	Sit to stand	Thorax	Spatial/intensity	Upper/lower thorax sagittal angular velocity (max)
%disp('  - BMo63');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'S2S_Unconstrained')
        temp = [];
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4)
            value = abs(gradient(permute(Trial(itrial).Joint(16).Euler.rcycle(1,1,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100;
            temp  = [temp max(value)];
            clear value;
        end
        Biomarker.BMo63(iparticipant,isession,1).label = "Sit to stand | Upper/lower thorax sagittal angular velocity (max)";
        Biomarker.BMo63(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo63(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo63(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo63(iparticipant,isession,1).units = '�deg.s-1';
        clear temp;
    end
end 
clear itrial; 

% BMo72 /CHECKED
% d415 "Maintaining a body position"	Sitting	Lumbar/pelvis	Spatial/intensity	Lumbopelvic sagittal angle (max)
%disp('  - BMo72');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Posture_Sitting')
        temp   = [];
        % Sitting session splitted into 3 parts to allow reliability
        % assessments
        value = abs(permute(Trial(itrial).Joint(5).Euler.smooth(1,1,1:fix(end/3)),[3,1,2]));
        temp  = [temp max(value)];
        clear value;
        value = abs(permute(Trial(itrial).Joint(5).Euler.smooth(1,1,fix(end/3)+1:2*fix(end/3)),[3,1,2]));
        temp  = [temp max(value)];
        clear value;
        value = abs(permute(Trial(itrial).Joint(5).Euler.smooth(1,1,2*fix(end/3)+1:end),[3,1,2]));
        temp  = [temp max(value)];
        clear value;
        Biomarker.BMo72(iparticipant,isession,1).label = "Sitting | Lumbopelvic sagittal angle (max)";
        Biomarker.BMo72(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo72(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo72(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo72(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end 
clear itrial; 

% BMo75 /CHECKED
% d415 "Maintaining a body position"	Two-legged standing	Thorax	Spatial/intensity	Thorax sagittal angle (max)
%disp('  - BMo75');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Posture_Standing')
        temp = [];
        % Sitting session splitted into 3 parts to allow reliability
        % assessments
        value = abs(permute(Trial(itrial).Segment(12).Euler.smooth(1,3,1:fix(end/3)),[3,1,2]));
        temp  = [temp max(value)];
        clear value;
        value = abs(permute(Trial(itrial).Segment(12).Euler.smooth(1,3,fix(end/3)+1:2*fix(end/3)),[3,1,2]));
        temp  = [temp max(value)];
        clear value;
        value = abs(permute(Trial(itrial).Segment(12).Euler.smooth(1,3,2*fix(end/3)+1:end),[3,1,2]));
        temp  = [temp max(value)];
        clear value;   
        Biomarker.BMo75(iparticipant,isession,1).label = "Two-legged standing | Thorax sagittal angle (max)";
        Biomarker.BMo75(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo75(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo75(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo75(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end 
clear itrial; 

% BMo77 /CHECKED
% d430 "Lifting and carrying objects"	Trunk sagittal bending	Pelvis/leg	Spatial/intensity	Hip sagittal angle (min)
%disp('  - BMo77');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Weight_Unconstrained')
        temp1 = [];
        temp2 = [];
        for icycle = 1:2:size(Trial(itrial).Joint(5).Euler.lcycle,4) % Only cycles with weight (lifting subcycle = lcycle)
            value1 = abs(permute(Trial(itrial).Joint(4).Euler.lcycle(1,1,:,icycle),[3,1,2]));
            temp1  = [temp1 min(value1)];
            value2 = abs(permute(Trial(itrial).Joint(9).Euler.lcycle(1,1,:,icycle),[3,1,2]));
            temp2  = [temp2 min(value2)];
            clear value1 value2;
        end
        Biomarker.BMo77(iparticipant,isession,1).label = "Trunk sagittal bending | Hip sagittal angle (min)";
        Biomarker.BMo77(iparticipant,isession,1).value = rad2deg(temp1);
        Biomarker.BMo77(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
        Biomarker.BMo77(iparticipant,isession,1).std   = std(rad2deg(temp1));
        Biomarker.BMo77(iparticipant,isession,1).units = '�deg';
        Biomarker.BMo77(iparticipant,isession,2).label = "Trunk sagittal bending | Hip sagittal angle (min)";
        Biomarker.BMo77(iparticipant,isession,2).value = rad2deg(temp2);
        Biomarker.BMo77(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
        Biomarker.BMo77(iparticipant,isession,2).std   = std(rad2deg(temp2));
        Biomarker.BMo77(iparticipant,isession,2).units = '�deg';
        clear temp1 temp2;
    end
end
clear itrial;

% BMo79 /MODIFIED
% d430 "Lifting and carrying objects"	Trunk sagittal bending	Leg	Coordination	Hip/knee deviation phase (mean)
%disp('  - BMo79');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Weight_Unconstrained')
        phi1 = [];
        phi2 = [];
        phi3 = [];
        phi4 = [];
        for icycle = 1:2:size(Trial(itrial).Joint(5).Euler.lcycle,4) % Only cycles with weight (lifting subcycle = lcycle)
            % Right hip
            angle1              = permute(Trial(itrial).Joint(4).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            normalisedAngle1    = (angle1-min(angle1))/(max(angle1)-min(angle1))*2-1;
            normalisedVelocity1 = gradient(angle1)/max(abs(gradient(angle1)));
            phi1                = [phi1 atan(normalisedVelocity1./normalisedAngle1)]; % Phase angle
            % Right knee
            angle2              = -permute(Trial(itrial).Joint(3).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            normalisedAngle2    = (angle2-min(angle2))/(max(angle2)-min(angle2))*2-1;
            normalisedVelocity2 = gradient(angle2)/max(abs(gradient(angle2)));
            phi2                = [phi2 atan(normalisedVelocity2./normalisedAngle2)]; % Phase angle
            % Left hip
            angle3              = permute(Trial(itrial).Joint(9).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            normalisedAngle3    = (angle3-min(angle3))/(max(angle3)-min(angle3))*2-1;
            normalisedVelocity3 = gradient(angle3)/max(abs(gradient(angle3)));
            phi3                = [phi3 atan(normalisedVelocity3./normalisedAngle3)]; % Phase angle
            % Left knee
            angle4              = -permute(Trial(itrial).Joint(8).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            normalisedAngle4    = (angle4-min(angle4))/(max(angle4)-min(angle4))*2-1;
            normalisedVelocity4 = gradient(angle4)/max(abs(gradient(angle4)));
            phi4                = [phi4 atan(normalisedVelocity4./normalisedAngle4)]; % Phase angle
            clear angle1 angle2 angle3 angle4 ...
                  normalisedAngle1 normalisedAngle2 normalisedAngle3 normalisedAngle4 ...
                  normalisedVelocity1 normalisedVelocity2 normalisedVelocity3 normalisedVelocity4
        end
        DP1(1) = sum(std(phi1(:,1)-phi2(:,1)))/101;
        DP1(2) = sum(std(phi1(:,2)-phi2(:,2)))/101;
        if size(phi1,2) > 2
            DP1(3) = sum(std(phi1(:,3)-phi2(:,3)))/101;
        end
        DP2(1) = sum(std(phi3(:,1)-phi4(:,1)))/101;   
        DP2(2) = sum(std(phi3(:,2)-phi4(:,2)))/101; 
        if size(phi1,2) > 2
            DP2(3) = sum(std(phi3(:,3)-phi4(:,3)))/101;
        end
        Biomarker.BMo79(iparticipant,isession,1).label = "Weight_Unconstrained | Hip/knee deviation phase (mean)";
        Biomarker.BMo79(iparticipant,isession,1).value = rad2deg(DP1);
        Biomarker.BMo79(iparticipant,isession,1).mean  = mean(rad2deg(DP1));
        Biomarker.BMo79(iparticipant,isession,1).std   = std(rad2deg(DP1));
        Biomarker.BMo79(iparticipant,isession,1).units = '�deg';
        clear phi1 phi2 phi3 phi4 relativePhase1 relativePhase2 DP1 DP2;
    end
end
clear itrial;

% BMo80 /CHECKED
% d430 "Lifting and carrying objects" Trunk sagittal bending	Lumbar/pelvis	Spatial/intensity	Lumbopelvic sagittal angle (max)
%disp('  - BMo80');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Weight_Unconstrained')
        temp = [];
        for icycle = 1:2:size(Trial(itrial).Joint(10).Euler.lcycle,4) % Only cycles with weight
            value = abs(permute(Trial(itrial).Joint(10).Euler.lcycle(1,1,:,icycle),[3,1,2]));
            temp  = [temp max(value)];
            clear value;
        end
        Biomarker.BMo80(iparticipant,isession,1).label = "Weight_Unconstrained | Lumbopelvic sagittal angle (max)";
        Biomarker.BMo80(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo80(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo80(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo80(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end
clear itrial;

% BMo81 /CHECKED
% d430 "Lifting and carrying objects"	Trunk sagittal bending	Thorax/lumbar	Spatial/intensity	Thoracolumbar sagittal angle (max)
%disp('  - BMo81');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Weight_Unconstrained')
        temp = [];
        for icycle = 1:2:size(Trial(itrial).Joint(11).Euler.lcycle,4) % Only cycles with weight
            value = abs(permute(Trial(itrial).Joint(11).Euler.lcycle(1,1,:,icycle),[3,1,2]));
            temp  = [temp max(value)];
            clear value;
        end
        Biomarker.BMo81(iparticipant,isession,1).label = "Weight_Unconstrained | Thoracolumbar sagittal angle (max)";
        Biomarker.BMo81(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo81(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo81(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo81(iparticipant,isession,1).units = '�deg';
        clear temp;
    end
end
clear itrial;

% BMo82 /MODIFIED
% d430 "Lifting and carrying objects"	Trunk sagittal bending	Thorax	Spatial/intensity	Thorax linear acceleration (max)
%disp('  - BMo82');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Weight_Unconstrained')
        temp = [];
        for icycle = 1:2:size(Trial(itrial).Segment(12).dj.lcycle,4) % Only cycles with weight
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = gradient(gradient(sqrt(permute(Trial(itrial).Segment(12).dj.lcycle(2,1,:,icycle),[3,1,2]).^2+...
                                               permute(Trial(itrial).Segment(12).dj.lcycle(3,1,:,icycle),[3,1,2]).^2)))/(Trial(itrial).RCycle(icycle).n/100)^2*1000; % Sagittal linear acceleration
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = gradient(gradient(sqrt(permute(Trial(itrial).Segment(12).dj.lcycle(1,1,:,icycle),[3,1,2]).^2+...
                                               permute(Trial(itrial).Segment(12).dj.lcycle(2,1,:,icycle),[3,1,2]).^2)))/(Trial(itrial).RCycle(icycle).n/100)^2*1000; % Sagittal linear acceleration
                temp  = [temp max(value)];
                clear value;
            end            
        end
        Biomarker.BMo82(iparticipant,isession,1).label = "Weight lifting | Thorax linear acceleration (max)";
        Biomarker.BMo82(iparticipant,isession,1).value = temp;
        Biomarker.BMo82(iparticipant,isession,1).mean  = mean(temp);
        Biomarker.BMo82(iparticipant,isession,1).std   = std(temp);
        Biomarker.BMo82(iparticipant,isession,1).units = 'm.s-2';
        clear temp;
    end
end
clear itrial;

% BMo83 /MODIFIED
% d430 "Lifting and carrying objects"	Trunk sagittal bending	Thorax	Spatial/intensity	Thorax linear velocity (max)
%disp('  - BMo83');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Weight_Unconstrained')
        temp = [];
        for icycle = 1:2:size(Trial(itrial).Segment(12).dj.lcycle,4) % Only cycles with weight
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = gradient(sqrt(permute(Trial(itrial).Segment(12).dj.lcycle(2,1,:,icycle),[3,1,2]).^2+...
                                      permute(Trial(itrial).Segment(12).dj.lcycle(3,1,:,icycle),[3,1,2]).^2))/(Trial(itrial).RCycle(icycle).n/100)*100; % Sagittal linear velocity
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = gradient(sqrt(permute(Trial(itrial).Segment(12).dj.lcycle(1,1,:,icycle),[3,1,2]).^2+...
                                      permute(Trial(itrial).Segment(12).dj.lcycle(2,1,:,icycle),[3,1,2]).^2))/(Trial(itrial).RCycle(icycle).n/100)*100; % Sagittal linear velocity
                temp  = [temp max(value)];
                clear value;
            end       
        end
        Biomarker.BMo83(iparticipant,isession,1).label = "Weight lifting | Thorax linear velocity (max)";
        Biomarker.BMo83(iparticipant,isession,1).value = temp;
        Biomarker.BMo83(iparticipant,isession,1).mean  = mean(temp);
        Biomarker.BMo83(iparticipant,isession,1).std   = std(temp);
        Biomarker.BMo83(iparticipant,isession,1).units = 'm.s-1';
        clear temp;
    end
end
clear itrial;

% BMo84 /CHECKED
% d430 "Lifting and carrying objects"	Trunk sagittal bending	Lumbar	Spatial/intensity	Upper lumbar curvature (max)
%disp('  - BMo84');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Weight_Unconstrained')
        temp = [];
        for icycle = 1:size(Trial(itrial).Marker(7).Trajectory.lcycle,4)
            vec1  = [];
            vec2  = [];
            % L3-L1 vector
            vec1 = permute(Trial(itrial).Marker(39).Trajectory.lcycle(:,:,:,icycle) - Trial(itrial).Marker(38).Trajectory.lcycle(:,:,:,icycle),[3,1,2]);
            % L1-T12 vector
            vec2 = permute(Trial(itrial).Marker(38).Trajectory.lcycle(:,:,:,icycle) - Trial(itrial).Marker(47).Trajectory.lcycle(:,:,:,icycle),[3,1,2]);
            for t = 1:101
                value(t) = atan2(norm(cross(vec1(t,:),vec2(t,:))),dot(vec1(t,:),vec2(t,:)));
            end         
            temp = [temp max(abs(value))];
            clear value;
        end
        Biomarker.BMo84(iparticipant,isession,1).label = "Weight lifting | Lower thorax curvature (max)";
        Biomarker.BMo84(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo84(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo84(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo84(iparticipant,isession,1).units = '�deg';
        clear vec1 vec2 value;
    end
end
clear itrial;

% BMo85 /MODIFIED
% d430 "Lifting and carrying objects"	Trunk sagittal bending	Thorax	Spatial/intensity	Thorax angular acceleration (max)
%disp('  - BMo85');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Weight_Unconstrained')
        temp = [];
        for icycle = 1:2:size(Trial(itrial).Segment(12).Euler.lcycle,4) % Only cycles with weight
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(gradient(gradient(permute(Trial(itrial).Segment(12).Euler.lcycle(1,2,:,icycle),[3,1,2]))))/(Trial(itrial).RCycle(icycle).n/100)^2*100; % Sagittal angular acceleration
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(gradient(gradient(permute(Trial(itrial).Segment(12).Euler.lcycle(1,3,:,icycle),[3,1,2]))))/(Trial(itrial).RCycle(icycle).n/100)^2*100; % Sagittal angular acceleration
                temp  = [temp max(value)];
                clear value;
            end
        end
        Biomarker.BMo85(iparticipant,isession,1).label = "Weight lifting | Thorax angular acceleration (max)";
        Biomarker.BMo85(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo85(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo85(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo85(iparticipant,isession,1).units = '�deg.s-2';
        clear temp;
    end
end
clear itrial;

% BMo86 /MODIFIED
% d430 "Lifting and carrying objects"	Trunk sagittal bending	Thorax	Spatial/intensity	Thorax angular velocity (max)
%disp('  - BMo86');
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Weight_Unconstrained')
        temp = [];
        for icycle = 1:2:size(Trial(itrial).Segment(12).Euler.lcycle,4) % Only cycles with weight
            if abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(1,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(1,:,:),3))>0.1 % -Z forward
                value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.lcycle(1,2,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100; % Sagittal angular acceleration
                temp  = [temp max(value)];
                clear value;
            elseif abs(mean(Trial(itrial).Marker(1).Trajectory.smooth(3,:,:),3)-mean(Trial(itrial).Marker(6).Trajectory.smooth(3,:,:),3))>0.1 % X forward
                value = abs(gradient(permute(Trial(itrial).Segment(12).Euler.lcycle(1,3,:,icycle),[3,1,2])))/(Trial(itrial).RCycle(icycle).n/100)*100; % Sagittal angular acceleration
                temp  = [temp max(value)];
                clear value;
            end
        end
        Biomarker.BMo86(iparticipant,isession,1).label = "Weight lifting | Thorax angular velocity (max)";
        Biomarker.BMo86(iparticipant,isession,1).value = rad2deg(temp);
        Biomarker.BMo86(iparticipant,isession,1).mean  = mean(rad2deg(temp));
        Biomarker.BMo86(iparticipant,isession,1).std   = std(rad2deg(temp));
        Biomarker.BMo86(iparticipant,isession,1).units = '�deg.s-1';
        clear temp;
    end
end
clear itrial;

% BMo87 /CHECKED
% d450 "Walking"	Walking	Lumbar/pelvis	Variability	Lumbar/pelvis frontal angle (coefficient of variation)
%disp('  - BMo87');
temp1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
temp2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4) % Right cycles
            value = permute(Trial(itrial).Joint(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
            temp1 = [temp1 abs(value)];
            clear value;
        end
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.lcycle,4) % Left cycles
            value = permute(Trial(itrial).Joint(5).Euler.lcycle(1,2,:,icycle),[3,1,2]);
            temp2 = [temp2 abs(value)];
            clear value;
        end
    end
end 
clear itrial;
% Split cycles into 3 groups to allow reliability assessment
N1       = 101;
S1(:,1)  = std(temp1(:,1:fix(end/3)),0,2);
X1(:,1)  = mean(temp1(:,1:fix(end/3)),2);
CV1(:,1) = sqrt(sum(S1(:,1).^2,1)/N1)./(sum(abs(X1(:,1)))/N1)*100; % expressed in %
S1(:,2)  = std(temp1(:,fix(end/3)+1:fix(end/3)*2),0,2);
X1(:,2)  = mean(temp1(:,fix(end/3)+1:fix(end/3)*2),2);
CV1(:,2) = sqrt(sum(S1(:,2).^2,1)/N1)./(sum(abs(X1(:,2)))/N1)*100; % expressed in %
S1(:,3)  = std(temp1(:,fix(end/3)*2+1:end),0,2);
X1(:,3)  = mean(temp1(:,fix(end/3)*2+1:end),2);
CV1(:,3) = sqrt(sum(S1(:,3).^2,1)/N1)./(sum(abs(X1(:,3)))/N1)*100; % expressed in %
Biomarker.BMo87(iparticipant,isession,1).label = "Walking | Lumbar/pelvis frontal angle (coefficient of variation)";
Biomarker.BMo87(iparticipant,isession,1).value = CV1;
Biomarker.BMo87(iparticipant,isession,1).mean  = mean(CV1);
Biomarker.BMo87(iparticipant,isession,1).std   = std(CV1);
Biomarker.BMo87(iparticipant,isession,1).units = '%';
N2       = 101;
S2(:,1)  = std(temp2(:,1:fix(end/3)),0,2);
X2(:,1)  = mean(temp2(:,1:fix(end/3)),2);
CV2(:,1) = sqrt(sum(S2(:,1).^2,1)/N2)./(sum(abs(X2(:,1)))/N2)*100; % expressed in %
S2(:,2)  = std(temp2(:,fix(end/3)+1:fix(end/3)*2),0,2);
X2(:,2)  = mean(temp2(:,fix(end/3)+1:fix(end/3)*2),2);
CV2(:,2) = sqrt(sum(S2(:,2).^2,1)/N2)./(sum(abs(X2(:,2)))/N2)*100; % expressed in %
S2(:,3)  = std(temp2(:,fix(end/3)*2+1:end),0,2);
X2(:,3)  = mean(temp2(:,fix(end/3)*2+1:end),2);
CV2(:,3) = sqrt(sum(S2(:,3).^2,1)/N2)./(sum(abs(X2(:,3)))/N2)*100; % expressed in %
Biomarker.BMo87(iparticipant,isession,2).label = "Walking | Lumbar/pelvis frontal angle (coefficient of variation)";
Biomarker.BMo87(iparticipant,isession,2).value = CV2;
Biomarker.BMo87(iparticipant,isession,2).mean  = mean(CV2);
Biomarker.BMo87(iparticipant,isession,2).std   = std(CV2);
Biomarker.BMo87(iparticipant,isession,2).units = '%';
clear temp1 temp2 S1 S2 X1 X2 N1 N2 CV1 CV2;

% BMo89 /CHECKED
% d450 "Walking"	Walking	Lumbar/pelvis	Variability	Lumbar/pelvis sagittal angle (coefficient of variation)
%disp('  - BMo89');
temp1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
temp2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4) % Right cycles
            value = permute(Trial(itrial).Joint(5).Euler.rcycle(1,1,:,icycle),[3,1,2]);
            temp1 = [temp1 abs(value)];
            clear value;
        end
        for icycle = 1:size(Trial(itrial).Joint(5).Euler.lcycle,4) % Left cycles
            value = permute(Trial(itrial).Joint(5).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            temp2 = [temp2 abs(value)];
            clear value;
        end
    end
end 
clear itrial;
% Split cycles into 3 groups to allow reliability assessment
N1       = 101;
S1(:,1)  = std(temp1(:,1:fix(end/3)),0,2);
X1(:,1)  = mean(temp1(:,1:fix(end/3)),2);
CV1(:,1) = sqrt(sum(S1(:,1).^2,1)/N1)./(sum(abs(X1(:,1)))/N1)*100; % expressed in %
S1(:,2)  = std(temp1(:,fix(end/3)+1:fix(end/3)*2),0,2);
X1(:,2)  = mean(temp1(:,fix(end/3)+1:fix(end/3)*2),2);
CV1(:,2) = sqrt(sum(S1(:,2).^2,1)/N1)./(sum(abs(X1(:,2)))/N1)*100; % expressed in %
S1(:,3)  = std(temp1(:,fix(end/3)*2+1:end),0,2);
X1(:,3)  = mean(temp1(:,fix(end/3)*2+1:end),2);
CV1(:,3) = sqrt(sum(S1(:,3).^2,1)/N1)./(sum(abs(X1(:,3)))/N1)*100; % expressed in %
Biomarker.BMo89(iparticipant,isession,1).label = "Walking | Lumbar/pelvis sagittal angle (coefficient of variation)";
Biomarker.BMo89(iparticipant,isession,1).value = CV1;
Biomarker.BMo89(iparticipant,isession,1).mean  = mean(CV1);
Biomarker.BMo89(iparticipant,isession,1).std   = std(CV1);
Biomarker.BMo89(iparticipant,isession,1).units = '%';
N2       = 101;
S2(:,1)  = std(temp2(:,1:fix(end/3)),0,2);
X2(:,1)  = mean(temp2(:,1:fix(end/3)),2);
CV2(:,1) = sqrt(sum(S2(:,1).^2,1)/N2)./(sum(abs(X2(:,1)))/N2)*100; % expressed in %
S2(:,2)  = std(temp2(:,fix(end/3)+1:fix(end/3)*2),0,2);
X2(:,2)  = mean(temp2(:,fix(end/3)+1:fix(end/3)*2),2);
CV2(:,2) = sqrt(sum(S2(:,2).^2,1)/N2)./(sum(abs(X2(:,2)))/N2)*100; % expressed in %
S2(:,3)  = std(temp2(:,fix(end/3)*2+1:end),0,2);
X2(:,3)  = mean(temp2(:,fix(end/3)*2+1:end),2);
CV2(:,3) = sqrt(sum(S2(:,3).^2,1)/N2)./(sum(abs(X2(:,3)))/N2)*100; % expressed in %
Biomarker.BMo89(iparticipant,isession,2).label = "Walking | Lumbar/pelvis sagittal angle (coefficient of variation)";
Biomarker.BMo89(iparticipant,isession,2).value = CV2;
Biomarker.BMo89(iparticipant,isession,2).mean  = mean(CV2);
Biomarker.BMo89(iparticipant,isession,2).std   = std(CV2);
Biomarker.BMo89(iparticipant,isession,2).units = '%';
clear temp1 temp2 S1 S2 X1 X2 N1 N2 CV1 CV2;

% BMo91 /DELETED (no possible measurement around this axis with the markerset)
% d450 "Walking"	Walking	Lumbar/pelvis	Variability	Lumbar/pelvis transversal angle (coefficient of variation)
%disp('  - BMo91');
%temp1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
%temp2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
%for itrial = 1:size(Trial,2)
%    if contains(Trial(itrial).type,'Gait_Normal')
%        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4) % Right cycles
%            value = permute(Trial(itrial).Joint(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
%            temp1 = [temp1 abs(value)];
%            clear value;
%        end
%        for icycle = 1:size(Trial(itrial).Joint(5).Euler.lcycle,4) % Left cycles
%            value = permute(Trial(itrial).Joint(5).Euler.lcycle(1,3,:,icycle),[3,1,2]);
%            temp2 = [temp2 abs(value)];
%            clear value;
%        end
%    end
%end 
%clear itrial;
%% Split cycles into 3 groups to allow reliability assessment
%N1       = 101;
%S1(:,1)  = std(temp1(:,1:fix(end/3)),0,2);
%X1(:,1)  = mean(temp1(:,1:fix(end/3)),2);
%CV1(:,1) = sqrt(sum(S1(:,1).^2,1)/N1)./(sum(abs(X1(:,1)))/N1)*100; % expressed in %
%S1(:,2)  = std(temp1(:,fix(end/3)+1:fix(end/3)*2),0,2);
%X1(:,2)  = mean(temp1(:,fix(end/3)+1:fix(end/3)*2),2);
%CV1(:,2) = sqrt(sum(S1(:,2).^2,1)/N1)./(sum(abs(X1(:,2)))/N1)*100; % expressed in %
%S1(:,3)  = std(temp1(:,fix(end/3)*2+1:end),0,2);
%X1(:,3)  = mean(temp1(:,fix(end/3)*2+1:end),2);
%CV1(:,3) = sqrt(sum(S1(:,3).^2,1)/N1)./(sum(abs(X1(:,3)))/N1)*100; % expressed in %
%Biomarker.BMo91(iparticipant,isession,1).label = "Walking | Lumbar/pelvis transversal angle (coefficient of variation)";
%Biomarker.BMo91(iparticipant,isession,1).value = CV1;
%Biomarker.BMo91(iparticipant,isession,1).mean  = mean(CV1);
%Biomarker.BMo91(iparticipant,isession,1).std   = std(CV1);
%Biomarker.BMo91(iparticipant,isession,1).units = '%';
%N2       = 101;
%S2(:,1)  = std(temp2(:,1:fix(end/3)),0,2);
%X2(:,1)  = mean(temp2(:,1:fix(end/3)),2);
%CV2(:,1) = sqrt(sum(S2(:,1).^2,1)/N2)./(sum(abs(X2(:,1)))/N2)*100; % expressed in %
%S2(:,2)  = std(temp2(:,fix(end/3)+1:fix(end/3)*2),0,2);
%X2(:,2)  = mean(temp2(:,fix(end/3)+1:fix(end/3)*2),2);
%CV2(:,2) = sqrt(sum(S2(:,2).^2,1)/N2)./(sum(abs(X2(:,2)))/N2)*100; % expressed in %
%S2(:,3)  = std(temp2(:,fix(end/3)*2+1:end),0,2);
%X2(:,3)  = mean(temp2(:,fix(end/3)*2+1:end),2);
%CV2(:,3) = sqrt(sum(S2(:,3).^2,1)/N2)./(sum(abs(X2(:,3)))/N2)*100; % expressed in %
%Biomarker.BMo91(iparticipant,isession,2).label = "Walking | Lumbar/pelvis transversal angle (coefficient of variation)";
%Biomarker.BMo91(iparticipant,isession,2).value = CV2;
%Biomarker.BMo91(iparticipant,isession,2).mean  = mean(CV2);
%Biomarker.BMo91(iparticipant,isession,2).std   = std(CV2);
%Biomarker.BMo91(iparticipant,isession,2).units = '%';
%clear temp1 temp2 S1 S2 X1 X2 N1 N2 CV1 CV2;

% BMo98 /DELETED (no possible measurement around this axis with the markerset)
% d450 "Walking"	Walking	Lumbar/pelvis	Spatial/intensity	Lumbopelvic frontal angle (rom)
%disp('  - BMo98');
%temp1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
%temp2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
%for itrial = 1:size(Trial,2)
%    if contains(Trial(itrial).type,'Gait_Normal')
%        for icycle = 1:size(Trial(itrial).Joint(5).Euler.rcycle,4) % Right cycles
%            value = permute(Trial(itrial).Joint(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
%            temp1 = [temp1 max(value)-min(value)];
%            clear value;
%        end
%        for icycle = 1:size(Trial(itrial).Joint(5).Euler.lcycle,4) % Left cycles
%            value = permute(Trial(itrial).Joint(5).Euler.lcycle(1,2,:,icycle),[3,1,2]);
%            temp2 = [temp2 max(value)-min(value)];
%            clear value;
%        end
%    end
%end 
%clear itrial;
%Biomarker.BMo98(iparticipant,isession,1).label = "Walking | Lumbopelvic frontal angle (rom)";
%Biomarker.BMo98(iparticipant,isession,1).value = rad2deg(temp1);
%Biomarker.BMo98(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
%Biomarker.BMo98(iparticipant,isession,1).std   = std(rad2deg(temp1));
%Biomarker.BMo98(iparticipant,isession,1).units = '�deg';
%Biomarker.BMo98(iparticipant,isession,2).label = "Walking | Lumbopelvic frontal angle (rom)";
%Biomarker.BMo98(iparticipant,isession,2).value = rad2deg(temp2);
%Biomarker.BMo98(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
%Biomarker.BMo98(iparticipant,isession,2).std   = std(rad2deg(temp2));
%Biomarker.BMo98(iparticipant,isession,2).units = '�deg';
%clear temp1 temp2;

% BMo99 /CHECKED
% d450 "Walking"	Walking	Pelvis 	Variability	Pelvis frontal angle (coefficient of variation)
%disp('  - BMo99');
temp1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
temp2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Segment(5).Euler.rcycle,4) % Right cycles
            value = permute(Trial(itrial).Segment(5).Euler.rcycle(1,2,:,icycle),[3,1,2]);
            temp1 = [temp1 abs(value)];
            clear value;
        end
        for icycle = 1:size(Trial(itrial).Segment(5).Euler.lcycle,4) % Left cycles
            value = permute(Trial(itrial).Segment(5).Euler.lcycle(1,2,:,icycle),[3,1,2]);
            temp2 = [temp2 abs(value)];
            clear value;
        end
    end
end 
clear itrial;
% Split cycles into 3 groups to allow reliability assessment
N1       = 101;
S1(:,1)  = std(temp1(:,1:fix(end/3)),0,2);
X1(:,1)  = mean(temp1(:,1:fix(end/3)),2);
CV1(:,1) = sqrt(sum(S1(:,1).^2,1)/N1)./(sum(abs(X1(:,1)))/N1)*100; % expressed in %
S1(:,2)  = std(temp1(:,fix(end/3)+1:fix(end/3)*2),0,2);
X1(:,2)  = mean(temp1(:,fix(end/3)+1:fix(end/3)*2),2);
CV1(:,2) = sqrt(sum(S1(:,2).^2,1)/N1)./(sum(abs(X1(:,2)))/N1)*100; % expressed in %
S1(:,3)  = std(temp1(:,fix(end/3)*2+1:end),0,2);
X1(:,3)  = mean(temp1(:,fix(end/3)*2+1:end),2);
CV1(:,3) = sqrt(sum(S1(:,3).^2,1)/N1)./(sum(abs(X1(:,3)))/N1)*100; % expressed in %
Biomarker.BMo99(iparticipant,isession,1).label = "Walking | Pelvis frontal angle (coefficient of variation)";
Biomarker.BMo99(iparticipant,isession,1).value = CV1;
Biomarker.BMo99(iparticipant,isession,1).mean  = mean(CV1);
Biomarker.BMo99(iparticipant,isession,1).std   = std(CV1);
Biomarker.BMo99(iparticipant,isession,1).units = '%';
N2       = 101;
S2(:,1)  = std(temp2(:,1:fix(end/3)),0,2);
X2(:,1)  = mean(temp2(:,1:fix(end/3)),2);
CV2(:,1) = sqrt(sum(S2(:,1).^2,1)/N2)./(sum(abs(X2(:,1)))/N2)*100; % expressed in %
S2(:,2)  = std(temp2(:,fix(end/3)+1:fix(end/3)*2),0,2);
X2(:,2)  = mean(temp2(:,fix(end/3)+1:fix(end/3)*2),2);
CV2(:,2) = sqrt(sum(S2(:,2).^2,1)/N2)./(sum(abs(X2(:,2)))/N2)*100; % expressed in %
S2(:,3)  = std(temp2(:,fix(end/3)*2+1:end),0,2);
X2(:,3)  = mean(temp2(:,fix(end/3)*2+1:end),2);
CV2(:,3) = sqrt(sum(S2(:,3).^2,1)/N2)./(sum(abs(X2(:,3)))/N2)*100; % expressed in %
Biomarker.BMo99(iparticipant,isession,2).label = "Walking | Pelvis frontal angle (coefficient of variation)";
Biomarker.BMo99(iparticipant,isession,2).value = CV2;
Biomarker.BMo99(iparticipant,isession,2).mean  = mean(CV2);
Biomarker.BMo99(iparticipant,isession,2).std   = std(CV2);
Biomarker.BMo99(iparticipant,isession,2).units = '%';
clear temp1 temp2 S1 S2 X1 X2 N1 N2 CV1 CV2;

% BMo101 /CHECKED
% d450 "Walking"	Walking	Pelvis 	Variability	Pelvis sagittal angle (coefficient of variation)
%disp('  - BMo101');
temp1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
temp2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Segment(5).Euler.rcycle,4) % Right cycles
            value = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,:,icycle),[3,1,2]);
            temp1 = [temp1 abs(value)];
            clear value;
        end
        for icycle = 1:size(Trial(itrial).Segment(5).Euler.lcycle,4) % Left cycles
            value = permute(Trial(itrial).Segment(5).Euler.lcycle(1,3,:,icycle),[3,1,2]);
            temp2 = [temp2 abs(value)];
            clear value;
        end
    end
end 
clear itrial;
% Split cycles into 3 groups to allow reliability assessment
N1       = 101;
S1(:,1)  = std(temp1(:,1:fix(end/3)),0,2);
X1(:,1)  = mean(temp1(:,1:fix(end/3)),2);
CV1(:,1) = sqrt(sum(S1(:,1).^2,1)/N1)./(sum(abs(X1(:,1)))/N1)*100; % expressed in %
S1(:,2)  = std(temp1(:,fix(end/3)+1:fix(end/3)*2),0,2);
X1(:,2)  = mean(temp1(:,fix(end/3)+1:fix(end/3)*2),2);
CV1(:,2) = sqrt(sum(S1(:,2).^2,1)/N1)./(sum(abs(X1(:,2)))/N1)*100; % expressed in %
S1(:,3)  = std(temp1(:,fix(end/3)*2+1:end),0,2);
X1(:,3)  = mean(temp1(:,fix(end/3)*2+1:end),2);
CV1(:,3) = sqrt(sum(S1(:,3).^2,1)/N1)./(sum(abs(X1(:,3)))/N1)*100; % expressed in %
Biomarker.BMo101(iparticipant,isession,1).label = "Walking | Pelvis sagittal angle (coefficient of variation)";
Biomarker.BMo101(iparticipant,isession,1).value = CV1;
Biomarker.BMo101(iparticipant,isession,1).mean  = mean(CV1);
Biomarker.BMo101(iparticipant,isession,1).std   = std(CV1);
Biomarker.BMo101(iparticipant,isession,1).units = '%';
N2       = 101;
S2(:,1)  = std(temp2(:,1:fix(end/3)),0,2);
X2(:,1)  = mean(temp2(:,1:fix(end/3)),2);
CV2(:,1) = sqrt(sum(S2(:,1).^2,1)/N2)./(sum(abs(X2(:,1)))/N2)*100; % expressed in %
S2(:,2)  = std(temp2(:,fix(end/3)+1:fix(end/3)*2),0,2);
X2(:,2)  = mean(temp2(:,fix(end/3)+1:fix(end/3)*2),2);
CV2(:,2) = sqrt(sum(S2(:,2).^2,1)/N2)./(sum(abs(X2(:,2)))/N2)*100; % expressed in %
S2(:,3)  = std(temp2(:,fix(end/3)*2+1:end),0,2);
X2(:,3)  = mean(temp2(:,fix(end/3)*2+1:end),2);
CV2(:,3) = sqrt(sum(S2(:,3).^2,1)/N2)./(sum(abs(X2(:,3)))/N2)*100; % expressed in %
Biomarker.BMo101(iparticipant,isession,2).label = "Walking | Pelvis sagittal angle (coefficient of variation)";
Biomarker.BMo101(iparticipant,isession,2).value = CV2;
Biomarker.BMo101(iparticipant,isession,2).mean  = mean(CV2);
Biomarker.BMo101(iparticipant,isession,2).std   = std(CV2);
Biomarker.BMo101(iparticipant,isession,2).units = '%';
clear temp1 temp2 S1 S2 X1 X2 N1 N2 CV1 CV2;

% BMo103 /CHECKED
% d450 "Walking"	Walking	Pelvis 	Variability	Pelvis transversal angle (coefficient of variation)
%disp('  - BMo103');
temp1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
temp2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Segment(5).Euler.rcycle,4) % Right cycles
            value = permute(Trial(itrial).Segment(5).Euler.rcycle(1,1,:,icycle),[3,1,2]);
            temp1 = [temp1 abs(value)];
            clear value;
        end
        for icycle = 1:size(Trial(itrial).Segment(5).Euler.lcycle,4) % Left cycles
            value = permute(Trial(itrial).Segment(5).Euler.lcycle(1,1,:,icycle),[3,1,2]);
            temp2 = [temp2 abs(value)];
            clear value;
        end
    end
end 
clear itrial;
% Split cycles into 3 groups to allow reliability assessment
N1       = 101;
S1(:,1)  = std(temp1(:,1:fix(end/3)),0,2);
X1(:,1)  = mean(temp1(:,1:fix(end/3)),2);
CV1(:,1) = sqrt(sum(S1(:,1).^2,1)/N1)./(sum(abs(X1(:,1)))/N1)*100; % expressed in %
S1(:,2)  = std(temp1(:,fix(end/3)+1:fix(end/3)*2),0,2);
X1(:,2)  = mean(temp1(:,fix(end/3)+1:fix(end/3)*2),2);
CV1(:,2) = sqrt(sum(S1(:,2).^2,1)/N1)./(sum(abs(X1(:,2)))/N1)*100; % expressed in %
S1(:,3)  = std(temp1(:,fix(end/3)*2+1:end),0,2);
X1(:,3)  = mean(temp1(:,fix(end/3)*2+1:end),2);
CV1(:,3) = sqrt(sum(S1(:,3).^2,1)/N1)./(sum(abs(X1(:,3)))/N1)*100; % expressed in %
Biomarker.BMo103(iparticipant,isession,1).label = "Walking | Pelvis transversal angle (coefficient of variation)";
Biomarker.BMo103(iparticipant,isession,1).value = CV1;
Biomarker.BMo103(iparticipant,isession,1).mean  = mean(CV1);
Biomarker.BMo103(iparticipant,isession,1).std   = std(CV1);
Biomarker.BMo103(iparticipant,isession,1).units = '%';
N2       = 101;
S2(:,1)  = std(temp2(:,1:fix(end/3)),0,2);
X2(:,1)  = mean(temp2(:,1:fix(end/3)),2);
CV2(:,1) = sqrt(sum(S2(:,1).^2,1)/N2)./(sum(abs(X2(:,1)))/N2)*100; % expressed in %
S2(:,2)  = std(temp2(:,fix(end/3)+1:fix(end/3)*2),0,2);
X2(:,2)  = mean(temp2(:,fix(end/3)+1:fix(end/3)*2),2);
CV2(:,2) = sqrt(sum(S2(:,2).^2,1)/N2)./(sum(abs(X2(:,2)))/N2)*100; % expressed in %
S2(:,3)  = std(temp2(:,fix(end/3)*2+1:end),0,2);
X2(:,3)  = mean(temp2(:,fix(end/3)*2+1:end),2);
CV2(:,3) = sqrt(sum(S2(:,3).^2,1)/N2)./(sum(abs(X2(:,3)))/N2)*100; % expressed in %
Biomarker.BMo103(iparticipant,isession,2).label = "Walking | Pelvis transversal angle (coefficient of variation)";
Biomarker.BMo103(iparticipant,isession,2).value = CV2;
Biomarker.BMo103(iparticipant,isession,2).mean  = mean(CV2);
Biomarker.BMo103(iparticipant,isession,2).std   = std(CV2);
Biomarker.BMo103(iparticipant,isession,2).units = '%';
clear temp1 temp2 S1 S2 X1 X2 N1 N2 CV1 CV2;

% BMo105 /MODIFIED
% d450 "Walking"	Walking	Pelvis/leg	Coordination	Pelvis/thigh sagittal deviation phase during stance (mean)
%disp('  - BMo105');
CRP1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
CRP2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Segment(5).Euler.rcycle,4) % Right cycles
            theta1  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,1:60,icycle),[3,1,2]); % Pelvis /Z % 1:60 = during stance (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(4).Euler.rcycle(1,3,1:60,icycle),[3,1,2]); % Thigh /Z % 1:60 = during stance (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP1    = [CRP1 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
        for icycle = 1:size(Trial(itrial).Segment(5).Euler.lcycle,4) % Left cycles
            theta1  = permute(Trial(itrial).Segment(5).Euler.lcycle(1,3,1:60,icycle),[3,1,2]); % Pelvis /Z % 1:60 = during stance (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(9).Euler.lcycle(1,3,1:60,icycle),[3,1,2]); % Thigh /Z % 1:60 = during stance (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP2    = [CRP2 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
    end
end 
clear itrial;
DP1(1) = sum(std(CRP1(:,1:fix(end/3)),0,2)/size(CRP1(:,1:fix(end/3)),2));
DP1(2) = sum(std(CRP1(:,fix(end/3)+1:fix(end/3)*2),0,2)/size(CRP1(:,fix(end/3)+1:fix(end/3)*2),2));
DP1(3) = sum(std(CRP1(:,fix(end/3)*2+1:end),0,2)/size(CRP1(:,fix(end/3)*2+1:end),2));
DP2(1) = sum(std(CRP2(:,1:fix(end/3)),0,2)/size(CRP2(:,1:fix(end/3)),2));
DP2(2) = sum(std(CRP2(:,fix(end/3)+1:fix(end/3)*2),0,2)/size(CRP2(:,fix(end/3)+1:fix(end/3)*2),2));
DP2(3) = sum(std(CRP2(:,fix(end/3)*2+1:end),0,2)/size(CRP2(:,fix(end/3)*2+1:end),2));
Biomarker.BMo105(iparticipant,isession,1).label = "Walking | Pelvis/thigh sagittal deviation phase during stance (mean)";
Biomarker.BMo105(iparticipant,isession,1).value = 180-rad2deg(DP1);
Biomarker.BMo105(iparticipant,isession,1).mean  = mean(180-rad2deg(DP1));
Biomarker.BMo105(iparticipant,isession,1).std   = std(180-rad2deg(DP1));
Biomarker.BMo105(iparticipant,isession,1).units = '�deg';
Biomarker.BMo105(iparticipant,isession,2).label = "Walking | Pelvis/thigh sagittal deviation phase during stance (mean)";
Biomarker.BMo105(iparticipant,isession,2).value = rad2deg(180-DP2);
Biomarker.BMo105(iparticipant,isession,2).mean  = mean(180-rad2deg(DP2));
Biomarker.BMo105(iparticipant,isession,2).std   = std(180-rad2deg(DP2));
Biomarker.BMo105(iparticipant,isession,2).units = '�deg';
clear DP1 DP2;

% BMo106 /MODIFIED
% d450 "Walking"	Walking	Pelvis/leg	Coordination	Pelvis/thigh sagittal deviation phase during swing (mean)
%disp('  - BMo106');
CRP1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
CRP2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Segment(5).Euler.rcycle,4) % Right cycles
            theta1  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,61:101,icycle),[3,1,2]); % Pelvis /Z % 61:101 = during swing (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(4).Euler.rcycle(1,3,61:101,icycle),[3,1,2]); % Thigh /Z % 61:101 = during swing (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP1    = [CRP1 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
        for icycle = 1:size(Trial(itrial).Segment(5).Euler.lcycle,4) % Left cycles
            theta1  = permute(Trial(itrial).Segment(5).Euler.lcycle(1,3,61:101,icycle),[3,1,2]); % Pelvis /Z % 61:101 = during swing (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(9).Euler.lcycle(1,3,61:101,icycle),[3,1,2]); % Thigh /Z % 61:101 = during swing (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP2    = [CRP2 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
    end
end 
clear itrial;
DP1(1) = sum(std(CRP1(:,1:fix(end/3)),0,2)/size(CRP1(:,1:fix(end/3)),2));
DP1(2) = sum(std(CRP1(:,fix(end/3)+1:fix(end/3)*2),0,2)/size(CRP1(:,fix(end/3)+1:fix(end/3)*2),2));
DP1(3) = sum(std(CRP1(:,fix(end/3)*2+1:end),0,2)/size(CRP1(:,fix(end/3)*2+1:end),2));
DP2(1) = sum(std(CRP2(:,1:fix(end/3)),0,2)/size(CRP2(:,1:fix(end/3)),2));
DP2(2) = sum(std(CRP2(:,fix(end/3)+1:fix(end/3)*2),0,2)/size(CRP2(:,fix(end/3)+1:fix(end/3)*2),2));
DP2(3) = sum(std(CRP2(:,fix(end/3)*2+1:end),0,2)/size(CRP2(:,fix(end/3)*2+1:end),2));
Biomarker.BMo106(iparticipant,isession,1).label = "Walking | Pelvis/thigh sagittal deviation phase during swing (mean)";
Biomarker.BMo106(iparticipant,isession,1).value = 180-rad2deg(DP1);
Biomarker.BMo106(iparticipant,isession,1).mean  = mean(180-rad2deg(DP1));
Biomarker.BMo106(iparticipant,isession,1).std   = std(180-rad2deg(DP1));
Biomarker.BMo106(iparticipant,isession,1).units = '�deg';
Biomarker.BMo106(iparticipant,isession,2).label = "Walking | Pelvis/thigh sagittal deviation phase during swing (mean)";
Biomarker.BMo106(iparticipant,isession,2).value = 180-rad2deg(DP2);
Biomarker.BMo106(iparticipant,isession,2).mean  = mean(180-rad2deg(DP2));
Biomarker.BMo106(iparticipant,isession,2).std   = std(180-rad2deg(DP2));
Biomarker.BMo106(iparticipant,isession,2).units = '�deg';
clear DP1 DP2;

% BMo107 /CHECKED
% d450 "Walking"	Walking	Leg	Coordination	Shank/foot sagittal relative phase during swing (mean)
%disp('  - BMo107');
CRP1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
CRP2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Segment(3).Euler.rcycle,4) % Right cycles
            theta1  = permute(Trial(itrial).Segment(3).Euler.rcycle(1,3,61:101,icycle),[3,1,2]); % Shank /Z % 61:101 = during swing (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(2).Euler.rcycle(1,3,61:101,icycle),[3,1,2]); % Foot /Z % 61:101 = during swing (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP1    = [CRP1 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
        for icycle = 1:size(Trial(itrial).Segment(8).Euler.lcycle,4) % Left cycles
            theta1  = permute(Trial(itrial).Segment(8).Euler.lcycle(1,3,61:101,icycle),[3,1,2]); % Shank /Z % 61:101 = during swing (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(7).Euler.lcycle(1,3,61:101,icycle),[3,1,2]); % Foot /Z % 61:101 = during swing (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP2    = [CRP2 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
    end
end 
clear itrial;
MARP1 = sum(abs(CRP1)/size(CRP1,2));
MARP2 = sum(abs(CRP2)/size(CRP2,2));
Biomarker.BMo107(iparticipant,isession,1).label = "Walking | Shank/foot sagittal relative phase during swing (mean)";
Biomarker.BMo107(iparticipant,isession,1).value = rad2deg(MARP1);
Biomarker.BMo107(iparticipant,isession,1).mean  = NaN;
Biomarker.BMo107(iparticipant,isession,1).std   = NaN;
Biomarker.BMo107(iparticipant,isession,1).units = '�deg';
Biomarker.BMo107(iparticipant,isession,2).label = "Walking | Shank/foot sagittal relative phase during swing (mean)";
Biomarker.BMo107(iparticipant,isession,2).value = rad2deg(MARP2);
Biomarker.BMo107(iparticipant,isession,2).mean  = NaN;
Biomarker.BMo107(iparticipant,isession,2).std   = NaN;
Biomarker.BMo107(iparticipant,isession,2).units = '�deg';
clear MARP1 MARP2;

% BMo108 /DELETED (no possible measurement around this axis with the markerset)
% d450 "Walking"	Walking	Thorax/lumbar	Spatial/intensity	Thoracolumbar transversal angle (max)
%disp('  - BMo108');
%temp1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
%temp2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
%for itrial = 1:size(Trial,2)
%    if contains(Trial(itrial).type,'Gait_Normal')
%        for icycle = 1:size(Trial(itrial).Joint(11).Euler.rcycle,4) % Right cycles
%            value = permute(Trial(itrial).Joint(11).Euler.rcycle(1,3,:,icycle),[3,1,2]);
%            temp1 = [temp1 max(value)-min(value)];
%            clear value;
%        end
%        for icycle = 1:size(Trial(itrial).Joint(11).Euler.lcycle,4) % Left cycles
%            value = permute(Trial(itrial).Joint(11).Euler.lcycle(1,3,:,icycle),[3,1,2]);
%            temp2 = [temp2 max(value)-min(value)];
%            clear value;
%        end
%    end
%end 
%clear itrial;
%Biomarker.BMo108(iparticipant,isession,1).label = "Walking | Thoracolumbar transversal angle (max)";
%Biomarker.BMo108(iparticipant,isession,1).value = rad2deg(temp1);
%Biomarker.BMo108(iparticipant,isession,1).mean  = mean(rad2deg(temp1));
%Biomarker.BMo108(iparticipant,isession,1).std   = std(rad2deg(temp1));
%Biomarker.BMo108(iparticipant,isession,1).units = '�deg';
%Biomarker.BMo108(iparticipant,isession,2).label = "Walking | Thoracolumbar transversal angle (max)";
%Biomarker.BMo108(iparticipant,isession,2).value = rad2deg(temp2);
%Biomarker.BMo108(iparticipant,isession,2).mean  = mean(rad2deg(temp2));
%Biomarker.BMo108(iparticipant,isession,2).std   = std(rad2deg(temp2));
%Biomarker.BMo108(iparticipant,isession,2).units = '�deg';
%clear temp1 temp2;

% BMo112 /MODIFIED
% d450 "Walking"	Walking	Thorax/pelvis	Coordination	Thorax/pelvis sagittal deviation phase during stance (mean)
%disp('  - BMo112');
CRP1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
CRP2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.rcycle,4) % Right cycles
            theta1  = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,1:60,icycle),[3,1,2]); % Thorax /Z % 1:60 = during stance (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,1:60,icycle),[3,1,2]); % Pelvis /Z % 1:60 = during stance (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP1    = [CRP1 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.lcycle,4) % Left cycles
            theta1  = permute(Trial(itrial).Segment(12).Euler.lcycle(1,3,1:60,icycle),[3,1,2]); % Thorax /Z % 1:60 = during stance (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(5).Euler.lcycle(1,3,1:60,icycle),[3,1,2]); % Pelvis /Z % 1:60 = during stance (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP2    = [CRP2 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
    end
end 
clear itrial;
DP1(1) = sum(std(CRP1(:,1:fix(end/3)),0,2)/size(CRP1(:,1:fix(end/3)),2));
DP1(2) = sum(std(CRP1(:,fix(end/3)+1:fix(end/3)*2),0,2)/size(CRP1(:,fix(end/3)+1:fix(end/3)*2),2));
DP1(3) = sum(std(CRP1(:,fix(end/3)*2+1:end),0,2)/size(CRP1(:,fix(end/3)*2+1:end),2));
DP2(1) = sum(std(CRP2(:,1:fix(end/3)),0,2)/size(CRP2(:,1:fix(end/3)),2));
DP2(2) = sum(std(CRP2(:,fix(end/3)+1:fix(end/3)*2),0,2)/size(CRP2(:,fix(end/3)+1:fix(end/3)*2),2));
DP2(3) = sum(std(CRP2(:,fix(end/3)*2+1:end),0,2)/size(CRP2(:,fix(end/3)*2+1:end),2));
Biomarker.BMo112(iparticipant,isession,1).label = "Walking | Thorax/pelvis sagittal deviation phase during stance (mean)";
Biomarker.BMo112(iparticipant,isession,1).value = 180-rad2deg(DP1);
Biomarker.BMo112(iparticipant,isession,1).mean  = mean(180-rad2deg(DP1));
Biomarker.BMo112(iparticipant,isession,1).std   = std(180-rad2deg(DP1));
Biomarker.BMo112(iparticipant,isession,1).units = '�deg';
Biomarker.BMo112(iparticipant,isession,2).label = "Walking | Thorax/pelvis sagittal deviation phase during stance (mean)";
Biomarker.BMo112(iparticipant,isession,2).value = 180-rad2deg(DP2);
Biomarker.BMo112(iparticipant,isession,2).mean  = mean(180-rad2deg(DP2));
Biomarker.BMo112(iparticipant,isession,2).std   = std(180-rad2deg(DP2));
Biomarker.BMo112(iparticipant,isession,2).units = '�deg';
clear DP1 DP2;

% BMo113 /MODIFIED
% d450 "Walking"	Walking	Thorax/pelvis	Coordination	Thorax/pelvis sagittal deviation phase during swing (mean)
%disp('  - BMo113');
CRP1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
CRP2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.rcycle,4) % Right cycles
            theta1  = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,61:101,icycle),[3,1,2]); % Thorax /Z % 61:101 = during swing (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,61:101,icycle),[3,1,2]); % Pelvis /Z % 61:101 = during swing (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP1    = [CRP1 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.lcycle,4) % Left cycles
            theta1  = permute(Trial(itrial).Segment(12).Euler.lcycle(1,3,61:101,icycle),[3,1,2]); % Thorax /Z % 61:101 = during swing (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(5).Euler.lcycle(1,3,61:101,icycle),[3,1,2]); % Pelvis /Z % 61:101 = during swing (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP2    = [CRP2 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
    end
end 
clear itrial;
DP1(1) = sum(std(CRP1(:,1:fix(end/3)),0,2)/size(CRP1(:,1:fix(end/3)),2));
DP1(2) = sum(std(CRP1(:,fix(end/3)+1:fix(end/3)*2),0,2)/size(CRP1(:,fix(end/3)+1:fix(end/3)*2),2));
DP1(3) = sum(std(CRP1(:,fix(end/3)*2+1:end),0,2)/size(CRP1(:,fix(end/3)*2+1:end),2));
DP2(1) = sum(std(CRP2(:,1:fix(end/3)),0,2)/size(CRP2(:,1:fix(end/3)),2));
DP2(2) = sum(std(CRP2(:,fix(end/3)+1:fix(end/3)*2),0,2)/size(CRP2(:,fix(end/3)+1:fix(end/3)*2),2));
DP2(3) = sum(std(CRP2(:,fix(end/3)*2+1:end),0,2)/size(CRP2(:,fix(end/3)*2+1:end),2));
Biomarker.BMo113(iparticipant,isession,1).label = "Walking | Thorax/pelvis sagittal deviation phase during swing (mean)";
Biomarker.BMo113(iparticipant,isession,1).value = 180-rad2deg(DP1);
Biomarker.BMo113(iparticipant,isession,1).mean  = mean(180-rad2deg(DP1));
Biomarker.BMo113(iparticipant,isession,1).std   = std(180-rad2deg(DP1));
Biomarker.BMo113(iparticipant,isession,1).units = '�deg';
Biomarker.BMo113(iparticipant,isession,2).label = "Walking | Thorax/pelvis sagittal deviation phase during swing (mean)";
Biomarker.BMo113(iparticipant,isession,2).value = 180-rad2deg(DP2);
Biomarker.BMo113(iparticipant,isession,2).mean  = mean(180-rad2deg(DP2));
Biomarker.BMo113(iparticipant,isession,2).std   = std(180-rad2deg(DP2));
Biomarker.BMo113(iparticipant,isession,2).units = '�deg';
clear DP1 DP2;

% BMo114 /CHECKED
% d450 "Walking"	Walking	Thorax/pelvis	Coordination	Thorax/pelvis sagittal relative phase during stance (mean)
%disp('  - BMo114');
CRP1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
CRP2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.rcycle,4) % Right cycles
            theta1  = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,1:60,icycle),[3,1,2]); % Thorax /Z % 1:60 = during stance (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,1:60,icycle),[3,1,2]); % Pelvis /Z % 1:60 = during stance (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP1    = [CRP1 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.lcycle,4) % Left cycles
            theta1  = permute(Trial(itrial).Segment(12).Euler.lcycle(1,3,1:60,icycle),[3,1,2]); % Thorax /Z % 1:60 = during stance (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(5).Euler.lcycle(1,3,1:60,icycle),[3,1,2]); % Pelvis /Z % 1:60 = during stance (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP2    = [CRP2 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
    end
end 
clear itrial;
MARP1 = sum(abs(CRP1)/size(CRP1,2));
MARP2 = sum(abs(CRP2)/size(CRP2,2));
Biomarker.BMo114(iparticipant,isession,1).label = "Walking | Thorax/pelvis sagittal relative phase during stance (mean)";
Biomarker.BMo114(iparticipant,isession,1).value = rad2deg(MARP1);
Biomarker.BMo114(iparticipant,isession,1).mean  = NaN;
Biomarker.BMo114(iparticipant,isession,1).std   = NaN;
Biomarker.BMo114(iparticipant,isession,1).units = '�deg';
Biomarker.BMo114(iparticipant,isession,2).label = "Walking | Thorax/pelvis sagittal relative phase during stance (mean)";
Biomarker.BMo114(iparticipant,isession,2).value = rad2deg(MARP2);
Biomarker.BMo114(iparticipant,isession,2).mean  = NaN;
Biomarker.BMo114(iparticipant,isession,2).std   = NaN;
Biomarker.BMo114(iparticipant,isession,2).units = '�deg';
clear MARP1 MARP2;

% BMo115 /CHECKED
% d450 "Walking"	Walking	Thorax/pelvis	Coordination	Thorax/pelvis sagittal relative phase during swing (mean)
%disp('  - BMo115');
CRP1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
CRP2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.rcycle,4) % Right cycles
            theta1  = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,61:101,icycle),[3,1,2]); % Thorax /Z % 61:101 = during swing (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(5).Euler.rcycle(1,3,61:101,icycle),[3,1,2]); % Pelvis /Z % 61:101 = during swing (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP1    = [CRP1 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.lcycle,4) % Left cycles
            theta1  = permute(Trial(itrial).Segment(12).Euler.lcycle(1,3,61:101,icycle),[3,1,2]); % Thorax /Z % 61:101 = during swing (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(5).Euler.lcycle(1,3,61:101,icycle),[3,1,2]); % Pelvis /Z % 61:101 = during swing (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP2    = [CRP2 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
    end
end 
clear itrial;
MARP1 = sum(abs(CRP1)/size(CRP1,2));
MARP2 = sum(abs(CRP2)/size(CRP2,2));
Biomarker.BMo115(iparticipant,isession,1).label = "Walking | Thorax/pelvis sagittal relative phase during swing (mean)";
Biomarker.BMo115(iparticipant,isession,1).value = rad2deg(MARP1);
Biomarker.BMo115(iparticipant,isession,1).mean  = NaN;
Biomarker.BMo115(iparticipant,isession,1).std   = NaN;
Biomarker.BMo115(iparticipant,isession,1).units = '�deg';
Biomarker.BMo115(iparticipant,isession,2).label = "Walking | Thorax/pelvis sagittal relative phase during swing (mean)";
Biomarker.BMo115(iparticipant,isession,2).value = rad2deg(MARP2);
Biomarker.BMo115(iparticipant,isession,2).mean  = NaN;
Biomarker.BMo115(iparticipant,isession,2).std   = NaN;
Biomarker.BMo115(iparticipant,isession,2).units = '�deg';
clear MARP1 MARP2;

% BMo118 /CHECKED
% d450 "Walking"	Walking	Leg	Coordination	Thigh/shank sagittal relative phase during swing (mean)
%disp('  - BMo118');
CRP1 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
CRP2 = []; % temp data completed across all cycles of all Gait_Normal trials to increase the number of gait cycles studied
for itrial = 1:size(Trial,2)
    if contains(Trial(itrial).type,'Gait_Normal')
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.rcycle,4) % Right cycles
            theta1  = permute(Trial(itrial).Segment(12).Euler.rcycle(1,3,61:101,icycle),[3,1,2]); % Thorax /Z % 61:101 = during swing (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(3).Euler.rcycle(1,3,61:101,icycle),[3,1,2]); % Shank /Z % 61:101 = during swing (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP1    = [CRP1 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
        for icycle = 1:size(Trial(itrial).Segment(12).Euler.lcycle,4) % Left cycles
            theta1  = permute(Trial(itrial).Segment(12).Euler.lcycle(1,3,61:101,icycle),[3,1,2]); % Thorax /Z % 61:101 = during swing (+/-)
            thetan1 = (2*(theta1-min(theta1)))./(max(theta1)-min(theta1));
            vel1    = gradient(theta1);
            veln1   = vel1./max(norm(vel1));
            phi1    = atan(veln1./thetan1);            
            theta2  = permute(Trial(itrial).Segment(8).Euler.lcycle(1,3,61:101,icycle),[3,1,2]); % Shank /Z % 61:101 = during swing (+/-)
            thetan2 = (2*(theta2-min(theta2)))./(max(theta2)-min(theta2));
            vel2    = gradient(theta2);
            veln2   = vel2./max(norm(vel2));
            phi2    = atan(veln2./thetan2);
            CRP2    = [CRP2 abs(phi1-phi2)];
            clear theta1 thetan1 vel1 veln1 phi1 theta2 thetan2 vel2 veln2 phi2;
        end
    end
end 
clear itrial;
MARP1 = sum(abs(CRP1)/size(CRP1,2));
MARP2 = sum(abs(CRP2)/size(CRP2,2));
Biomarker.BMo118(iparticipant,isession,1).label = "Walking | Thigh/shank sagittal relative phase during swing (mean)";
Biomarker.BMo118(iparticipant,isession,1).value = rad2deg(MARP1);
Biomarker.BMo118(iparticipant,isession,1).mean  = NaN;
Biomarker.BMo118(iparticipant,isession,1).std   = NaN;
Biomarker.BMo118(iparticipant,isession,1).units = '�deg';
Biomarker.BMo118(iparticipant,isession,2).label = "Walking | Thigh/shank sagittal relative phase during swing (mean)";
Biomarker.BMo118(iparticipant,isession,2).value = rad2deg(MARP2);
Biomarker.BMo118(iparticipant,isession,2).mean  = NaN;
Biomarker.BMo118(iparticipant,isession,2).std   = NaN;
Biomarker.BMo118(iparticipant,isession,2).units = '�deg';
clear MARP1 MARP2;

% % TO BE REVISED
% % BMu42
% % d410 "Changing basic body position"	Trunk sagittal bending	Lumbar	Spatial/intensity	Erector spinae (longissimus) EMG signal (max) flexion / maximal flexion ratio
% %disp('  - BMu42');
% for i = 1:size(Trial,2)
%     if contains(Trial(i).type,'Trunk_Forward')        
%         temp1 = [];   
%         temp2 = [];
%         for icycle = 1:size(Trial(i).EMG(5).Signal.rcycle,4)
%             value = Trial(i).EMG(5).Signal.rcycle(:,:,icycle) / ...
%                     mean(Trial(i).EMG(5).Signal.rcycle(end-10:end,:,icycle));
%             temp1 = [temp1 max(value,[],1)];
%             clear value;
%             value = Trial(i).EMG(6).Signal.rcycle(:,:,icycle) / ...
%                     mean(Trial(i).EMG(6).Signal.rcycle(end-10:end,:,icycle));
%             temp2 = [temp2 max(value,[],1)];
%             clear value;
%         end
%         Biomarker.BMu42.value(igroup,iparticipant,isession,1) = mean(temp1);
%         Biomarker.BMu42.value(igroup,iparticipant,isession,2) = mean(temp2);
%         Biomarker.BMu42.units                                 = 'ratio';
%         clear temp1 temp2;
%     end
% end