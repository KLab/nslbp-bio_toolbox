% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : June 2020
% -------------------------------------------------------------------------
% Description  : Initialise the Event structure
% Inputs       : Trial (structure), Event (structure)
% Outputs      : Trial (structure)
% -------------------------------------------------------------------------
% Dependencies : None
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function Trial = InitialiseEvents(Trial,Event)

% Set eventset
eventSet = {'RHS','RTO','LHS','LTO',...
            'General_startMVC','General_stopMVC',...
            'General_startEndurance','General_stopEndurance',...
            'startMotion','stopMotion',...
            'General_startMotion','General_stopMotion'};
        
% Initialise markers
Trial.Event = [];
for i = 1:length(eventSet)
    Trial.Event(i).label = regexprep(eventSet{i},'General_','');
    if isfield(Event,eventSet{i})
        Trial.Event(i).value = round(Event.(eventSet{i})*Trial.fmarker)-Trial.n0+1;
    else
        Trial.Event(i).value = [];
    end
    Trial.Event(i).units = 'frame';
    Trial.Event(i).Processing.detection = 'none';
end     