% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : June 2020
% -------------------------------------------------------------------------
% Description  : Main routine used to launch NSLBP-BIO routines
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% INIT THE WORKSPACE
% -------------------------------------------------------------------------
clearvars;
close all;
warning ('off','all');
clc;

% -------------------------------------------------------------------------
% SET FOLDERS
% -------------------------------------------------------------------------
Folder.toolbox      = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\';
Folder.data         = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO\Data\';
Folder.biomarkers   = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\data\';
Folder.dependencies = [Folder.toolbox,'dependencies\'];
addpath(Folder.toolbox);
addpath(genpath(Folder.dependencies));

% -------------------------------------------------------------------------
% DEFINE PARTICIPANTS
% -------------------------------------------------------------------------
cd(Folder.data);
% Set participant IDs
participantList = {'NSLBP-BIO-001','NSLBP-BIO-002','NSLBP-BIO-003',...
                   'NSLBP-BIO-005','NSLBP-BIO-006','NSLBP-BIO-007',...
                   'NSLBP-BIO-008','NSLBP-BIO-013','NSLBP-BIO-016',...
                   'NSLBP-BIO-021','NSLBP-BIO-022','NSLBP-BIO-023',...
                   'NSLBP-BIO-025','NSLBP-BIO-035','NSLBP-BIO-038',...
                   'NSLBP-BIO-039','NSLBP-BIO-040','NSLBP-BIO-044',...
                   'NSLBP-BIO-046','NSLBP-BIO-048','NSLBP-BIO-049',...
                   'NSLBP-BIO-052','NSLBP-BIO-055','NSLBP-BIO-056',...
                   'NSLBP-BIO-057','NSLBP-BIO-060','NSLBP-BIO-064',...
                   'NSLBP-BIO-066','NSLBP-BIO-067','NSLBP-BIO-068'};    % Controls (n = 30)
% participantList = {'NSLBP-BIO-009','NSLBP-BIO-010','NSLBP-BIO-011',...
%                    'NSLBP-BIO-012','NSLBP-BIO-014','NSLBP-BIO-017',...
%                    'NSLBP-BIO-024','NSLBP-BIO-026','NSLBP-BIO-027',...
%                    'NSLBP-BIO-028','NSLBP-BIO-029','NSLBP-BIO-030',...
%                    'NSLBP-BIO-032','NSLBP-BIO-033','NSLBP-BIO-034',...
%                    'NSLBP-BIO-036','NSLBP-BIO-041','NSLBP-BIO-042',...
%                    'NSLBP-BIO-043','NSLBP-BIO-045','NSLBP-BIO-047',...
%                    'NSLBP-BIO-050','NSLBP-BIO-051','NSLBP-BIO-053',...
%                    'NSLBP-BIO-054','NSLBP-BIO-058','NSLBP-BIO-061',...
%                    'NSLBP-BIO-062','NSLBP-BIO-063','NSLBP-BIO-065'};	% Patients (n = 30)             

% Extract related information
[~,~,temp] = xlsread('NSLBP-BIO-Participants.xlsx');
for iparticipant = 1:size(participantList,2)
    Participant(iparticipant).id = participantList{iparticipant};
    for iparticipant2 = 4:size(temp,1)
        if contains(temp{iparticipant2,2},Participant(iparticipant).id)
            % General information
            if temp{iparticipant2,3} == 0
                Participant(iparticipant).group = 'Patient';
            elseif temp{iparticipant2,3} == 1
                Participant(iparticipant).group = 'Control';
            end
            if temp{iparticipant2,4} == 0
                Participant(iparticipant).gender = 'Female';
            elseif temp{iparticipant2,4} == 1
                Participant(iparticipant).gender = 'Male';
            end
            Participant(iparticipant).inclusionAge = temp{iparticipant2,5}+temp{iparticipant2,6}/12; % yrs
            Participant(iparticipant).height       = temp{iparticipant2,7}*1e-2; %m
            Participant(iparticipant).weight       = temp{iparticipant2,8}; % kg
            % Anthropometric information
            Participant(iparticipant).pelvisWidth  = temp{iparticipant2,9}*1e-2; %m;
            Participant(iparticipant).RlegLength   = temp{iparticipant2,10}*1e-2; %m
            Participant(iparticipant).LlegLength   = temp{iparticipant2,11}*1e-2; %m
            Participant(iparticipant).RkneeWidth   = temp{iparticipant2,12}*1e-2; %m
            Participant(iparticipant).LkneeWidth   = temp{iparticipant2,13}*1e-2; %m
            Participant(iparticipant).RankleWidth  = temp{iparticipant2,14}*1e-2; %m
            Participant(iparticipant).LankleWidth  = temp{iparticipant2,15}*1e-2; %m
        end
    end
end
clear temp iparticipant iparticipant2;

% -------------------------------------------------------------------------
% EXPLORE PARTICIPANTS
% -------------------------------------------------------------------------
for iparticipant = 25:size(Participant,2)
    
    disp(' ');
    disp(['[PARTICIPANT: ',Participant(iparticipant).id,']']);

    % ---------------------------------------------------------------------
    % DEFINE SESSIONS
    % ---------------------------------------------------------------------
    cd([Folder.data,Participant(iparticipant).id])
    temp = dir;
    for isession = 1:size(temp,1)-2 % 1:2 rows in temp are related to "." and ".." folders
        Session(isession).folder       = [temp(isession+2).folder,'\',temp(isession+2).name,'\'];
        Session(isession).date         = temp(isession+2).name(1:8);
        Session(isession).type         = temp(isession+2).name(12:14);
        Session(isession).examiner     = 'FM'; 
        Session(isession).markerHeight = 0.014; % m
    end
    clear temp isession;

    % ---------------------------------------------------------------------
    % EXPLORE SESSIONS
    % ---------------------------------------------------------------------
    for isession = 1:size(Session,2)

        if ~contains(Session(isession).type,'FWP') % Only INI and REL sessions
            disp(' ');
            disp(['[SESSION: ',Session(isession).date,' - ',Session(isession).type,']']);

            % LOAD C3D FILES
            % -----------------------------------------------------------------
            disp('Load C3D files');

            % List all trial types
            trialTypes = {'Static',...
                          'Endurance_Ito','Endurance_Sorensen',...
                          'Gait_Fast','Gait_Normal','Gait_Slow',...
                          'Posture_Standing','Posture_Sitting',...
                          'Perturbation_R_Shoulder','Perturbation_L_Shoulder',...
                          'S2S_Constrained','S2S_Unconstrained',...
                          'Swing_R_Leg','Swing_L_Leg',...
                          'Trunk_Forward','Trunk_Lateral','Trunk_Rotation',...
                          'Weight_Constrained','Weight_Unconstrained',...
                          'sMVC'};

            % Extract data from C3D files
            cd(Session(isession).folder);
            c3dFiles = dir('*.c3d');
            k1       = 1;
            k2       = 1;
            for ifile = 1:size(c3dFiles,1)
                %disp(['  - ',c3dFiles(ifile).name]);
                for itype = 1:size(trialTypes,2)
                    if contains(c3dFiles(ifile).name,trialTypes{itype})
                        if contains(trialTypes{itype},'Static')
                            Static(k1).type    = trialTypes{itype};
                            Static(k1).file    = c3dFiles(ifile).name;
                            Static(k1).btk     = btkReadAcquisition(c3dFiles(ifile).name);
                            Static(k1).n0      = btkGetFirstFrame(Static(k1).btk);
                            Static(k1).n1      = btkGetLastFrame(Static(k1).btk)-Static(k1).n0+1;
                            Static(k1).fmarker = btkGetPointFrequency(Static(k1).btk);
                            Static(k1).fanalog = btkGetAnalogFrequency(Static(k1).btk);
                            k1 = k1+1;
                        else
                            Trial(k2).type    = trialTypes{itype};
                            Trial(k2).file    = c3dFiles(ifile).name;
                            Trial(k2).btk     = btkReadAcquisition(c3dFiles(ifile).name);
                            Trial(k2).n0      = btkGetFirstFrame(Trial(k2).btk);
                            Trial(k2).n1      = btkGetLastFrame(Trial(k2).btk)-Trial(k2).n0+1;
                            Trial(k2).fmarker = btkGetPointFrequency(Trial(k2).btk);
                            Trial(k2).fanalog = btkGetAnalogFrequency(Trial(k2).btk);
                            k2 = k2+1;
                        end
                    end
                end
            end
            clear ifile itype k1 k2 c3dFiles trialTypes;

            % PRE-PROCESS DATA
            % -----------------------------------------------------------------

            % Static data
            disp('Pre-process static data');
            for istatic = 1%:size(Static,2) % For the moment, only one static allowed in the process
                disp(['  - ',Static(istatic).file]);

                % Initialise events (get events from C3D file when available)
                Static(istatic).Event = [];

                % Process marker trajectories
                Marker                  = btkGetMarkers(Static(istatic).btk);
                Static(istatic).Marker  = [];
                Static(istatic).Vmarker = [];
                Static(istatic).Segment = [];
                Static(istatic).Joint   = [];
                Static(istatic)         = InitialiseMarkerTrajectories(Static(istatic),Marker);
                Static(istatic)         = InitialiseVmarkerTrajectories(Static(istatic));
                Static(istatic)         = InitialiseSegments(Static(istatic));
                Static(istatic)         = InitialiseJoints(Static(istatic));
                Static(istatic)         = ProcessMarkerTrajectories([],Static(istatic));
                Static(istatic)         = DefineSegments(Participant(iparticipant),[],Static(istatic));
                clear Marker;

                % Process EMG signals
                Static(istatic).EMG = [];

                % Process forceplate signals
                Static(istatic).GRF = [];

                % Store processed static data in a new C3D file
                mkdir('output');
                cd('output');
                ExportC3D(Static(istatic),[],Participant(iparticipant),Session(isession),Folder);
                cd(Session(isession).folder);
            end
            clear istatic;

            % EMG calibration data
            disp('Pre-process EMG calibration data');
            Calibration(1).type = 'EMG_calibration';
            Calibration(1).EMG  = [];
%             for icalibration = 1:size(Trial,2)
% 
%                 if contains(Trial(icalibration).type,'Endurance') || ...
%                    contains(Trial(icalibration).type,'sMVC') 
% 
%                     disp(['  - ',Trial(icalibration).file]);
% 
%                     % Initialise events (get events from C3D file when available)
%                     Event                     = btkGetEvents(Trial(icalibration).btk);
%                     Trial(icalibration).Event = [];
%                     Trial(icalibration)       = InitialiseEvents(Trial(icalibration),Event);
%                     clear Event;   
% 
%                     % Process marker trajectories
%                     Trial(icalibration).Marker  = [];  
%                     Trial(icalibration).Vmarker = [];  
% 
%                     % Process EMG signals
%                     EMG                               = btkGetAnalogs(Trial(icalibration).btk);
%                     Trial(icalibration).EMG           = [];
%                     [Calibration,Trial(icalibration)] = InitialiseEMGSignals(Calibration,Trial(icalibration),EMG);
%                     fmethod.type                      = 'butterBand4';
%                     fmethod.parameter                 = [10 450];
%                     smethod.type                      = 'butterLow2';
%                     smethod.parameter                 = 3;
%                     nmethod.type                      = 'sMVC';
%                     [Calibration,Trial(icalibration)] = ProcessEMGSignals(Calibration,Trial(icalibration),1,fmethod,smethod,nmethod);
%                     clear EMG fmethod smethod nmethod;
% 
%                     % Process forceplate signals
%                     Trial(icalibration).GRF = [];
% 
%                     % Store processed static data in a new C3D file
%                     cd('output');
%                     ExportC3D(Trial(icalibration),[],Participant(iparticipant),Session(isession),Folder);  
%                     cd(Session(isession).folder);
% 
%                 end
%             end
%             clear icalibration;

            % Trial data
            disp('Pre-process trial data');
            for itrial = 1:size(Trial,2)

                if ~contains(Trial(itrial).type,'sMVC') && ~contains(Trial(itrial).type,'Swing') % Endurance tasks considered as Trial here 

                    disp(['  - ',Trial(itrial).file]);

                    % Initialise events
                    Trial(itrial).Event = [];
                    Event               = btkGetEvents(Trial(itrial).btk);
                    Trial(itrial)       = InitialiseEvents(Trial(itrial),Event);
                    clear Event;   

                    % Process marker trajectories   
                    Trial(itrial).Marker = [];
                    Marker               = btkGetMarkers(Trial(itrial).btk);
                    Trial(itrial)        = InitialiseMarkerTrajectories(Trial(itrial),Marker);        
                    fmethod.type         = 'intercor';
                    fmethod.gapThreshold = [];
                    smethod.type         = 'movmean';
                    smethod.parameter    = 15;        
                    Trial(itrial)        = ProcessMarkerTrajectories(Static,Trial(itrial),fmethod,smethod);   
                    clear Marker fmethod smethod;

                    % Compute segment and joint kinematics
                    Trial(itrial).Vmarker = [];
                    Trial(itrial).Segment = [];
                    Trial(itrial).Joint   = [];
                    Trial(itrial)         = InitialiseVmarkerTrajectories(Trial(itrial));
                    Trial(itrial)         = InitialiseSegments(Trial(itrial));
                    Trial(itrial)         = InitialiseJoints(Trial(itrial));
                    if isempty(strfind(Trial(itrial).type,'Endurance'))
                        Trial(itrial)            = DefineSegments(Participant,Static,Trial(itrial));
                        Trial(itrial)            = ComputeKinematics(Trial(itrial),2,5); % Right lower limb kinematic chain
                        Trial(itrial)            = ComputeKinematics(Trial(itrial),7,10); % Left lower limb kinematic chain
                        Trial(itrial)            = ComputeKinematics(Trial(itrial),10,13); % Pelvis/lumbar/thorax/head
                        Trial(itrial)            = ComputeKinematics(Trial(itrial),14,19); % Pelvis/lower lumbar/upper lumbar/lower thorax/upper thorax/head
                        Trial(itrial).Joint(5)   = Trial(itrial).Joint(10); % Double pelvis/lumbar joint for indices coherence
                        Trial(itrial).Segment(5) = Trial(itrial).Segment(10); % Double pelvis segment for indices coherence
                    end

                    % Process EMG signals
                    Trial(itrial).EMG           = [];
%                     EMG                         = btkGetAnalogs(Trial(itrial).btk);
%                     [~,Trial(itrial)]           = InitialiseEMGSignals([],Trial(itrial),EMG);
%                     fmethod.type                = 'butterBand4';
%                     fmethod.parameter           = [10 450];
%                     smethod.type                = 'butterLow2';
%                     smethod.parameter           = 3;
%                     [Calibration,Trial(itrial)] = ProcessEMGSignals(Calibration,Trial(itrial),0,fmethod,smethod,[]);
%                     clear EMG fmethod smethod;

                    % Process forceplate signals
                    Trial(itrial).GRF      = [];
                    tGRF                   = [];
%                     Trial(itrial).btk      = Correct_FP_C3D_Mokka(Trial(itrial).btk);
%                     tGRF                   = btkGetForcePlatformWrenches(Trial(itrial).btk); % Required for C3D exportation only
%                     GRF                    = btkGetGroundReactionWrenches(Trial(itrial).btk);
%                     GRFmeta                = btkGetMetaData(Trial(itrial).btk,'FORCE_PLATFORM');
%                     Trial(itrial)          = InitialiseGRFSignals(Trial(itrial),GRF,GRFmeta);
%                     fmethod.type           = 'threshold';
%                     fmethod.parameter      = 35;
%                     smethod.type           = 'butterLow2';
%                     smethod.parameter      = 50;
%                     [Trial(itrial),tGRF]   = ProcessGRFSignals(Session(isession),Trial(itrial),GRF,tGRF,fmethod,smethod);
%                     clear GRF GRFmeta fmethod smethod;

                    % Detect events
                    Trial(itrial) = ProcessEvents(Participant(iparticipant),Session(isession),Trial(itrial));

                    % Cut data per cycle
                    Trial(itrial).RCycle = [];
                    Trial(itrial).LCycle = [];
                    Trial(itrial)        = CutCycles(Trial(itrial));

                    % Store processed static data in a new C3D file
                    cd('output');
                    ExportC3D(Trial(itrial),tGRF,Participant(iparticipant),Session(isession),Folder);
                    cd(Session(isession).folder);
                    clear tGRF; 

                end                        
            end
            clear itrial;

            % Store session data
            disp('Save Session data');
            cd(Folder.data);
            save([Participant(iparticipant).id,'_',Session(isession).type,'.mat'],'Participant','Session','Calibration','Static','Trial');
            clear Static Trial;   
        end
        
    end
    clear isession Session Calibration;
end
clear iparticipant;

% -------------------------------------------------------------------------
% COMPUTE BIOMARKERS
% Biomarker dimensions: participant x session x side 
% side: size 1 if central biomarker, size 2 if right/left biomarker
% -------------------------------------------------------------------------
disp('Compute biomarkers');
Biomarker_control = [];
Biomarker_patient = [];
for iparticipant = 1:size(participantList,2)
    disp(['  Participant: ',participantList{iparticipant}]);
    clearvars -except Folder participantList iparticipant isession Biomarker_control Biomarker_patient;
    cd(Folder.data);
    matFiles = dir([participantList{iparticipant},'*.mat']);
    for isession = 1:size(matFiles,1)
        disp(['    - Session: ',num2str(isession)]);
        cd(Folder.data);
        load(matFiles(isession).name);
        if contains(Participant(iparticipant).group,'Control')
            % Load corresponding biomarker dataset
            cd(Folder.biomarkers);
            % Compute biomarkers related to the participant/session
            Biomarker_control = ComputeBiomarkers(Session(isession),Trial,Biomarker_control,Participant(iparticipant).id);
            % Store results
            save('Biomarker_control.mat','Biomarker_control');
        elseif contains(Participant(iparticipant).group,'Patient')
            % Load corresponding biomarker dataset
            cd(Folder.biomarkers);
            % Compute biomarkers related to the participant/session
            Biomarker_patient = ComputeBiomarkers(Session(isession),Trial,Biomarker_patient,Participant(iparticipant).id);
            % Store results
            save('Biomarker_patient.mat','Biomarker_patient');
        end
    end
end
% CheckBiomarkers;
ExportR;
clear Participant Folder;