% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : June 2020
% -------------------------------------------------------------------------
% Description  : Check biomarkers values to detect potential outliers
% -------------------------------------------------------------------------
% Dependencies : None
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% INIT THE WORKSPACE
% -------------------------------------------------------------------------
clearvars;
close all;
warning ('off','all');
clc;

% -------------------------------------------------------------------------
% SET FOLDERS
% -------------------------------------------------------------------------
Folder.toolbox      = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\';
Folder.data         = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO\Data\';
Folder.biomarkers   = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\data\';
Folder.dependencies = [Folder.toolbox,'dependencies\'];
addpath(Folder.toolbox);
addpath(genpath(Folder.dependencies));

% -------------------------------------------------------------------------
% LOAD DATA
% -------------------------------------------------------------------------
cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\data\');
load('Biomarker_control.mat');
load('Biomarker_patient.mat');
reference = xlsread('Biomarkers.xlsx','Reference values','B3:E75');

% -------------------------------------------------------------------------
% MERGE DATA
% -------------------------------------------------------------------------
BMc = [];
BMp = [];
nBM = fieldnames(Biomarker_control);
for iBM = 2:length(nBM)
    Biomarker = Biomarker_control.(nBM{iBM});
    BMc(iBM-1).value = [];
    for i = 1:size(Biomarker,1)
        BMc(iBM-1).label = Biomarker(i,1,1).label;
        for j = 1:size(Biomarker,2)
            for k = 1:size(Biomarker,3)
                BMc(iBM-1).value = [BMc(iBM-1).value Biomarker(i,j,k).value];
            end
        end
    end
    Biomarker = Biomarker_patient.(nBM{iBM});
    BMp(iBM-1).value = [];
    for i = 1:size(Biomarker,1)
        BMp(iBM-1).label = Biomarker(i,1,1).label;
        for j = 1:size(Biomarker,2)
            for k = 1:size(Biomarker,3)
                BMp(iBM-1).value = [BMp(iBM-1).value Biomarker(i,j,k).value];
            end
        end
    end    
end
for iBM = 1:size(BMp,2)
    if size(BMc(iBM).value,2) > size(BMp(iBM).value,2)
        imax(iBM) = size(BMc(iBM).value,2);
    elseif size(BMc(iBM).value,2) < size(BMp(iBM).value,2)
        imax(iBM) = size(BMp(iBM).value,2);
    end
end

% -------------------------------------------------------------------------
% PLOT DATA
% -------------------------------------------------------------------------
vectBM = [];
for iBM = 1:size(BMp,2)
    [~,p(iBM)] = ttest2(BMc(iBM).value,BMp(iBM).value);
    vectBM(iBM).value = nan(2,imax(iBM));
    vectBM(iBM).value(1,1:size(BMc(iBM).value,2)) = BMc(iBM).value;
    vectBM(iBM).value(2,1:size(BMp(iBM).value,2)) = BMp(iBM).value;
    figure(iBM);
    title([[nBM{iBM+1}],BMc(iBM).label,['(p = ',num2str(round(p(iBM),4)),')']]);
    xticklabels({'Controls','Patients'});
    notBoxPlot(vectBM(iBM).value','style','patch');
    hold on;
    plot(0.5,reference(iBM,1),'Marker','.','Markersize',15,'Color','blue');
    line([0.5 0.5],[reference(iBM,1)-reference(iBM,2) reference(iBM,1)+reference(iBM,2)],'Color','blue','Linewidth',1.5);
    plot(2.5,reference(iBM,3),'Marker','.','Markersize',15,'Color','red');
    line([2.5 2.5],[reference(iBM,3)-reference(iBM,4) reference(iBM,3)+reference(iBM,4)],'Color','red','Linewidth',1.5);
end