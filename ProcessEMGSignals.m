% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : June 2020
% -------------------------------------------------------------------------
% Description  : Process EMG signals
% Inputs       : Calibration (structure), Trial (structure), defCal (1x1),
%                fmethod (structure), smethod (structure), nmethod (structure)
% Outputs      : Calibration (structure), Trial (structure)
% -------------------------------------------------------------------------
% Dependencies : None
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function [Calibration,Trial] = ProcessEMGSignals(Calibration,Trial,defCal,fmethod,smethod,nmethod)

for i = 1:size(Trial.EMG,2)
    if ~isempty(Trial.EMG(i).Signal.raw)
        
        % -----------------------------------------------------------------
        % REPLACE NANs BY ZEROs
        % -----------------------------------------------------------------   
        Trial.EMG(i).Signal.filt                                  = Trial.EMG(i).Signal.raw;
        Trial.EMG(i).Signal.filt(isnan(Trial.EMG(i).Signal.filt)) = 0;

        % -----------------------------------------------------------------
        % ZEROING AND FILTER EMG SIGNAL
        % -----------------------------------------------------------------
        Trial.EMG(i).Signal.filt = Trial.EMG(i).Signal.filt-mean(Trial.EMG(i).Signal.filt,3,'omitnan');
        
        % Method 1: No filtering
        if strcmp(fmethod.type,'none')
            Trial.EMG(i).Signal.filt     = Trial.EMG(i).Signal.filt;
            Trial.EMG(i).Processing.filt = fmethod.type;
        
        % Method 2: Band pass filter (Butterworth 4nd order, [fmethod.parameter fmethod.parameter] Hz)
        elseif strcmp(fmethod.type,'butterBand4')
            [B,A]                        = butter(2,[fmethod.parameter(1) fmethod.parameter(2)]./(Trial.fanalog/2),'bandpass');
            Trial.EMG(i).Signal.filt     = permute(filtfilt(B,A,permute(Trial.EMG(i).Signal.filt,[3,1,2])),[2,3,1]);
            Trial.EMG(i).Processing.filt = fmethod.type;
        end

        % -----------------------------------------------------------------
        % RECTIFY EMG SIGNAL
        % -----------------------------------------------------------------
        Trial.EMG(i).Signal.rect = abs(Trial.EMG(i).Signal.filt);
        
        % -----------------------------------------------------------------
        % SMOOTH EMG SIGNAL
        % -----------------------------------------------------------------        
        % Method 1: No smoothing
        if strcmp(smethod.type,'none')
            Trial.EMG(i).Signal.smooth     = Trial.EMG(i).Signal.rect;
            Trial.EMG(i).Processing.smooth = smethod.type;
        
        % Method 2: Low pass filter (Butterworth 2nd order, [smethod.parameter] Hz)
        elseif strcmp(smethod.type,'butterLow2')
            [B,A]                          = butter(1,smethod.parameter/(Trial.fanalog/2),'low');
            Trial.EMG(i).Signal.smooth     = permute(filtfilt(B,A,permute(Trial.EMG(i).Signal.rect,[3,1,2])),[2,3,1]);
            Trial.EMG(i).Processing.smooth = smethod.type;
        
        % Method 3: Moving average (window of [smethod.parameter] frames)
        elseif strcmp(smethod.type,'movmean')
            Trial.EMG(i).Signal.smooth = permute(smoothdata(permute(Trial.EMG(i).Signal.rect,[3,1,2]),'movmean',smethod.parameter),[2,3,1]);
            Trial.EMG(i).Processing.smooth = 'movmean';
        
        % Method 4: Moving average (window of [smethod.parameter] frames)
        elseif strcmp(smethod.type,'movmedian')
            Trial.EMG(i).Signal.smooth = permute(smoothdata(permute(Trial.EMG(i).Signal.rect,[3,1,2]),'movmedian',smethod.parameter),[2,3,1]);
            Trial.EMG(i).Processing.smooth = 'movmedian';
        
        % Method 5: Signal root mean square (RMS) (window of [smethod.parameter] frames)
        elseif strcmp(smethod.type,'rms')
            Trial.EMG(i).Signal.smooth     = permute(envelope(permute(Trial.EMG(i).Signal.filt,[3,1,2]),smethod.parameter,'rms'),[2,3,1]);
            Trial.EMG(i).Processing.smooth = 'rms';
        end
        
        % -----------------------------------------------------------------
        % APPLY SIGNAL CALIBRATION
        % -----------------------------------------------------------------        
        if defCal ~= 1
            if ~isempty(Calibration.EMG(i).normValue)
                Trial.EMG(i).Signal.norm     = Trial.EMG(i).Signal.smooth./Calibration.EMG(i).normValue;
                Trial.EMG(i).Processing.norm = Calibration.EMG(i).normMethod;
            end
        end

        % -----------------------------------------------------------------
        % KEEP ONLY MEAN VALUES FOR NORMALISATION DATA
        % -----------------------------------------------------------------      
        if defCal == 1
            
            % Method 1: No normalisation
            if strcmp(nmethod.type,'none')
                Calibration.EMG(i).normValue  = 1;
                Calibration.EMG(i).normMethod = nmethod.type;
            
            % Method 2: Mean value during subMVC task
            elseif strcmp(nmethod.type,'sMVC')
                if ~isempty(strfind(Trial.file,Trial.EMG(i).Processing.normTask))  
                    if isempty(Calibration.EMG(i)) % Special need for RF signals, sometimes recorded in 2 conditions. Only the first condition is used here.
                        start                         = Trial.Event(5).value*Trial.fanalog/Trial.fmarker;
                        stop                          = Trial.Event(6).value*Trial.fanalog/Trial.fmarker;
                        Calibration.EMG(i).label      = Trial.EMG(i).label;
                        Calibration.EMG(i).file       = Trial.file;
                        temp = [];
    %                     for j = 1:size(start,2)
    %                         temp = [temp; permute(Trial.EMG(i).Signal.smooth(1,1,start(j):stop(j)),[3,1,2])];
    %                     end
                        if contains(Trial.type,'sMVC')
                            temp = permute(Trial.EMG(i).Signal.smooth(1,1,start(2):stop(2)),[3,1,2]); % Use only the second contraction for normalisation
                        elseif contains(Trial.type,'Endurance')
                            temp = permute(Trial.EMG(i).Signal.smooth(1,1,start(1):stop(1)),[3,1,2]); % Use only the first (only) contraction for normalisation
                        end
                        Calibration.EMG(i).normValue  = mean(temp);
                        Calibration.EMG(i).normMethod = nmethod.type;
                        clear temp;
                    end    
                else
                    Calibration.EMG(i).normValue  = [];
                    Calibration.EMG(i).normMethod = 'no MVC available';                    
                end
            end
            
        end        
    end
end