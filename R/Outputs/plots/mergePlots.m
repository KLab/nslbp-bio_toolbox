close all;
clearvars;
clc;
opengl hardware;
addpath('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\dependencies\export_fig');

% % Rename files
% cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\R\Outputs\plots');
% pngFiles = dir('*.png');
% 
% for ifile = 1:size(pngFiles,1)
%     if contains(pngFiles(ifile).name,'boxplot')
%         if length(pngFiles(ifile).name) == 16
%             pngFiles(ifile).name2 = [pngFiles(ifile).name(1:3),'00',pngFiles(ifile).name(4:end)];
%         elseif length(pngFiles(ifile).name) == 17
%             pngFiles(ifile).name2 = [pngFiles(ifile).name(1:3),'0',pngFiles(ifile).name(4:end)];
%         else
%             pngFiles(ifile).name2 = pngFiles(ifile).name;
%         end
%     end
%     if contains(pngFiles(ifile).name,'ROC')
%         if length(pngFiles(ifile).name) == 12
%             pngFiles(ifile).name2 = [pngFiles(ifile).name(1:3),'00',pngFiles(ifile).name(4:end)];
%         elseif length(pngFiles(ifile).name) == 13
%             pngFiles(ifile).name2 = [pngFiles(ifile).name(1:3),'0',pngFiles(ifile).name(4:end)];
%         else
%             pngFiles(ifile).name2 = pngFiles(ifile).name;
%         end
%     end
%     if contains(pngFiles(ifile).name,'normality')
%         if length(pngFiles(ifile).name) == 18
%             pngFiles(ifile).name2 = [pngFiles(ifile).name(1:3),'00',pngFiles(ifile).name(4:end)];
%         elseif length(pngFiles(ifile).name) == 19
%             pngFiles(ifile).name2 = [pngFiles(ifile).name(1:3),'0',pngFiles(ifile).name(4:end)];
%         else
%             pngFiles(ifile).name2 = pngFiles(ifile).name;
%         end
%     end
%     movefile(['C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\R\Outputs\plots\',pngFiles(ifile).name], ...
%              ['C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\R\Outputs\plots\renamed\',pngFiles(ifile).name2]);
% end

% Sort files
cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\R\Outputs\plots\renamed');
pngFiles = dir('*.png');
inormality = 1;
iboxplot = 1;
iroc = 1;
for ifile = 1:size(pngFiles,1)
    if contains(pngFiles(ifile).name,'normality')
        normalityFiles(inormality) = pngFiles(ifile);
        inormality = inormality+1;
    end
    if contains(pngFiles(ifile).name,'boxplot')
        boxplotFiles(iboxplot) = pngFiles(ifile);
        iboxplot = iboxplot+1;
    end
    if contains(pngFiles(ifile).name,'ROC')
        rocFiles(iroc) = pngFiles(ifile);
        iroc = iroc+1;
    end
end
for ibiomarker = 1:length(normalityFiles)
    if ibiomarker == 1
        sortedFiles = {normalityFiles(ibiomarker).name boxplotFiles(ibiomarker).name rocFiles(ibiomarker).name};
    else
        sortedFiles = [sortedFiles normalityFiles(ibiomarker).name boxplotFiles(ibiomarker).name rocFiles(ibiomarker).name];
    end
end

% Merge files in PDF
for ipage = 1:18
    frames = 12*(ipage-1)+1:12*(ipage-1)+12;
    out = imtile(sortedFiles,'Frames',frames,'GridSize',[4 3],'BackgroundColor','white');
    fig = imshow(out);
    exportgraphics(gca,'merged.pdf','BackgroundColor','none','Resolution',1200,'Append',true);
    close all;
end