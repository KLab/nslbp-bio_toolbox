clearvars;
close all;
clc;

BMList   = {'BMo1','BMo3','BMo4','BMo5','BMo6','BMo7','BMo8','BMo9','BMo10',...
            'BMo12','BMo14','BMo15','BMo17','BMo23','BMo24','BMo25','BMo26',...
            'BMo27','BMo28','BMo29','BMo30','BMo31','BMo33','BMo34','BMo35',...
            'BMo37','BMo38','BMo42','BMo43','BMo44','BMo45','BMo46','BMo48',...
            'BMo49','BMo50','BMo51','BMo53','BMo54','BMo55','BMo57','BMo58',...
            'BMo59','BMo60','BMo61','BMo62','BMo63','BMo72','BMo75','BMo77',...
            'BMo79','BMo80','BMo81','BMo82','BMo83','BMo84','BMo85','BMo86',...
            'BMo87','BMo89','BMo91','BMo98','BMo99','BMo101','BMo103','BMo105',...
            'BMo106','BMo107','BMo108','BMo112','BMo113','BMo114','BMo115','BMo118'};
plotType = {'boxplot','ROC'};

cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\R\Outputs\plots');
pngFiles = dir('*.png');

% Rename files
for iBM = 1:8
    for ifile = 1:size(pngFiles,1)
        if contains(pngFiles(ifile).name,plotType{1})
            pngFiles(ifile).name = ['BMo00',num2str(iBM),'_',plotType{1}];
        elseif contains(pngFiles(ifile).name,plotType{2})
            pngFiles(ifile).name = ['BMo00',num2str(iBM),'_',plotType{1}];
        end
    end
end
for iBM = 9:62
    for ifile = 1:size(pngFiles,1)
        if contains(pngFiles(ifile).name,plotType{1})
            pngFiles(ifile).name = ['BMo0',num2str(iBM),'_',plotType{1}];
        elseif contains(pngFiles(ifile).name,plotType{2})
            pngFiles(ifile).name = ['BMo0',num2str(iBM),'_',plotType{1}];
        end
    end
end

figure;
hold on;
iplot = 1;
for ifile = 1:size(pngFiles,1)
    if contains(pngFiles(ifile).name,plotType{1}) || contains(pngFiles(ifile).name,plotType{2})
        for iBM = 1:size(BMList,2)
            if contains(pngFiles(ifile).name,BMList{iBM})
                pngFiles(ifile).name
    %             subplot(37,6,iplot);
    %             imread(pngFiles(ifile).name,'png');
    %             iplot = iplot+1;
            end
        end
    end
end

