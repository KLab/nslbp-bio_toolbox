clear all
close all
clc
%
% Make list for repeatability
% -> 30 patients
% -> similar age distribution
% -> similar sex distribution
% 
% Need n patients for each age
% Need n patients F & M

[num,txt,~] = xlsread('export Hannouche elective PTH_2.xlsx','YES');

%% 1. Patient Population
% 1.1 - Number of patients
nP = size(num,1);

% 1.2 - Age distribution
% a - 
Age = num(:,end);           % Extract data
Age_min = min(Age);         % Younger Patient
Age_max = max(Age);         % Older Patient
Age_cat = Age_min:Age_max;  % Range of years

% b - Number of patients per year
for i =1:size(Age_cat,2)
    [a,~] = find(Age == Age_cat(i));
    if isempty(a)
        Age_num(i) = 0;
    else
        Age_num(i) = size(a,1);
    end
end

% 1.3 - Sex of patients
Sex = txt(:,8);
nF  = sum(cell2mat(strfind(Sex,'F')));
nM  = sum(cell2mat(strfind(Sex,'M')));
nF + nM - nP;
nF / nP

%% 2. Random selection of patients
pat_list = round(rand(1,30) * size(Age,1));
% Charactarestics
% 2.1 - Age
pat_age = Age(pat_list);
    tmp1 = min(pat_age);
    tmp2 = max(pat_age);
    pat_age_cat = tmp1:tmp2;
for i =1:size(pat_age_cat,2)
    [a,~] = find(pat_age == pat_age_cat(i));
    if isempty(a)
        pat_age_num(i) = 0;
    else
        pat_age_num(i) = size(a,1);
    end
end   
%
pat_sex = Sex(pat_list);
pat_nF  = sum(cell2mat(strfind(pat_sex,'F')));
pat_nF / size(pat_list,2)

% Significant Differences ?
[H,p] = ttest2(pat_age,Age);

% Figure
fonts = 16;
fig = figure; hold on
bar(Age_cat,Age_num)
bar(pat_age_cat,pat_age_num,'r')
legend('Total','Rand')

% Texts
X = 10.5;
Y_top = 24;
% mean sd
text(X,Y_top,['Mean Age Total: ',num2str(round(mean(Age),1)),' (',num2str(round(std(Age),1)),')'])
text(X,Y_top-1,['Mean Age Rand: ',num2str(round(mean(pat_age),1)),' (',num2str(round(std(pat_age),1)),')'])
text(X,Y_top-2,['t-test: p = ',num2str(round(p,3))])
% n female
text(X,Y_top-4,['Female Total: ',num2str(round(nF / nP,2)*100),'%'])
text(X,Y_top-5,['Female Rand: ',num2str(round(pat_nF / size(pat_list,2)*100,1)),'%'])
% Options
fig.Position = [0.1210    0.3002    1.1744    0.4040] * 1000;

%% 3 - Export Patient List
Num_Patients = num(pat_list,1);
csvwrite('Num_Patients.csv',sort(Num_Patients))