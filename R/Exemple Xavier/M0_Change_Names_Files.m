clear all
close all
clc

% Change names of files from EOS to remove special characters and put
% everything in Upper Case
% matlab 

% Folders & files path
folder_raw = 'Data/1_Raw_Data/';            % Raw Name Data
folder_pro = 'Data/2_Pre_Processed_Data/';  % Processed Name Data
op = {'Xavier/';'Pauline/'};                % Operators
recon = {'Recon1/';'Recon2/';'Recon3/'};    % Reconstructions
visits = {'PRE*';'P3M*'};                   % Visits

% Loop on op, recon, visits, files
for i = 1:size(op,1) % Loop on op
    for j = 1:size(recon,1) % Loop on reconstructions
        for k =1:size(visits,1) % Loop on visits
            % List of files (include .xlsx & .numx)
            files = dir([folder_raw,op{i},recon{j},visits{k}]);
            for p = 1:size(files,1) % Loop on files
                % Raw name of file
                name = files(p).name; % Raw Name
                 
                % Remove special characters
                name(strfind(name,'^')) = '_';
                name(strfind(name,' ')) = '_';
                if ~isempty(strfind(name,','))
                    name(strfind(name,',')) = '_';
                end
                
                % Remove # of recon for op 2
                switch i
                    case 2
                    name(4:5) = [];
                end
                
                % Folder of file
                folder = [folder_pro,op{i},recon{j}];
                    
                % Copy file to processed name folder
                copyfile([files(p).folder,'\',files(p).name],[folder,upper(name)])
                    
            end % Loop on files
        end % Loop on visits
    end % Loop on reconstructions
end % Loop on op
