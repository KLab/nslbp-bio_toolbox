clear all
close all
clc

% V2: additional loop on all type of parameters
% V3: patients are P01 to P28

% Compute variability of reconstruction EOS
% 1 - Variability of anatomical points

% Multiple type of variability
% - Intra Operator (3 reconstructions) 
% - Inter Operator (2 operators)

% - On same image done 3 times -> ?
% - Pre / Post surgery -> repeated measure

%% 0- Load Data
folder_input = 'Data/3_M_Output_MAT/';
% Load Parameter EOS XG
load([folder_input,'Param_EOS_XG.mat'])
P_EOS.XG = ParamEOS; clear ParamEOS
% Load 3D Points XG
% load([folder_input,'Points_EOS_XG.mat'])
load([folder_input,'Points_EOS_XG_GT.mat'])
rP.XG = Points; clear Points
% Load Parameter EOS PB
load([folder_input,'Param_EOS_PB.mat'])
P_EOS.PB = ParamEOS; clear ParamEOS
% Load 3D Points PB
% load([folder_input,'Points_EOS_PB.mat'])
load([folder_input,'Points_EOS_PB_GT.mat'])
rP.PB = Points; clear Points

%% 1- Data Preparation
% TO DO -> need to check units

% Loop Operator
operator = {'XG';'PB'};
n_op = size(operator,1);
% Loop Recon
recon = {'Recon1';'Recon2';'Recon3'};
n_rec = size(recon,1);
% Loop Visits
vis = {'PRE';'P3M'};
n_v = size(vis,1);
% Loop Patients
pat = fieldnames(rP.XG.Recon3.P3M);
n_pat = size(pat,1);

% 1.1 - EOS parameters

% 1.1.1. Param Independant Surgery
param_list.IS = {'ContraFemHead_d';'ContraFemOffset';'ContraFemNeckLength';'ContraFemLength';...
                 'ContraCervicoDiaphA';'ContraTorsion_Fem';'ContraK_MecaA';'Contra_HKS'};
folder.IS = 'Data/4_R_Input_CSV/Independent_Surgery/';
% names for plots
out_filename.ContraFemHead_d     = 'Femur_Head_Diameter_Contralat';
out_filename.ContraFemOffset     = 'Femoral_Offset_Contralateral';
out_filename.ContraFemNeckLength = 'Femoral_Neck_Length_Contralateral';
out_filename.ContraFemLength     = 'Femur_Length_Contralateral';
out_filename.ContraCervicoDiaphA = 'Cervico_Diaphyseal_Angle_Contralateral';
out_filename.ContraTorsion_Fem   = 'Femoral_Torsion_Contralateral';
out_filename.ContraK_MecaA       = 'Mechanical_Angle_Contralateral';
out_filename.Contra_HKS          = 'Hip_Knee_Shaft_Angle_Contralateral';
    
% 1.1.2. Param Dependant Surgery
% p_DS = {'HomoFemHead_d';'HomoFemOffset';'HomoFemNeckLength';'HomoFemLength';...
%         'HomoCervicoDiaphA';'HomoTorsion_Fem';'HomoK_MecaA';'Homo_HKS','PelvicIncidence'};
param_list.DS = {'HomoFemOffset';'HomoFemLength';'HomoCervicoDiaphA';'Homo_HKS';'PelvicIncidence'};
folder.DS = 'Data/4_R_Input_CSV/Dependent_Surgery/';
% names for plots
out_filename.HomoFemOffset     = 'Femoral_Offset_Homo';
out_filename.HomoFemLength     = 'Femur_Length_Homolateral';
out_filename.HomoCervicoDiaphA = 'Cervico_Diaph_Angle_Homolateral';
out_filename.Homo_HKS          = 'Hip_Knee_Shaft_Angle_Homolateral';
out_filename.PelvicIncidence   = 'Pelvic_Incidence';

% /!\ 
% - HomoFemHead_d is empty at P3M (can get from numX)
% - HomoFemNeckLength is empty at P3M (recompute?)
% - HomoTorsion_Fem is empty at P3M -> HomoAnteTorsStem
% - HomoMecaA is empty at P3M (recompute?)
% -> Will have only one visit (PRE) for those param: TO DO - need =/= ICC formula

% 1.1.3. Param Only Post Surgery
param_list.PS = {'CupInclination';'CupAnteversion';'CupInclPPA';'CupAntePPA';'HomoAnteTorsStem';'PPAinclination'}; % Post-Surgery
% -> Will have only one visit (P3M)for those param
folder.PS = 'Data/4_R_Input_CSV/Post_Surgery_Only/';
% names for plots
out_filename.CupInclination   = 'Cup_Inclination_wrt_cabin';
out_filename.CupAnteversion   = 'Cup_Anteversion_wrt_cabin';
out_filename.CupInclPPA       = 'Cup_Inclination_wrt_APP';
out_filename.CupAntePPA       = 'Cup_Anteversion_wrt_APP';
out_filename.HomoAnteTorsStem = 'Antetorsion_Stem';
out_filename.PPAinclination   = 'APP_Inclination';

% 1.1.4. Param Dependant on Posture
param_list.DP = {'SacralSlope';'PelvicObliquity';'PelvicAxialRot';'PelvicVersion'};
folder.DP = 'Data/4_R_Input_CSV/Dependent_Posture/';
% names for plots
out_filename.SacralSlope     = 'Sacral_Slope';
out_filename.PelvicObliquity = 'Obliquity_Pelvis';
out_filename.PelvicAxialRot  = 'Axial_Rotation_Pelvis';
out_filename.PelvicVersion   = 'Pelvic_Version';

% 1.1.5. Paran Only Pre Surgery
param_list.PRS = {'HomoFemNeckLength'}; % Pre-Surgery
% -> Will have only one visit (P3M)for those param
folder.PRS = 'Data/4_R_Input_CSV/Pre_Surgery_Only/';
% names for plots
out_filename.HomoFemNeckLength = 'Fem_Neck_Length_Homo_Pre_THA';

% Other - Distances and diameter
out_filename.SSC_PSYM = 'Distance_Centre_Sacral_Slope_to_Pubic_Symp';
out_filename.SSC_HJC  = 'Distance_Centre_Sacral_Slope_to_Contra_Acet';
out_filename.PSYM_HJC = 'Distance_Pubic_Symph_to_Contra_Acet';
out_filename.Contra_Cotyle_d = 'Diameter_Acetabulum_Contralateral';

%% 2. Loop Data Storage - Standard EOS parameters + Lengths
% Out.Operator.Visit.Param(n_patients,recon1:recon3)
% 2.1 - Param list
pl = fieldnames(param_list); 
n_pl = size(pl,1);

% 2.2 - Loop on param list
for L = 1:n_pl % Loop on param list
    for op = 1:n_op % Loop on operator
        for r = 1:n_rec % Loop on reconstruction
            cpt_p = 0;
            for v = 1:n_v % Loop on visits
                cpt = 0;
                for p = 1:n_pat % Loop on patients
                        cpt = cpt + 1;
                        % 1.1 - Patient name
                        Data.(operator{op}).Patient{cpt} = pat{p};
                        % 1.2 - Standard Parameters EOS
                        n_p = size(param_list.(pl{L}),1);
                        if strcmp(pl{L},'PS') && strcmp(vis{v},'PRE') 
                            % PS is only post, no data to extract
                        else % Extract data
                            for i = 1:n_p % Loop on parameters
                                % Parameter
                                prm = param_list.(pl{L}){i}; 
                                % Data
                                value = P_EOS.(operator{op}).(recon{r}).(vis{v}).(pat{p}).(prm);
                                % Store
                                Data.(pl{L}).(operator{op}).(vis{v}).(prm)(cpt,r) = value;
                                clear value
                            end % Loop on parameters
                        end
                        % 1.3 - Specific parameters
                        % 1.3.1 - Independant of surgery
                        if strcmp(pl{L},'IS')
                            % Parameters from points
                            % a. Side of surgery from Cupule*_CentreOfMass
                            f_p3m = fieldnames(rP.(operator{op}).(recon{r}).P3M.(pat{p}).Pelvis);
                            cote = f_p3m{end}(7);
                            % b. Contralateral side
                            switch cote
                                case 'D'
                                    contra = 'G';
                                case 'G'
                                    contra = 'D';
                            end
                            % c. Points
                            pelvis = rP.(operator{op}).(recon{r}).(vis{v}).(pat{p}).Pelvis;
                            HJC  = pelvis.(['Cotyle_',contra])(1:3) + pelvis.Translation;
                            PSYM = pelvis.SYM_PUB_SUP;
                            SSC  = pelvis.Plateau_Sacrum(1,:) + pelvis.Translation;
                            % d. Length
                            Data.(pl{L}).(operator{op}).(vis{v}).SSC_PSYM(cpt,r) = norm(SSC - PSYM);
                            Data.(pl{L}).(operator{op}).(vis{v}).SSC_HJC(cpt,r)  = norm(SSC - HJC);
                            Data.(pl{L}).(operator{op}).(vis{v}).PSYM_HJC(cpt,r) = norm(PSYM - HJC);
                            Data.(pl{L}).(operator{op}).(vis{v}).Contra_Cotyle_d(cpt,r) = pelvis.(['Cotyle_',contra])(4);
                        end
                        % 1.4 - Store patients name
                        Data.(vis{v}).Patients{cpt,1} = [vis{v},'_',pat{p}];
                end % Loop on patients
            end % Loop on visits
        end % Loop on reconstruction
    end % Loop on operator

    % 2 - Write csv 
    % 2.1 - Param csv
    folder_csv = folder.(pl{L});%'Data/CSV/Independant_Surgery/';

    % 2.2 Param loop
    param = fieldnames(Data.(pl{L}).(operator{op}).(vis{v}));
    n_param = size(param,1);

    % 2.3 - Loop
    for i = 1:n_param
        idx = 1;
        csv{1,1} = 'PAT'; % Patient Name
        csv{1,2} = 'VIS'; % Visit
        csv{1,3} = 'OPE'; % Operator
        csv{1,4} = 'MES'; % Measurement / Reconstruction
        csv{1,5} = 'VAL'; % Parameter Value
        % 2.3.1 - Prepare csv
        % Out.Operator.Visit.Param(n_patients,recon1:recon3)
        for op = 1:n_op % Loop on operator
            for r = 1:n_rec % Loop on reconstruction
                cpt_p = 0;
                for v = 1:n_v % Loop on visits
                    cpt = 0;
                    for p = 1:n_pat % Loop on patients
                        % Parameter Value - SSC to PSYM
                            if strcmp(pl{L},'PS') && strcmp(vis{v},'PRE') 
                            % PS is only post, no data to extract
                            elseif strcmp(pl{L},'PRS') && strcmp(vis{v},'P3M')
                            % PRS is only post, no data to extract    
                            else % Extract data
                                cpt = cpt + 1;
                                % 1.5 - Export csv - (for A. Poncet)
                                % a. patient number
                                tmp1 = num2str(p); tmp2 = size(tmp1,2);
                                if tmp2 ==1; pnum = ['P0',tmp1];
                                elseif tmp2 == 2;pnum = ['P',tmp1];end
                                % b - Store in cell
                                idx = idx + 1;
                                csv{idx,1} = pnum; %pat{p};                                    % Patient Name
                                csv{idx,2} = vis{v};                                    % Visit
                                csv{idx,3} = operator{op};                              % Operator
                                csv{idx,4} = recon{r}([1,end]);                                  % Measurement / Reconstruction
                                csv{idx,5} = Data.(pl{L}).(operator{op}).(vis{v}).(param{i})(cpt,r);
                                
                                % DEBUG
                                ConPat{p,1} = pnum;
                                ConPat{p,2} = Data.(operator{op}).Patient{cpt};
                            end
                    end % Loop on patients
                end % Loop on visits
            end % Loop on reconstruction
        end % Loop on operator

        % 2.3.2 - write csv
        tab = cell2table(csv);
        file_path_name = [folder_csv,out_filename.(param{i}),'.csv'];
        writetable(tab,file_path_name,'Delimiter',',','WriteVariableNames',0)
        clear csv tab
    end % Loop on param
end % Loop on Param List

%% 3. Anatomical Points 
% Out.Operator.Visit.Param(n_patients,recon1:recon3)
% 3.1 - Segment list
clear CSV
sl = fieldnames(rP.PB.Recon1.P3M.BAT); 
n_sl = size(pl,1);
xyzr = {'AP','ML','V','R'};
folder_csv = 'Data/4_R_Input_CSV/Position_Radius/';
% 3.2 - Parameters
% 3.2.1 - Pelvis
% a - PRE / POST
    % Cotyle_# + Translation / Radius -> # is D or G (Droite / Gauche)
    % Plateau_Sacrum(3,:)
    % TO DO: Where is Normal to sacrum ? -> Get 3 angles
    % SYM_PUB_SUP
% b - POST
    % Cupule#_CentreOfMass + Radius
    % Cupule#_Orthogonal -> Get 3 angles
    % EI_AS_#
    % # is D or G

% 3.2.2 - L/R Femur
    % Centre -> Femoral Head
    % CentreCondyle# -> # is M or L (Medial / Lateral)

% 3.3 - Extract & Store Data 
idx = 0;
for op = 1:n_op % Loop on operator
    for r = 1:n_rec % Loop on reconstruction
        for v = 1:n_v % Loop on visits
            cpt = 0;
            for p = 1:n_pat % Loop on patients
                    code = [operator{op},'_',recon{r},'_',vis{v},'_',pat{p}]; % Only for debug
                    cpt = cpt + 1;
                    idx = idx + 1;
                    % 3.3.1 - MetaData
                    % a - Patient name
                    Data.(operator{op}).Patient{cpt} = pat{p};
                    % b - Points in one fields
                    pts = rP.(operator{op}).(recon{r}).(vis{v}).(pat{p});
                    % c - Side Surgery
                    f_p3m = fieldnames(rP.(operator{op}).(recon{r}).P3M.(pat{p}).Pelvis);
                    homo = f_p3m{end}(7);
                    % d. Contralateral side
                    switch homo
                        case 'D'
                            contra = 'G';
                            y = -1; z = -1; % For cup angles
                            % Homo / Contra Femur
                            homoF   = pts.R_femur; 
                            contraF = pts.L_femur;
                        case 'G'
                            contra = 'D';
                            y =  1; z = -1;
                            % Homo / Contra Femur
                            homoF   = pts.L_femur;
                            contraF = pts.R_femur;
                    end
                    
                    % 3.3.2 - Position & Radius Data
                    % a - Pelvis pre & post
                    T = pts.Pelvis.Translation;
                    Export.Contra_Cotyle = pts.Pelvis.(['Cotyle_',contra]) + [T 0];
                    Export.Centre_Sacral_Slope = pts.Pelvis.Plateau_Sacrum(1,:) + T; % TO DO: Check which is centre
                    Export.Pubic_Symphisis = pts.Pelvis.SYM_PUB_SUP;

                    % b - Femur
                    % b.1 - HomoLat
                    Export.Homo_Medial_Condyle = homoF.CentreCondyleM;                 % Medial Condyle
                    Export.Homo_Lateral_Condyle = homoF.CentreCondyleL;                 % Lateral Condyle
                    % b.2 - Contralat
                    Export.Contra_Femoral_Head = contraF.Centre;                     % Femoral Head
                    Export.Contra_Medial_Condyle = contraF.CentreCondyleM;             % Medial Condyle
                    Export.Contra_Lateral_Condyle = contraF.CentreCondyleL;             % Lateral Condyle

                    % c - Param PRE only
                    if strcmp(vis{v},'PRE')
                        % Homo Cotyle & Femoral Head
                        Export.Homo_Cotyle = pts.Pelvis.(['Cotyle_',homo]) + [T 0]; 
                        Export.Homo_Femoral_Head = homoF.Centre;
                        % Left and Right Greater Trochanter
                        Export.Pre_THA_Right_Greater_Trochanter = pts.R_femur.GrandTroch_Q;
                        Export.Pre_THA_Left_Greater_Trochanter  = pts.L_femur.GrandTroch_Q;
                    end
                    
                    % d - Param P3M only - Implant Parameters - Contra GT
                    if strcmp(vis{v},'P3M')
                        % d.1 - Cup
                            % Centre of Cup
                            Export.Acetabular_Cup = pts.Pelvis.(['Cupule',homo,'_CenterOfMass']);
                            % Cup Orientation
                            Cup_N = pts.Pelvis.(['Cupule',homo,'_Orthogonal']);
                            % LCS EOS:
                            % X - AP pointing backward
                            % Y - ML pointing to the left
                            % Z - Vertical axis pointing down
                            xz =  Cup_N([1 3]); % Proj on XZ plane
                            yz =  Cup_N([2 3]); % Proj on YZ plane
                            % Cup Inclination - angle in frontal plane 
                            Export.Cup_Inclination = acosd(dot(yz/norm(yz),[y 0]));
                            % Cup Anteversion - angle in sagittal plane
                            Export.Cup_Anteversion = acosd(dot(xz/norm(xz),[0 z]));
                            % Anterior Superior Iliac Spines
                            Export.Right_Ant_Sup_Iliac_Spine = pts.Pelvis.EI_AS_D;
                            Export.Left_Ant_Sup_Iliac_Spine = pts.Pelvis.EI_AS_G;
                            
                        % d.2 - Stem
                            Export.Stem_Femoral_Head = homoF.Centre;
                            
                        % d.3 - Contra GT
                            Export.Post_THA_Contra_Greater_Trochanter = contraF.GrandTroch_Q;
                    else % PRE
%                         % d.3 -  Homo Femoral head
%                         Export.Homo_FH = homoF.Centre;
                    end
                    
                    % Write cell for export csv
                    fEx = fieldnames(Export);
                    n_fEX = size(fEx);
                    for i = 1:n_fEX % Loop on points to export
                        point = Export.(fEx{i});
                        for j = 1:size(point,2)
                            % a. name variable
                            % Do not put X for cup inclination and anteversion
                            test = strcmp(fEx{i},'Cup_Inclination_') ...
                                 + strcmp(fEx{i},'Cup_Anteversion_');
                            if test
                                feature_name = fEx{i};
                            else % Add AP, ML, V or R
                                feature_name = [fEx{i},'_',xyzr{j}];
                            end
                            % a. patient number
                            tmp1 = num2str(p); tmp2 = size(tmp1,2);
                            if tmp2 ==1; pnum = ['P0',tmp1];
                            elseif tmp2 == 2;pnum = ['P',tmp1];end
                            % Store in Cell
                            CSV.(feature_name){idx,1} = pnum;%pat{p};
                            CSV.(feature_name){idx,2} = vis{v};
                            CSV.(feature_name){idx,3} = operator{op};
                            CSV.(feature_name){idx,4} = recon{r}([1,end]);
                            if j == 2 % Take Abs value for Y (ML axis)
                                CSV.(feature_name){idx,5} = abs(point(j));
                            else % no Abs for X (AP axis) and Z (Vert axis)
                                CSV.(feature_name){idx,5} = point(j);
                            end
                        end % Loop on coordinates X, Y, Z and/or R
                    end % Loop on points to export
                    clear Export
            end % Loop on patients
        end % Loop on visits
    end % Loop on reconstruction
end % Loop on operator

% 3.4 - Export CSV
% 3.4.1 - Param loop
f_CSV = fieldnames(CSV);
n_CSV = size(f_CSV,1);
% 3.4.2 - Column Name
csv{1,1} = 'PAT'; % Patient Name
csv{1,2} = 'VIS'; % Visit
csv{1,3} = 'OPE'; % Operator
csv{1,4} = 'MES'; % Measurement / Reconstruction
csv{1,5} = 'VAL'; % Parameter Value
% 3.4.3 - Subfolder CSV
% a - One visit only
oneV = {'Homo_Cotyle','Homo_Femoral_Head','Acetabular_Cup','Cup_Inclination',...
        'Cup_Anteversion','Right_Ant_Sup_Iliac_Spine','Left_Ant_Sup_Iliac_Spine','Stem_Femoral_Head',...
        'Pre_THA_Right_Greater_Trochanter','Pre_THA_Left_Greater_Trochanter','Post_THA_Contra_Greater_Trochanter'};
for i =1:size(oneV,2)
    subF.(oneV{i}) = 'One_Visit/';
end
% b - Two visits
twoV = {'Contra_Cotyle','Centre_Sacral_Slope','Pubic_Symphisis','Homo_Medial_Condyle',...
        'Homo_Lateral_Condyle','Contra_Femoral_Head','Contra_Medial_Condyle','Contra_Lateral_Condyle'};
for i =1:size(twoV,2)
    subF.(twoV{i}) = 'Two_Visit/';
end

% 3.4.4 - Loop to write CSV
for i = 1:n_CSV
    % a. Remove Empty Cells
    % -> for feature only at PRE or P3M, due to index
    tmp1 = CSV.(f_CSV{i});
    tmp1(cellfun('isempty',tmp1(:,1)),:) = [];
    % b. Add Column names
    out_csv = [csv; tmp1];
    % c. Table
    out_T = cell2table(out_csv);
    % d. Write table
        % d.1 - Proper field for output folder
        tmp1 = strfind(f_CSV{i},'_');
        if size(tmp1,2) > 1 % for positions
            subF_f = f_CSV{i}(1:tmp1(end)-1);
        else % For angles of cup
            subF_f = f_CSV{i};
        end
        % d.2 - Write table
    writetable(out_T,[folder_csv,subF.(subF_f),f_CSV{i},'.csv'],'Delimiter',',','WriteVariableNames',0)
    clear tmp1 out_csv out_T
end

% 3.5 - Specific export
% PRE & P3M only
% Put in One Visit
f_out = [folder_csv,'One_Visit/'];
% - Contra Femoral Head & Contra Cotyle
feat = {'Contra_Femoral_Head', 'Contra_Cotyle'}; n_feat = size(feat,2);
vis = {'PRE','P3M'}; n_vis = size(vis,2);
vis_out = {'Pre_THA','Post_THA'};
for i =1:n_feat % Loop on features
    % Index of coordinates of feature
    idx = find(contains(f_CSV,feat{i}));
    n_idx = size(idx,1);
    for j = 1:n_idx % Loop on coordinates
        for k = 1:n_vis % Loop on visits
        % Fields with only selected visit
        idx_vis = find(contains(CSV.(f_CSV{idx(j)})(:,2),vis{k}));
        % Output Cell
        c_out = [csv;CSV.(f_CSV{j})(idx_vis,:)];
        % Output table
        t_out = cell2table(c_out);
        % Write table
        writetable(t_out,[f_out,vis_out{k},'_',f_CSV{idx(j)},'.csv'],'Delimiter',',','WriteVariableNames',0)
        end % Loop on visits
    end % Loop on coordinates
end % Loop on features


