clear all
close all
clc

% Read Data from Numx (& xls) for repeatability study
% V2 - Improved loop point extraction
% V3 - Read in function
% V4 - All auto
% V5 - v2 for read xls, homo/contra instead of right/left

% 0 - Folders & files path
addpath('M_Function/')
folder_raw = 'Data/1_Raw_Data/';                % Raw Name Data
folder_pro = 'Data/2_Pre_Processed_Data/';      % Processed Name Data
folder_out = 'Data//3_M_output_MAT/';
operator   = {'Xavier/','XG';'Pauline/','PB'};  % Operators
recon  = {'Recon1';'Recon2';'Recon3'};          % Reconstructions
visits = {'PRE*';'P3M*'};                       % Visits
%
xls_read = 'on';
numx_read = 'off';

% 1 - Loop for XLSX
switch xls_read
    case 'on'
        tic
        for op = 1:size(operator,1) % Loop on operator
            for j = 1:3 % Loop on reconstructions
                % 1.1 - Folder where input files are stored
                folder_in = [folder_pro,operator{op,1},recon{j},'/'];

                % 1.2 - Input files
                files_xlsx_pre = dir([folder_in,'/PRE*.xlsx']);
                files_xlsx_p3m = dir([folder_in,'/P3M*.xlsx']);
                
                n_files = size(files_xlsx_p3m,1);

                % 1.3 - Loop on xlsx files
                f = waitbar(0, 'Starting');
                for i =1:n_files
                    % a. Name Patient (letters 1,2,4 to remove space for di xx / de xx)
                    patient = upper(files_xlsx_pre(i).name([5 6 8]));

                    % b. Wait bar
                    waitbar(i/n_files, f, sprintf([recon{j},' XLSX - Patient: ',patient], floor(i/n_files*100)));

                    % d. Read xlsx P3M: gets points + side of surgery
                    
                    % /!\ BAT P3M RECON1 PB modified by hand to match reader with no tibia
                    % -> Done in RAW, but to check when updating files
                    
                    file_path_p3m = [files_xlsx_p3m(i).folder,'\',files_xlsx_p3m(i).name];
                    [ParamEOS.(recon{j}).P3M.(patient)] = f_EOS_Read_XLSX_P3M_v3(file_path_p3m);
                    side = ParamEOS.(recon{j}).P3M.(patient).SideTKA;
                    % c. Read xlsx PRE
                    file_path_pre = [files_xlsx_pre(i).folder,'\',files_xlsx_pre(i).name];
                    [ParamEOS.(recon{j}).PRE.(patient)] = f_EOS_Read_XLSX_PRE_v2(file_path_pre,side);

                    % TMP - CHECK FILE NAME
                    tmpC{i,1} = files_xlsx_pre(i).name(1:10);
                    tmpC{i,2} = files_xlsx_p3m(i).name(1:10);
                    % -> MUP and MAS are inversed in P3M and PRE
                    % -> why ? Came from Upper Case / Lower Case
                    % -> Solved
                    % CHECK FILE NAME - TMP
                    
                    clear patient file_path_*
                end % Loop on numx files
            end % Loop on reconstruction
            ParamEOS.Time = toc;
            save([folder_out,'Param_EOS_',operator{op,2}],'ParamEOS')
            clear ParamEOS
        end % Loop on Operator
end

% 2 - Loop for NUMX
switch numx_read
    case 'on'
        for op = 1:size(operator,1)
            tic
            for j = 1:3
                % 2.1 - Folder where input files are stored
                folder_in = [folder_pro,operator{op,1},recon{j},'/'];

                % 2.2 - Input files
                files_numx = dir([folder_in,'/*.numx']);
                n_files = size(files_numx,1);

                % 2.3 - Loop on numx files
                f = waitbar(0, 'Starting');
                for i = 1:n_files
                    % a. Name Patient (letters 1,2,4 to remove space for di xx / de xx)
                    patient = upper(files_numx(i).name([5 6 8]));

                    % b. Visit
                    visit = files_numx(i).name(1:3);

                    % c. Wait bar
                    waitbar(i/n_files, f, sprintf([recon{j},' ',visit,' NUMX - Patient: ',patient], floor(i/n_files*100)));

                    % d. Read numx
                    file_path = [files_numx(i).folder,'\',files_numx(i).name];
%                     [Points.(recon{j}).(visit).(patient)]  = f_EOS_Read_numx_Prims3D_v3(file_path,'off',visit);
                        % Additional param, just for P3M BAT Pauline Recon1
                        tmp1 = [operator{op,2},patient,visit,num2str(j)];
                    [Points.(recon{j}).(visit).(patient)]  = f_EOS_Read_numx_Prims3D_v4(file_path,visit,tmp1);
                    clear patient visit file_path
                end % Loop on numx files
            end % loop on reconstructions

            Points.time = toc;
            save([folder_out,'Points_EOS_',operator{op,2},'_GT'],'Points')%,'ParamEOS')
        end
end