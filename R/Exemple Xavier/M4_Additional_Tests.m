clear all
close all
clc

% Compute mean (SD) distance between condyles
addpath('M_Function')

%% 0- Load Data
folder_input = 'Data/3_M_Output_MAT/';
% Load Parameter EOS XG
load([folder_input,'Param_EOS_XG.mat'])
P_EOS.XG = ParamEOS; clear ParamEOS
% Load 3D Points XG
% load([folder_input,'Points_EOS_XG.mat'])
load([folder_input,'Points_EOS_XG_GT.mat'])
rP.XG = Points; clear Points
% Load Parameter EOS PB
load([folder_input,'Param_EOS_PB.mat'])
P_EOS.PB = ParamEOS; clear ParamEOS
% Load 3D Points PB
% load([folder_input,'Points_EOS_PB.mat'])
load([folder_input,'Points_EOS_PB_GT.mat'])
rP.PB = Points; clear Points

%% 1- Data Preparation
% Loop Operator
operator = {'XG';'PB'};
n_op = size(operator,1);
% Loop Recon
recon = {'Recon1';'Recon2';'Recon3'};
n_rec = size(recon,1);
% Loop Visits
vis = {'PRE';'P3M'};
n_v = size(vis,1);
% Loop Patients
pat = fieldnames(rP.XG.Recon3.P3M);
n_pat = size(pat,1);

% 1.1 - EOS parameters

%% 2. Compute Mean (SD) distance condyles / axial rot
% Out.Operator.Visit.Param(n_patients,recon1:recon3)
% 2.1 - Segment list

% 2.2 - Extract & Store Data 
s = 'LR';
idx = 0;
idx_p = 0;
for op = 1:n_op % Loop on operator
    for r = 1:n_rec % Loop on reconstruction
        for v = 1:n_v % Loop on visits
            cpt = 0;
            for p = 1:n_pat % Loop on patients
                    code = [operator{op},'_',recon{r},'_',vis{v},'_',pat{p}]; % Only for debug
                    cpt = cpt + 1;
                    % 2.2.1 - Patient name
                    Data.(operator{op}).Patient{cpt} = pat{p};
                    % 2.2.2 - Points in one fields
                    F.L = rP.(operator{op}).(recon{r}).(vis{v}).(pat{p}).L_femur;
                    F.R = rP.(operator{op}).(recon{r}).(vis{v}).(pat{p}).R_femur;
                    for i = 1:2
                       idx= idx +1;
                       tmp1 = F.(s(i)).CentreCondyleL; 
                       tmp2 = F.(s(i)).CentreCondyleM;
                       tmp3 = norm(tmp1-tmp2);
                       % store
                       cond(idx) = tmp3;
                    end
                    
                    % 2.2.3 - Pelvic Axial Rot
                    idx_p = idx_p + 1;
                    Pax(idx_p) = P_EOS.(operator{op}).(recon{r}).(vis{v}).(pat{p}).PelvicAxialRot;
                    
            end % Loop on patients
        end % Loop on visits
    end % Loop on reconstruction
end % Loop on operator

%% 3. Test LCS Pelvis
% Sequence of Euler angles for pelvis angles
seq = [2 1 3]; % YXZ - Tilt - Obliquity - Rotation

for op = 1:n_op % Loop on operator
    for r = 1:n_rec % Loop on reconstruction
        for p = 1:n_pat % Loop on patients
            for v = 1:n_v % Loop on visits
                ode = [operator{op},'_',recon{r},'_',vis{v},'_',pat{p}]; % Only for debug
                % 3.1 - Patient name
                Data.(operator{op}).Patient{cpt} = pat{p};

                % 3.2 - Points in one fields
                F.R = rP.(operator{op}).(recon{r}).(vis{v}).(pat{p}).R_femur;   
                F.L = rP.(operator{op}).(recon{r}).(vis{v}).(pat{p}).L_femur;
                PE = rP.(operator{op}).(recon{r}).(vis{v}).(pat{p}).Pelvis;

                % 3.3 - Anat Points
                % a. Side of Surgery
                side = P_EOS.(operator{op}).Recon1.P3M.(pat{p}).SideTKA;
                switch side
                    case 'R'
                        cote   = 'DR';
                        contra = 'GL';
                    case 'L'
                        cote   = 'GL';
                        contra = 'DR';
                end
                
                % b. Contralat Femur & Acet
                CFH = F.(contra(2)).Centre(1:3); % Femoral Head
                CAC = PE.(['Cotyle_',contra(1)])(1:3) + PE.Translation; % Acetabulum 

                % c. HomoLat Femur & Acet
                HFH = F.(cote(2)).Centre(1:3); % Femoral Head
                HAC = PE.(['Cotyle_',cote(1)])(1:3) + PE.Translation; % Acetabulum 
                
                % d. Midpoints 
                mAC = (HAC + CAC) / 2; % MidPoint Acet
                mFH = (HFH + CFH) / 2; % MidPoint Acet
                
                % d. Sacral Slope
                SSC = PE.Plateau_Sacrum(1,1:3) + PE.Translation; % Sacral Slope Centre
                SSR = PE.Plateau_Sacrum(2,1:3) + PE.Translation; % Sacral Slope Right
                SSL = PE.Plateau_Sacrum(3,1:3) + PE.Translation; % Sacral Slope Left
                
                % TO DO: CHECK IDX OF SSR AND SSL
                
                % e. Pubic Symphisis
                PSYM = PE.SYM_PUB_SUP;
                
                % 3.4 - Technical LCS 1 - With Acetabulum
                % Origins
                Ot11 = SSC;
                Ot12 = PSYM;
                Ot13 = CAC;
                % Matrices w.r.t global
                [~,T.t.g_t11] = f_EOS_local_Pelvis_Tech(SSC,PSYM,CAC,Ot11);
                [~,T.t.g_t12] = f_EOS_local_Pelvis_Tech(SSC,PSYM,CAC,Ot12);
                [~,T.t.g_t13] = f_EOS_local_Pelvis_Tech(SSC,PSYM,CAC,Ot13);
                
                % 3.5 - Technical LCS 2 - With Femoral Head
                % origins
                Ot21 = SSC;
                Ot22 = PSYM;
                Ot23 = CFH;
                % Matrices w.r.t global
                [~,T.t.g_t21] = f_EOS_local_Pelvis_Tech(SSC,PSYM,CFH(1:3),Ot21);
                [~,T.t.g_t22] = f_EOS_local_Pelvis_Tech(SSC,PSYM,CFH(1:3),Ot22);
                [~,T.t.g_t23] = f_EOS_local_Pelvis_Tech(SSC,PSYM,CFH(1:3),Ot23);
                
                switch vis{v} % Different computations at PRE or P3M
                % 3.6 - Anat LCS 1 - Only Pre - Acet
                    case 'PRE'
                    % a. LCS With PSYM & HJC
                        % a.1 - HJC is Acetabulum
                        RHJC = PE.Cotyle_D(1:3) + PE.Translation;
                        LHJC = PE.Cotyle_G(1:3) + PE.Translation;
                        % Origins 
                        O11 = PSYM; 
                        O12 = CAC;
                        % Matrices
                        [~, T.a.g_a11] = f_EOS_local_Pelvis_Anat_2(RHJC,LHJC,PSYM,O11);
                        [~, T.a.g_a12] = f_EOS_local_Pelvis_Anat_2(RHJC,LHJC,PSYM,O12);
                        
                        % a.2 - HJC is Femoral Head
                        RFH = F.R.Centre(1:3);
                        LFH = F.L.Centre(1:3);
                        % Origins
                        O21 = PSYM;
                        O22 = CFH;
                        % Matrices w.r.t global
                        [~, T.a.g_a21] = f_EOS_local_Pelvis_Anat_2(RFH,LFH,PSYM,O21);
                        [~, T.a.g_a22] = f_EOS_local_Pelvis_Anat_2(RFH,LFH,PSYM,O22);
                        
                    % b. LCS with SSC & HJC
                        % b.1 - HJC is Acetabulum 
                        % Origins
                        O31 = PSYM;
                        O32 = CAC;
                        % Matrices w.r.t global
                        [~,T.a.g_a31] = f_EOS_local_Pelvis_Anat_1(RHJC,LHJC,SSC,O31);
                        [~,T.a.g_a32] = f_EOS_local_Pelvis_Anat_1(RHJC,LHJC,SSC,O32);
                                               
                        % b.2 - HJC is Femoral Head
                        % Origins
                        O41 = PSYM;
                        O42 = CFH;
                        % Matrices w.r.t global
                        [~,T.a.g_a41] = f_EOS_local_Pelvis_Anat_1(RFH,LFH,SSC,O41);
                        [~,T.a.g_a42] = f_EOS_local_Pelvis_Anat_1(RFH,LFH,SSC,O42);
                        
                    
                    % >> Loops on Anatomical frames <<
                    ft = fieldnames(T.t); nt = size(ft,1);
                    fa = fieldnames(T.a); na = size(fa,1);
                    for t = 1:nt % Loop on Technical
                        % Matrix Technical w.r.t. global
                        Tgt = T.t.(ft{t});
                        Ttg = Tinv_array3(Tgt);
                        
                        for a =1:na % Loop on Anatomical
                            % Matrix Anatomical w.r.t. global
                            Tga = T.a.(fa{a});

                            % d. Angles Anatomical LCS w.r.t. Global
                            [YXZ_ga] = f_t_RotMatCardanAng(Tga,seq);

                            % e. Anatomical w.r.t. technical
                            Tta = Ttg * Tga;
                            % Idx of clusters
                            idx_t = ft{t}(end-2:end);
                            idx_a = fa{a}(end-2:end);
                            % Store Matrices
                            T.ta.([idx_t,'_',idx_a]) = Tta;
                            
                        end % Loop on Anatomical
                        
                    % g. Contralat Acet in Technical PRE
                        tmp1 = [CAC 1]';
                        tmp2 = Ttg * tmp1;
                        CAC_t.(ft{t}) = tmp2(1:3)'; 
                        clear tmp1 tmp2 Tgt Ttg Tga Tta
                    end % Loop on Technical
                    
                % 3.7 - Contralat Acet in Technical P3M
                    case 'P3M'
                        for t = 1:nt % Loop on Technical
                        % a. Matrix Technical w.r.t. global
                        Tgt = T.t.(ft{t});
                        Ttg = Tinv_array3(Tgt);
                        % b. Contralat Acet in Technical P3M
                        tmp1 = [CAC 1]';
                        tmp2 = Ttg * tmp1;
                        CAC_t.(ft{t}) = tmp2(1:3)'; 
                        clear tmp1 tmp2 Tgt Ttg
                        end % Loop on Technical
                end % Switch Visits
            end % Loop on visits
            
            % 3.8 - Compute Angles of All clusters
            % -> a lot of them, do anat and technical first
            % Technical: 6 -> 2 types of axis - 3 origins
            % Anatomical: 8 -> 4 types of axis - 2 origins
            
            % For angles, origins are irrelevant, select only the different
            % types of Axis
            
            % Anat w.r.t Tech: 48 -> 8
            fT = fieldnames(T);
            nT = size(fT,1);
            for i = 1:nT % Loop on Type of T
               ft = fieldnames(T.(fT{i}));
               nt = size(ft);
               for j = 1:nt % Loop on all combinations of this type
                   [tmp1,~] = f_t_RotMatCardanAng(T.(fT{i}).(ft{j}),seq);
                   % NOTE: the selection of number is to avoid storing too
                   % much fields t_a in Ang (48 -> 8) 
                   % -> not very clean but works fine.
                   if strcmp(fT{i},'ta') 
                       Ang.(ft{j}([1:2,4:end-1])) = tmp1 / pi * 180;
                   else
                       Ang.(ft{j}(1:end-1)) = tmp1 / pi * 180;
                   end
               end
            end % Loop on Type of T
            
            % 3.9 - STORE DATA - TO DO !
            
            
            
            
            % 3.8 - Difference between Contralat Acet in Technical PRE & P3M
            % -> No! will look at inter-session variability in R
            
        end % Loop on patients
    end % Loop on reconstruction
end % Loop on operator