function [Rg_p,Tg_p] = f_EOS_local_Pelvis_Tech(SSC,PSYM,CAC,O)

% Axis
z_gp = f_t_Vnorm(SSC - PSYM);
  ytmp = f_t_Vnorm(PSYM - CAC); 
x_gp = f_t_Vnorm(f_t_cross(ytmp,z_gp));
y_gp = f_t_Vnorm(f_t_cross(z_gp,x_gp));

% Matrices
Rg_p = [x_gp' y_gp' z_gp'];
Tg_p = [Rg_p O'; 0 0 0 1];