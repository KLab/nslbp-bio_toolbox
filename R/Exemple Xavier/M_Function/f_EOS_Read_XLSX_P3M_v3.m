function [ParamEOS] = f_EOS_Read_XLSX_P3M_v3(file_path)
% V3 - add switch for tibia / no tibia

% Read parameters in sheet "Avanc�"
[~,~,c] = xlsread(file_path,'Avanc�');

% Parameter from EOS P3M
% - Pelvis
    ParamEOS.PelvicIncidence = c{5,3};
    ParamEOS.SacralSlope     = c{6,3};
    ParamEOS.PelvicVersion   = c{7,3};
    ParamEOS.PelvicObliquity = c{8,3};
%     ParamEOS.PelvicAxialRot  = c{9,3};
    ParamEOS.PelvicAxialRot  = c{9,4};
    ParamEOS.PPAinclination  = c{10,3};
    
    switch c{14,3}
        case '-' % Left TKA
            % - Acetabular Cup - take both sides even if only one side
            ParamEOS.SideTKA = 'L';
            R = 'Contra'; L = 'Homo';
            ParamEOS.CupInclination = c{14,5};
            ParamEOS.CupAnteversion = c{15,5};
            %
            ParamEOS.CupInclPPA = c{14,6};
            ParamEOS.CupAntePPA = c{15,6};
        otherwise % Right TKA
            % - Acetabular Cup - take both sides even if only one side
            ParamEOS.SideTKA = 'R';
            R = 'Homo'; L = 'Contra';
            ParamEOS.CupInclination = c{14,3};
            ParamEOS.CupAnteversion = c{15,3};
            %
            ParamEOS.CupInclPPA = c{14,4};
            ParamEOS.CupAntePPA = c{15,4};
    end
    
    % - Femur
    ParamEOS.([R,'FemHead_d'])     = c{19,3}; ParamEOS.([L,'FemHead_d'])     = c{19,5};
    ParamEOS.([R,'FemOffset'])     = c{20,3}; ParamEOS.([L,'FemOffset'])     = c{20,5};
    ParamEOS.([R,'FemNeckLength']) = c{21,3}; ParamEOS.([L,'FemNeckLength']) = c{21,5};
    ParamEOS.([R,'CervicoDiaphA']) = c{22,3}; ParamEOS.([L,'CervicoDiaphA']) = c{22,5};
    
    % - Torsion
    ParamEOS.([R,'Torsion_Fem'])  = c{26,3}; ParamEOS.([L,'Torsion_Fem'])  = c{26,5};
    ParamEOS.([R,'AnteTorsStem']) = c{27,3}; ParamEOS.([L,'AnteTorsStem']) = c{27,5};
    
    % - Length
    ParamEOS.([R,'FemLength']) = c{31,3}; ParamEOS.([L,'FemLength']) = c{31,5};
    
%% Next param depend whether tibia was included or not
switch size(c,1)
    case 36 % No tibia
        % Knee
        ParamEOS.([R,'K_MecaA']) = c{35,3}; ParamEOS.([L,'K_MecaA']) = c{35,5};
        ParamEOS.([R,'_HKS'])    = c{36,3}; ParamEOS.([L,'_HKS'])    = c{36,5};   
    case 41 % With Tibia
        ParamEOS.([R,'TibLength']) = c{32,3}; ParamEOS.([L,'TibLength']) = c{32,5};
        ParamEOS.([R,'FemFunctL']) = c{33,3}; ParamEOS.([L,'FemFunctL']) = c{33,5};
        ParamEOS.([R,'FemAnatL'])  = c{34,3}; ParamEOS.([L,'FemAnatL'])  = c{34,5};

        % - Knee
        ParamEOS.([R,'K_AA'])    = c{38,3}; ParamEOS.([L,'K_AA'])    = c{38,5};
        ParamEOS.([R,'K_FE'])    = c{39,3}; ParamEOS.([L,'K_FE'])    = c{39,5};
        ParamEOS.([R,'K_MecaA']) = c{40,3}; ParamEOS.([L,'K_MecaA']) = c{40,5};
        ParamEOS.([R,'_HKS'])    = c{41,3}; ParamEOS.([L,'_HKS'])    = c{41,5};
    case 44
        a = file_path
end