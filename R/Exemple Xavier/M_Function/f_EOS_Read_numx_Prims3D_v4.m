function Point = f_EOS_Read_numx_Prims3D_v4(file_path,visit,test)
% Read the Prims3D part in numx file pre or p3m
% P3M - not enough information in Recons -> get points from Prims3D
% V2 - auto read
% V3 - PSYM + ASI
% V4 - Greater Trochanter

% 1 - Load numx file
numx = xml2struct(file_path);

% 2 - Idx for points of interest (from tables at bottom of function)
% 2.1 - Idx Pelvis
idx_P = [2 ... Translation ?
         4 ... Cotyle_G
         6 ... Cotyle_D
         8]; % SacroIliaqueInf -> 3 points of "Plateau sacrum"
% 2.2 - Index Femur
idx_F = [4 6 8 10 12 14 16];
% 2.3 - Index Tibia
idx_T = [4 6];

% 3. Initialisation structure
Point.Pelvis  = [];
Point.L_femur = [];
Point.R_femur = [];
Point.L_tibia = [];
Point.R_tibia = [];

% Name Seg
NS.Bassin = 'Pelvis';
NS.FemurG = 'L_femur';
NS.FemurD = 'R_femur';
NS.TibiaG = 'L_tibia';
NS.TibiaD = 'R_tibia';

%% PRIMS
% Data extraction v2 - take all available data
if strcmp(numx.Children(2).Children(6).Children(2).Attributes.Value,'MI') % Data Prims3D
    % Loop on Children - Segment
    n_seg = size(numx.Children(2).Children(6).Children(2).Children,2);
    for i = 1:n_seg
        if ~strcmp(numx.Children(2).Children(6).Children(2).Children(i).Name,'#text') % not text segment
            type = numx.Children(2).Children(6).Children(2).Children(i).Name;%.Attributes.Value;
            switch type
                % Points Pelvis
                case 'TEMPLATE3D' 
                    seg_name = numx.Children(2).Children(6).Children(2).Children(i).Attributes.Value;
                    seg = NS.(seg_name);
                    for j = idx_P % Loop on points pelvis
                        pt = numx.Children(2).Children(6).Children(2).Children(i).Children(j);
                        type = pt.Name; % Different type, different storage
                        if strcmp(type,'Translation') 
                           tmp1 = str2num(pt.Children.Data);
                           data = tmp1(1:3,4)';
                           name = type; 
                        elseif strcmp(type,'AXSPHERE') % 4 values
                            name = pt.Attributes(4).Value;
                            centre = str2num(pt.Children(4).Children.Data);
                            radius = str2num(pt.Children(4).Attributes(3).Value);
                            data = [centre radius];
                        elseif strcmp(type,'AxCibleFaceBassin') % Param with 3 values
                            name = pt.Children(8).Attributes(4).Value; % Sacral slope
                            centre  = str2num(pt.Children(2).Children.Data);
                            centreL = str2num(pt.Children(4).Children.Data);
                            centreR = str2num(pt.Children(6).Children.Data);
                            data = [centre;centreL;centreR];
                        end
                        % Store Pelvis
                        Point.(seg).(name) = data;                        
                        
                    end % Loop on points pelvis
                case 'AXFEMUR' % Points of femur
                    seg_name = numx.Children(2).Children(6).Children(2).Children(i).Attributes(6).Value;
                    seg = NS.(seg_name);
                    for j = idx_F
                        pt = numx.Children(2).Children(6).Children(2).Children(i).Children(j);
                        name = pt.Name;
                        data = str2num(pt.Children.Data); % Position
                        % Add radius
                        if strcmp(name,'Centre') % Femoral Head
                            r = numx.Children(2).Children(6).Children(2).Children(i).Attributes(7).Value;
                            data(4) = str2num(r);
                        elseif strcmp(name,'CentreCondyleM') % Femoral Condyle Medial
                            r = numx.Children(2).Children(6).Children(2).Children(i).Attributes(9).Value;
                            data(4) = str2num(r);
                        elseif strcmp(name,'CentreCondyleL') % Femoral Condyle Lateral
                            r = numx.Children(2).Children(6).Children(2).Children(i).Attributes(8).Value;
                            data(4) = str2num(r);
                        end
                        % Store femur
                        Point.(seg).(name) = data; 
                    end
                    
                case 'AXTIBIA'
                    seg_name = numx.Children(2).Children(6).Children(2).Children(i).Attributes(5).Value;
                    seg = NS.(seg_name);
                    for j = idx_T
                        pt = numx.Children(2).Children(6).Children(2).Children(i).Children(j);
                        name = pt.Name;
                        data = str2num(pt.Children.Data); % Position
                        % Store tibia
                        Point.(seg).(name) = data;
                    end
            end
        end
    end
end
 
%% Additional Pelvic Points
switch visit
    case 'PRE'
        % - PSYM
        Point.Pelvis.SYM_PUB_SUP = str2num(numx.Children(2).Children(24).Children(2).Children(2).Children(4).Children.Data);
    case 'P3M'
        % a - APP
        % numx.Children(2).Children(2).Children(2).Children(2) -> Bassin
        pt = [77; 78; 91];
        for i =1:size(pt,1)
            idx = pt(i) * 2;
            name = numx.Children(2).Children(2).Children(2).Children(2).Children(idx).Attributes(8).Value;
            data = str2num(numx.Children(2).Children(2).Children(2).Children(2).Children(idx).Children(2).Children(6).Children.Data);
            Point.Pelvis.(name) = data;
        end
        
        % b - Acetabular Cup
        % numx.Children(2).Children(2).Children(2).Children(10).Attributes.Value -> Cupule
        
        % b.1 - Loop on Children to find acetabular cup index
        n_c = size(numx.Children(2).Children(2).Children(2).Children,2);
        for i =2:2:n_c
            tmp = numx.Children(2).Children(2).Children(2).Children(i).Attributes.Value;
            if strcmp(tmp(1:3),'Cup')
                idx_c = i;
            end
        end
        % b.2 - Extract points
        Side_Cup  = numx.Children(2).Children(2).Children(2).Children(idx_c).Children(2).Attributes(3).Value;
        % - Centre of mass
        name   = numx.Children(2).Children(2).Children(2).Children(idx_c).Children(2).Children(2).Name;
        radius = str2num(numx.Children(2).Children(2).Children(2).Children(idx_c).Children(2).Attributes(4).Value);
        data   = str2num(numx.Children(2).Children(2).Children(2).Children(idx_c).Children(2).Children(2).Children.Data);
        Point.Pelvis.([Side_Cup,'_',name]) = [data radius];
        clear name radius data
        
        % - Orthogonal -> Normal
        name   =         numx.Children(2).Children(2).Children(2).Children(idx_c).Children(2).Children(4).Name;
        data   = str2num(numx.Children(2).Children(2).Children(2).Children(idx_c).Children(2).Children(4).Children.Data);
        Point.Pelvis.([Side_Cup,'_',name]) = data;
end

%% Greater Trochanter
% TO FINISH
% Make case for PRE & P3M
% In P3M need to select contralat to get troch.
% -> Use SideCup
recons = numx.Children(2).Children(2).Children(2);
n_r  = size(recons.Children,2);
switch visit
    case 'PRE'
        for i = 2:2:n_r
            idxGT = [];
            LF = strcmp(recons.Children(i).Attributes.Value,'FemurG');
            RF = strcmp(recons.Children(i).Attributes.Value,'FemurD');
            % LF and RF look to have same code - regroup?
            if LF 
                if sum(contains({recons.Children(i).Children(4).Attributes.Value},'GrandTroch'))
                    idxName = find(contains({recons.Children(i).Children(4).Attributes.Value},'GrandTroch'));
                    name = [recons.Children(i).Children(4).Attributes(idxName).Value,...
                            '_',recons.Children(i).Children(4).Children(2).Children(6).Name];
                    data = str2num(recons.Children(i).Children(4).Children(2).Children(6).Children.Data);                    Point.L_femur.(name) = data;
                else % Look for GrandTroch
                    Point.L_femur.(name) = 'TO_DO';
                end
            elseif RF
                if sum(contains({recons.Children(i).Children(4).Attributes.Value},'GrandTroch'))
                    idxName = find(contains({recons.Children(i).Children(4).Attributes.Value},'GrandTroch'));
                    name = [recons.Children(i).Children(4).Attributes(idxName).Value,...
                            '_',recons.Children(i).Children(4).Children(2).Children(6).Name];
                    data = str2num(recons.Children(i).Children(4).Children(2).Children(6).Children.Data);
                    Point.R_femur.(name) = data;
                else % Look for GrandTroch
                    Point.R_femur.(name) = 'TO_DO';
                end
            end
        end
        % Left Greater Troch - Left Femur idx = 4
%         name = [numx.Children(2).Children(2).Children(2).Children(4).Children(4).Attributes(8).Value,...
%                 '_',numx.Children(2).Children(2).Children(2).Children(4).Children(4).Children(2).Children(6).Name];
%         data = str2num(numx.Children(2).Children(2).Children(2).Children(4).Children(4).Children(2).Children(6).Children.Data);
%         Point.L_femur.(name) = data;
%         % Right Greater Troch - Right Femur idx = 10
%         name = [numx.Children(2).Children(2).Children(2).Children(10).Children(4).Attributes(8).Value,...
%                 '_',numx.Children(2).Children(2).Children(2).Children(10).Children(4).Children(2).Children(6).Name];
%         data = str2num(numx.Children(2).Children(2).Children(2).Children(10).Children(4).Children(2).Children(6).Children.Data);
%         Point.R_femur.(name) = data;
    case 'P3M'
        % Only Contralat
        switch Side_Cup
            case 'CupuleD' % Right THA -> Greater Troch Left
                if strcmp(test,'PBBATP3M1') % PB BAT P3M Recon1 
                    % patient 1 Pauline
                    name = [numx.Children(2).Children(2).Children(2).Children(4).Children(4).Attributes(8).Value,...
                        '_',numx.Children(2).Children(2).Children(2).Children(4).Children(4).Children(2).Children(6).Name];
                    data = str2num(numx.Children(2).Children(2).Children(2).Children(4).Children(4).Children(2).Children(6).Children.Data);
                    Point.L_femur.(name) = data;
                else
                % Left Femur idx = 10 if Cup is right
                name = [numx.Children(2).Children(2).Children(2).Children(10).Children(4).Attributes(8).Value,...
                        '_',numx.Children(2).Children(2).Children(2).Children(10).Children(4).Children(2).Children(6).Name];
                data = str2num(numx.Children(2).Children(2).Children(2).Children(10).Children(4).Children(2).Children(6).Children.Data);
                Point.L_femur.(name) = data;
                end
            case 'CupuleG' % Left  THA -> Greater Troch Right
                % Right Femur idx = 10 if Cup if left
                name = [numx.Children(2).Children(2).Children(2).Children(10).Children(4).Attributes(8).Value,...
                        '_',numx.Children(2).Children(2).Children(2).Children(10).Children(4).Children(2).Children(6).Name];
                data = str2num(numx.Children(2).Children(2).Children(2).Children(10).Children(4).Children(2).Children(6).Children.Data);
                Point.R_femur.(name) = data;
        end
            
end


% Pileprimitive.Recons.
% P3M
% .Pileprimitive.Recons.FemurD
% -> If P3M only greater troch on contra
% numx.Children(2).Children(2).Children(2).Children(8)

%% Tables of connection
% % MAP:
% Prims3D = numx.Children(2).Children(6);
% MI = numx.Children(2).Children(6).Children(2)
%     % MI.Children(2)  -> Bassin / TEMPLATE3D
%     % MI.Children(4)  -> FemurG / AXFEMUR
%     % MI.Children(6)  -> TibiaG / AXTIBIA
%     % MI.Children(8)  -> FemurD / AXFEMUR
%     % MI.Children(10) -> TibiaD / AXTIBIA
% 
% for i =1:size(MI.Children(2).Children,2)
%     table_bassin_MI{i,1} = i;
%     table_bassin_MI{i,2} = MI.Children(2).Children(i).Name;
% end
% 
% for i =1:size(MI.Children(4).Children,2)
%     table_AXFEMUR_MI{i,1} = i;
%     table_AXFEMUR_MI{i,2} = MI.Children(4).Children(i).Name;
% end
% 
% for i =1:size(MI.Children(6).Children,2)
%     table_AXTIBIA_MI{i,1} = i;
%     table_AXTIBIA_MI{i,2} = MI.Children(6).Children(i).Name;
% end