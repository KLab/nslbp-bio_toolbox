function ParamEOS = f_EOS_Read_XLSX_PRE_v2(file_path,side)
% Side surgery
switch side
    case 'R'
        R = 'Homo'; L = 'Contra';
    case 'L'
        R = 'Contra'; L = 'Homo';
end

% Read parameters in sheet "Avanc�"
[~,~,c] = xlsread(file_path,'Avanc�');
% Parameter from EOS PRE
% - Pelvis
ParamEOS.PelvicIncidence = c{5,3};
ParamEOS.SacralSlope     = c{6,3};
ParamEOS.PelvicVersion   = c{7,3};
ParamEOS.PelvicObliquity = c{8,3};
% ParamEOS.PelvicAxialRot  = c{9,3};
ParamEOS.PelvicAxialRot  = c{9,4};
% - Femur
ParamEOS.([R,'FemHead_d'])     = c{13,3}; ParamEOS.([L,'FemHead_d'])     = c{13,5};
ParamEOS.([R,'FemOffset'])     = c{14,3}; ParamEOS.([L,'FemOffset'])     = c{14,5};
ParamEOS.([R,'FemNeckLength']) = c{15,3}; ParamEOS.([L,'FemNeckLength']) = c{15,5};
ParamEOS.([R,'CervicoDiaphA']) = c{16,3}; ParamEOS.([L,'CervicoDiaphA']) = c{16,5};
% - Length
ParamEOS.([R,'FemLength']) = c{20,3}; ParamEOS.([L,'FemLength']) = c{20,5};
ParamEOS.([R,'TibLength']) = c{21,3}; ParamEOS.([L,'TibLength']) = c{21,5};
ParamEOS.([R,'FemFunctL']) = c{22,3}; ParamEOS.([L,'FemFunctL']) = c{22,5};
ParamEOS.([R,'FemAnatL'])  = c{23,3}; ParamEOS.([L,'FemAnatL'])  = c{23,5};
% - Knee
ParamEOS.([R,'K_AA'])    = c{27,3}; ParamEOS.([L,'K_AA'])    = c{27,5};
ParamEOS.([R,'K_FE'])    = c{28,3}; ParamEOS.([L,'K_FE'])    = c{28,5};
ParamEOS.([R,'K_MecaA']) = c{29,3}; ParamEOS.([L,'K_MecaA']) = c{29,5};
ParamEOS.([R,'T_MecaA']) = c{30,3}; ParamEOS.([L,'T_MecaA']) = c{30,5};
ParamEOS.([R,'_HKS'])    = c{31,3}; ParamEOS.([L,'_HKS'])    = c{31,5};
% - Torsion
ParamEOS.([R,'Torsion_Fem']) = c{35,3}; ParamEOS.([L,'Torsion_Fem']) = c{35,5};
ParamEOS.([R,'Torsion_Tib']) = c{36,3}; ParamEOS.([L,'Torsion_Tib']) = c{36,5};
ParamEOS.([R,'Torsion_FT'])  = c{37,3}; ParamEOS.([L,'Torsion_FT'])  = c{37,5};