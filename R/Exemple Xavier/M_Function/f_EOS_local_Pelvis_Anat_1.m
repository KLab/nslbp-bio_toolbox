function [Rg_p, Tg_p] = f_EOS_local_Pelvis_Anat_1(RHJC,LHJC,SSC,O)
% Based on HJC & SSC
% Mean HJC
mHJC = (RHJC + LHJC) / 2;
% Axis
y_gp = f_t_Vnorm(RHJC - LHJC); % Left to Right;
  ztmp = SSC - mHJC;
x_gp = f_t_Vnorm(f_t_cross(y_gp,ztmp));
z_gp = f_t_Vnorm(f_t_cross(x_gp,y_gp));
% Matrices
Rg_p = [x_gp' y_gp' z_gp'];
Tg_p = [Rg_p O'; 0 0 0 1];
