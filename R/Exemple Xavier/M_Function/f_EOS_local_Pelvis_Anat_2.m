function [Rg_p, Tg_p] = f_EOS_local_Pelvis_Anat_2(RHJC,LHJC,PSYM,O)
% Based on HJC & PSYM
% Mean HJC
mHJC = (RHJC + LHJC) / 2;
% LCS with Acetabulum & mean Acetabulum
% axis 
y_gp = f_t_Vnorm(RHJC - LHJC);% Left to Right
   xtmp = PSYM - mHJC;
z_gp = f_t_Vnorm(f_t_cross(xtmp,y_gp));
x_gp = f_t_Vnorm(f_t_cross(y_gp,z_gp));
% matrices
Rg_p = [x_gp' y_gp' z_gp'];
Tg_p = [Rg_p O'; 0 0 0 1];
