clear all
close all
clc

% Get age, weight, BMI of database and of population in reliability
% - Read data 
% - Select appropriate data between multiple xlsx
% - Write csv for comparison of populations in R
folder_in  = 'Data/0_Data_Population/Input_XLS/';
folder_out = 'Data/0_Data_Population/Output_CSV/';
file_database = '439_patients_Age_Height_Weight_Sex_Side.xlsx';
file_retrospective = '250 patients 26.10.2021_Weight_ETC.xlsx';
file_reliability = 'PAT_Reliability';

%% 1 - Main Database
% 1.1 - Read input file
[num_d,~,cell_d] = xlsread([folder_in,file_database]);

% 1.2 - Extract data
database = table;
% a. ID, Age, Weight, Height
database.PAT_ID = num_d(:,1);
database.Age = num_d(:,3);
database.Weight = num_d(:,5);
database.Height = num_d(:,6);
% b. Side & Sex
side = cell_d(2:end,7);
sex  = cell_d(2:end,4);
for i = 1:size(side,1)
     % c. Sex
    switch sex{i}
        case 'F'
            database.Sex(i,1) = 'F';
        case 'M'
            database.Sex(i,1) = 'M';
        otherwise
            % Isempty
            database.Sex(i,1) = '-';
    end
    % d. Side
    switch side{i}
        case 'Gauche'
            database.SideOA(i,1) = 'L';
        case 'Droite'
            database.SideOA(i,1) = 'R';
        otherwise
            % Isempty
            database.SideOA(i,1) = '-';
    end
   
end

% 1.3 - Output CSV
writetable(database,[folder_out,'Pat_Database.csv'])

%% 2 - Database retrospective study
% 2.1 - Read input file
[num_ret,~,~] = xlsread([folder_in,file_retrospective]);
retro = table; % define as table, can be used as ~struct
retro.PAT_ID = num_ret(:,1);

% 2.2 - Data extraction
for i = 1:size(retro.PAT_ID)
    % Index of patient in database
    idx = find(database.PAT_ID == retro.PAT_ID(i));
    % Extract Data
    retro.Age(i,1)    = database.Age(idx);
    retro.Weight(i,1) = database.Weight(idx);
    retro.Height(i,1) = database.Height(idx);
    retro.Sex(i,1)    = database.Sex(idx);
    retro.SideOA(i,1) = database.SideOA(idx);
end

% 2.3 - Output CSV
writetable(retro,[folder_out,'Pat_Retrospective.csv'])

%% 3 - Database Reliability
% 3.1 - Read input file
[num_rel,~,~] = xlsread([folder_in,file_reliability]);
rel = table;
rel.PAT_ID = num_rel(:,1);

% 3.2 - Data extraction
for i = 1:size(rel.PAT_ID)
    % Index of patient in database
    idx = find(database.PAT_ID == rel.PAT_ID(i));
    % Extract Data
    rel.Age(i,1)    = database.Age(idx);
    rel.Weight(i,1) = database.Weight(idx);
    rel.Height(i,1) = database.Height(idx);
    rel.Sex(i,1)    = database.Sex(idx);
    rel.SideOA(i,1) = database.SideOA(idx);
end

% 3.3 - Output CSV
writetable(rel,[folder_out,'Pat_Reliability.csv'])