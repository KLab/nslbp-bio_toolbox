clear all
close all
clc

% plot in 3D the points measured by both operators for each patients
% used to identify potential outliers

% Plot Pelvis, R_Femur & L_Femur
% -> only points in table
% PRE
    % Pelvis
    % - Pubic Symphysis
    % - Centre Sacral Slope
    % - Acetabulum R / L
    % Femur
    % - Femoral Head 
    % - GT
    % - Med/Lat Condyles

 % P3M 
    % Pelvis
    % - Pubic Symphysis
    % - Centre Sacral Slope
    % - Acetabulum Contra
    % - Acetabular Cup Centre
    % - R/L ASI
    % Femur
    % - Contra Femoral Head 
    % - Stem Head
    % - GT
    % - Med/Lat Condyles


% 0. Load data
    folder_input = 'Data/3_M_Output_MAT/';
    % Load 3D Points XG
    load([folder_input,'Points_EOS_XG_GT.mat'])
    rP.XG = Points; clear Points
    % Load 3D Points PB
    load([folder_input,'Points_EOS_PB_GT.mat'])
    rP.PB = Points; clear Points

% 00. Loops
    % Loop Operator
    operator = {'XG';'PB'};
    n_op = size(operator,1);
    % Loop Recon
    recon = {'Recon1';'Recon2';'Recon3'};
    n_rec = size(recon,1);
    % Loop Visits
    vis = {'PRE';'P3M'};
    n_v = size(vis,1);
    % Loop Patients
    pat = fieldnames(rP.XG.Recon3.P3M);
    n_pat = size(pat,1);
    % Femur Points to plot
    f_plot_contra = {'Centre','GrandTroch_Q','CentreCondyleM','CentreCondyleL'};
    n_fp_c = size(f_plot_contra,2);  
    mks_c = '^vso';
    
    f_plot_homo = {'Centre','CentreCondyleM','CentreCondyleL'};
    n_fp_h = size(f_plot_homo,2);  
    mks_h = '^so';
    
    lst = {'-',':','--'};
    lw = 1.5;
    col = ['rg';'bc'];
% 1. Points Selection
for p = 1:n_pat % Loop on patients
    % [3 9 15 22] - test with BE_ / GEO / LAH / ROS -> outliers fem torsion
    for v = 1:n_v % Loop on visits
        cpt = 0;
        for op = 1:n_op % Loop on operator
            for r = 1:n_rec % Loop on reconstruction
                cpt = cpt+1;
                leg{cpt} = [operator{op},' ',recon{r},' ',(vis{v})];
                % 3.3.1 - MetaData
                % a - Patient name
%                 Data.(operator{op}).Patient{cpt} = pat{p};
                % b - Points in one fields
                pts = rP.(operator{op}).(recon{r}).(vis{v}).(pat{p});
                % c - Side Surgery
                f_p3m = fieldnames(rP.(operator{op}).(recon{r}).P3M.(pat{p}).Pelvis);
                homo = f_p3m{end}(7);
                % d. Contralateral side
                switch homo
                    case 'D'
                        contra = 'G';
                        y = -1; z = -1; % For cup angles
                        % Homo / Contra Femur
                        homoF   = pts.R_femur; 
                        contraF = pts.L_femur;
                    case 'G'
                        contra = 'D';
                        y =  1; z = -1;
                        % Homo / Contra Femur
                        homoF   = pts.L_femur;
                        contraF = pts.R_femur;
                end
                
                code = [operator{op},'_',recon{r},'_',vis{v},'_',pat{p}]; % Only for debug
                  
                % Pelvis
                % - Pelvis pre & post
                T = pts.Pelvis.Translation;
                P.Contra_Cotyle = pts.Pelvis.(['Cotyle_',contra]) + [T 0];
                P.Centre_Sacral_Slope = pts.Pelvis.Plateau_Sacrum(1,:) + T; % TO DO: Check which is centre
                P.Pubic_Symphisis = pts.Pelvis.SYM_PUB_SUP;
                P.ContraCotyle = pts.Pelvis.(['Cotyle_',contra]) + [T 0];
                % - Pelvis dependant visit
                switch (vis{v})
                    case 'PRE'
                        P.Homo_Cotyle = pts.Pelvis.(['Cotyle_',homo]) + [T 0];
                        % Line Pelvis
                        p_line = [P.Centre_Sacral_Slope;...
                                  P.Pubic_Symphisis;...
                                  P.Homo_Cotyle(1:3);...
                                  P.Contra_Cotyle(1:3);...
                                  P.Pubic_Symphisis];
                        
                    case 'P3M'
                        P.CupCoM = pts.Pelvis.(['Cupule',homo,'_CenterOfMass'])(1:3);
                        P.RASI   = pts.Pelvis.EI_AS_D;
                        P.LASI   = pts.Pelvis.EI_AS_G;
                        p_line = [P.Pubic_Symphisis;...
                                  P.Contra_Cotyle(1:3);...
                                  P.CupCoM(1:3);...
                                  P.Pubic_Symphisis;...
                                  P.Centre_Sacral_Slope;...
                                  P.RASI;...
                                  P.LASI;...
                                  P.Centre_Sacral_Slope];
                end
                
                % Fig 1:
                % plot only contra femur points + value of torsion
                fig = figure(p);fig.Position = [1 41 1600 783];
                sub1 = subplot(1,2,v); hold on
                % Pelvis 
                f_P = fieldnames(P);
                nP = size(f_P);
                for i = 1:nP
                   pt(i,:) = P.(f_P{i})(1:3); 
                   a = plot3(pt(i,1),pt(i,2),pt(i,3),['ok'],'MarkerFaceColor',col(op,v));
                   a.Annotation.LegendInformation.IconDisplayStyle = 'off';
                end
                b = plot3(p_line(:,1),p_line(:,2),p_line(:,3),col(op,v),'LineStyle',lst{r},'LineWidth',lw)
                b.Annotation.LegendInformation.IconDisplayStyle = 'off';
                clear pt a P p_line
                
                % Femur Homolat Side
                for i = 1:n_fp_h
                    pt(i,:) = homoF.(f_plot_homo{i})(1:3);
                    a = plot3(pt(i,1),pt(i,2),pt(i,3),[mks_h(i),'k'],'MarkerFaceColor',col(op,v));
                    a.Annotation.LegendInformation.IconDisplayStyle = 'off';
                end
                pt = [pt;pt(1,:)];
                plot3(pt(:,1),pt(:,2),pt(:,3),col(op,v),'LineStyle',lst{r},'LineWidth',lw)
                clear pt a homoF
                
                % Femur Contralat Side
                for i = 1:n_fp_c
                    pt(i,:) = contraF.(f_plot_contra{i})(1:3);
                    a = plot3(pt(i,1),pt(i,2),pt(i,3),[mks_c(i),'k'],'MarkerFaceColor',col(op,v));
                    a.Annotation.LegendInformation.IconDisplayStyle = 'off';
                end
                pt = [pt;pt(2,:)];
                b = plot3(pt(:,1),pt(:,2),pt(:,3),col(op,v),'LineStyle',lst{r},'LineWidth',lw);
                b.Annotation.LegendInformation.IconDisplayStyle = 'off';

                % Plot Option
                axis equal
                xlim([-100 100])
                ylim([-300 300])
                clear pt a contraF
            end % Loop on reconstruction
        end % Loop on operator
        legend(leg)
        sub1.View = [-80 60];
        sub2.View = [-80 60];
        clear leg
    end % Loop on visits
end % Loop on patients 

 % PRE & P3M
    % Pelvis
    % - Pubic Symphysis
    % - Centre Sacral Slope
    % - Acetabulum Contra
    % Femur
    % - Femoral Head Contra
    % - GT Contra / Homo
    % - Med/Lat Condyles Contra / Homo

% PRE only
    % Pelvis
    % - Acetabulum Homo
    % Femur
    % - Femoral Head Homo

% P3M only
    % Pelvis
    % - R/L ASI
    % - Cup centre
    % Femur
    % - Stem Head
