% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/NSLBP-BIOToolbox
% Reference    : To be defined
% Date         : June 2020
% -------------------------------------------------------------------------
% Description  : Correct issues in raw c3d files
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

% NSLBP-BIO-006-INI: bad EMG channel names
clearvars;
close all;
warning ('off','all');
clc;
Folder.toolbox      = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\';
Folder.dependencies = [Folder.toolbox,'dependencies\'];
addpath(genpath(Folder.dependencies));

cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO\Data\NSLBP-BIO-006\20200728 - INI_session\');
true_labels  = {'R_RA' 'L_RA' 'R_EO' 'L_EO' 'R_LES' 'L_LES' 'R_ESI' 'L_ESI' 'R_MTF' 'L_MTF' 'R_GMED' 'L_GMED' 'R_RF' 'L_RF' 'R_SM' 'L_SM'};

c3dFiles = dir('*.c3d');

for ifile = 1:size(c3dFiles,1)
    btkFile = btkReadAcquisition(c3dFiles(ifile).name);
    for iemg = 1:size(true_labels,2)
        btkSetAnalogLabel(btkFile,iemg,true_labels{iemg});
    end
    btkWriteAcquisition(btkFile,c3dFiles(ifile).name);
    clear btkFile;
end

% NSLBP-BIO-006-REL: Switch between R_SM and L_SM
clearvars;
close all;
warning ('off','all');
clc;
Folder.toolbox      = 'C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO_Toolbox\';
Folder.dependencies = [Folder.toolbox,'dependencies\'];
addpath(genpath(Folder.dependencies));

cd('C:\Users\moissene\OneDrive - unige.ch\2019 - NSCLBP - Biomarkers\Data\NSLBP-BIO\Data\NSLBP-BIO-006\20200804 - REL_session\');
true_labels  = {'R_RA' 'L_RA' 'R_EO' 'L_EO' 'R_LES' 'L_LES' 'R_ESI' 'L_ESI' 'R_MTF' 'L_MTF' 'R_GMED' 'L_GMED' 'R_RF' 'L_RF' 'L_SM' 'R_SM'};

c3dFiles = dir('*.c3d');

for ifile = 1:size(c3dFiles,1)
    btkFile = btkReadAcquisition(c3dFiles(ifile).name);
    for iemg = 1:size(true_labels,2)
        btkSetAnalogLabel(btkFile,iemg,true_labels{iemg});
    end
    btkWriteAcquisition(btkFile,c3dFiles(ifile).name);
    clear btkFile;
end